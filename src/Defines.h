/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_DEFINES_H_
#define SRC_DEFINES_H_

#include <syslog.h>
#include <map>
#include <libserial/SerialPort.h>

/*
 * Application log level
 * 		LOG_EMERG	- system is unusable
 * 		LOG_ALERT	- action must be taken immediately
 * 		LOG_CRIT	- critical conditions
 * 		LOG_ERR		- error conditions
 * 		LOG_WARNING	- warning conditions
 * 		LOG_NOTICE	- normal but significant condition
 * 		LOG_INFO	- informational
 * 		LOG_DEBUG	- debug-level messages
 */
#define APP_LOG_LEVEL 					LOG_NOTICE

#define DAEMON_NAME 					"RFNSMARTGateway"
#define APP_NAME						"RFN-SMART-Gateway"
#define SOFTWARE_VERSION	    		"2.0.5"
#define SOFTWARE_VERSION_MAJOR			2
#define SOFTWARE_VERSION_MINOR			0
#define SOFTWARE_VERSION_BUILD			5

#define RADIO_SERIAL_DEVICE				"/dev/ttyS1"
#define RADIO_SERIAL_SPEED				LibSerial::BaudRate::BAUD_19200
#define RADIO_SERIAL_BUFFER_SIZE		512

#define RADIO_SERIAL_RTS_PIN			17
#define RADIO_SERIAL_CTS_PIN			19
#define GPIO_POWER_DETECT				6
#define ACCELEROMETER_INT_PIN			15

#define RADIO_NETWORK_BUFFER_SIZE		8192

#define SMS_MODEM_DEVICE				"/dev/ttyACM3"
#define SMS_CHECK_INTERVAL_SEC			10
#define SMS_MAX_INIT_RETRIES			5
#define SMS_INCOMING_FOLDER				"/var/spool/sms/incoming"
#define SMS_OUTGOING_FOLDER				"/var/spool/sms/outgoing"

#define NETWORK_LISTENING_PORT			10001
#define NETWORK_SETTINGS_CHECK_PERIOD	60 // Seconds
#define POWER_DETECT_CHECK_PERIOD		20000 // MilliSeconds
#define NETWORK_BUFF_SIZE 		 		512
#define MAX_SERVER_DATA_BUFF        	1024

enum WorkingMode {
	MODE_STANDARD = 0,
	MODE_TRANSPARENT,
	MODE_BRIDGE,
	MODE_TELIT_CLOUD,
	MODE_AWS_IOT,
//	MODE_RFN_MESH,
	MODE_MAX,
	MODE_UNKNOWN = 0xFF
};

enum LedStatus {
	LED_STATUS_OFF,
	LED_STATUS_ON,
	LED_STATUS_SYSTEM,
	LED_STATUS_MAX
};

#define DEFAULT_MODE					MODE_STANDARD

#define DEFAULT_SMS_PASSWORD			"1234"

#define DEFAULT_SERVER_IP				"80.178.98.197"
#define DEFAULT_SERVER_PORT				4009

#define DEFAULT_TELIT_TOKEN				"token"
#define DEFAULT_TELIT_CLOUD_URL			"api.devicewise.com"
#define DEFAULT_TELIT_APP_ID			"TagReceiver"

#define DEFAULT_AWS_CLOUD_URL			"eu-central-1.amazonaws.com"

#define MESSAGE_QUEUE_SIZE				1000
#define MESSAGE_SEND_MAX_RETRIES		3
#define QUEUE_STORE_INTERVAL_SEC		60
#define VOLTAGE_TEMP_INTERVAL_SEC		600

#define DEFAULT_SOCKET_IDLE				4
#define DEFAULT_REBOOT_AFTER			2

#define ACCELEROMETER_I2C_ADDRESS		0x1D
#define ACCELEROMETER_DEFAULT_SENS		0x7E
#define ACCELEROMETER_WHO_I_AM			0x0D
#define ACCELEROMETER_WHO_I_AM_VALUE	0x2A

#define DEFAULT_GPS_PERIOD_SECS			60
#define DEFAULT_GPS_THRESHOLD_SECS		15

// Mesh
#define MAX_CHILDREN_NUM				500
#define MESH_CHILDREN_TIMEOUT_MIN		12
#define MESH_DEFAULT_CHANNEL			2
#define MESH_DEFAULT_ADV_CHANNEL		0
#define MESH_DEFAULT_BASE_TIME			2
#define MESH_DEFAULT_FRAMES_IN_CYCLE	2
#define MESH_DEFAULT_SYNCWORD			0xB56B
#define MESH_DEFAULT_NETWORK_ID			0xAA


#endif /* SRC_DEFINES_H_ */
