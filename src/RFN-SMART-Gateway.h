/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_RFN_SMART_GATEWAY_H_
#define SRC_RFN_SMART_GATEWAY_H_

#include <string>
#include <vector>
#include <syslog.h>
#include <thread>

#include "SMSHandler.h"
#include "DataProcessors/BaseDataProcessor.h"

namespace RFN {

class RFNSMARTGateway {
public:
	/// @param args argc + argv style options
	explicit RFNSMARTGateway(
			const std::vector<std::string>& args = std::vector<std::string>());
	virtual ~RFNSMARTGateway();

	/// @brief Initializes router context / core settings
	void Initialize();

	/// @brief Starts instance
	void Start();

	/// @brief Stops instance
	void Stop();

	/// @brief Reloads configuration
	void Reload();

	BaseDataProcessor* GetProcessor();

	void ChangeMode(WorkingMode);
	void SendSMS(std::string number, std::string message);
	bool UpdateRadioFW(uint8_t type);
	int GetSignalStrength();
	void RestartProcessor();

private:
	WorkingMode mode;
	SMSHandler sms;
	BaseDataProcessor* processor;

	void ValidateConfigFile();
};

}

#endif /* SRC_RFN_SMART_GATEWAY_H_ */
