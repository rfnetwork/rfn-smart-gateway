# RFN-SMART-Gateway #

RF-Networks SMART gateway firmware for receiving tags running on OpenWRT OS

##### OpenWRT package denepdencies #####
* libstdcpp
* libpaho.mqtt.cpp
* libubox 
* libuci
* libserial 
* libnlsocketscpp 
* libgps
* libgpiod
* libi2c
* zlib 
* boost-filesystem 
* RFGSMlib 
* gpsd
* gpsd-clients

### Changelog ###

- **2.0.0** - First version

- **2.0.1** - Telit cloud (DeviceWise) mode fixes

- **2.0.2** - AWS cloud (Amazon) added, MQTT library changed to paho.mqtt

- **2.0.3** - GPS functionality added

- **2.0.4** - SIM not inserted bugfix (Should reset unit after inserting SIM)

- **2.0.5** - External power connect/disconnect detection added (Standard mode)

### LICENSE ###
Copyright (C) 2020 RF-Networks Ltd. All Rights Reserved.