/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include <string>

#include "Daemon.h"
#include "Utilities.h"
#include "Defines.h"

void handle_signal(int sig) {
	switch (sig) {
		case SIGHUP:
		  if (Daemon.m_IsDaemon) {
			static bool first = true;
			if (first) {
			  first = false;
			  return;
			}
		  }
		  syslog(LOG_INFO, "Reloading config...");
		  Daemon.Reload();
		  syslog(LOG_INFO, "Config reloaded");
		  break;
		case SIGABRT:
		case SIGTERM:
		case SIGINT:
		  syslog(LOG_INFO, "%s signal received", (sig == SIGABRT)? "SIGABRT" : (sig == SIGTERM)? "SIGTERM" : "SIGINT");
		  Daemon.m_IsRunning = false;  // Exit loop
		  break;
	}
}

namespace RFN {

DaemonSingleton::DaemonSingleton()
    : m_Exception(__func__), m_IsDaemon(false), m_IsRunning(true)
{
	setlogmask(LOG_UPTO(RFN::Utilities::Utilities::GetDebugLevel()));
	openlog(DAEMON_NAME, LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID,
				LOG_USER);
}

DaemonSingleton::~DaemonSingleton() {
	//Close the log
	closelog();
}

bool DaemonSingleton::Configure(int argc, char* argv[])
{
	try
    {
		RFN::Utilities::config::OptionParser(argc, argv);
		std::vector<std::string> args(argv, argv + argc);

		// Create/configure core instance
		m_obj = std::unique_ptr<RFNSMARTGateway>(new RFNSMARTGateway(args));

		// Set daemon mode (if applicable)
		m_IsDaemon = RFN::Utilities::config::GetArg("-daemon", 0)? true : false;
    }
	catch (...)
    {
		m_Exception.Dispatch(__func__);
		return false;
    }

	syslog(LOG_INFO, "DaemonSingleton: configured");
	return true;
}

bool DaemonSingleton::Initialize()
{
	// We must initialize contexts here (in child process, if in daemon mode)
	try
	{
		m_obj->Initialize();
	}
	catch (...)
    {
		m_Exception.Dispatch(__func__);
		Stop();
		return false;
    }

	syslog(LOG_INFO, "DaemonSingleton: initialized");
	return true;
}

bool DaemonSingleton::Start()
{
	try
	{
		m_obj->Start();
	}
	catch (...)
	{
		m_Exception.Dispatch(__func__);
		return false;
	}

	syslog(LOG_INFO, "DaemonSingleton: successfully started");
	return true;
}

void DaemonSingleton::Reload()
{
	syslog(LOG_INFO, "DaemonSingleton: reloading configuration");
	// Reload client configuration
	m_obj->Reload();
}

bool DaemonSingleton::Stop()
{
	try
	{
		m_obj->Stop();
	}
	catch (...)
	{
		m_Exception.Dispatch(__func__);
		return false;
	}

	syslog(LOG_INFO, "DaemonSingleton: successfully stopped");
	return true;
}

bool DaemonLinux::Configure(int argc, char* argv[]) {
	return DaemonSingleton::Configure(argc, argv);
}

bool DaemonLinux::Initialize() {
	m_PIDPath = (RFN::Utilities::config::GetArg("-service", 0)? "/var/run" : "");
	m_PIDFile = m_PIDPath + "/" + DAEMON_NAME + ".pid";

	if (m_IsDaemon) {
		// Parent
		pid_t pid = fork();
		if (pid > 0)  {
			syslog(LOG_DEBUG, "DaemonLinux: fork success");
			::exit(EXIT_SUCCESS);
		}
		if (pid < 0) {
			syslog(LOG_ERR, "DaemonLinux: fork error");
			return false;
		}
		// Child
		syslog(LOG_DEBUG, "DaemonLinux: creating process group");
		umask(0);
		int sid = setsid();
		if (sid < 0) {
			syslog(LOG_ERR, "DaemonLinux: could not create process group");
			return false;
		}
		syslog(LOG_DEBUG, "DaemonLinux: changing directory to %s", m_PIDPath.c_str());
		if (chdir(m_PIDPath.c_str()) == -1) {
			syslog(LOG_ERR, "DaemonLinux: could not change directory: %d", errno);
			return false;
		}
		// Close stdin/stdout/stderr descriptors
		syslog(LOG_DEBUG, "DaemonLinux: closing descriptors");
		::close(0);
		if (::open("/dev/null", O_RDWR) < 0)
		  return false;
		::close(1);
		if (::open("/dev/null", O_RDWR) < 0)
		  return false;
		::close(2);
		if (::open("/dev/null", O_RDWR) < 0)
		  return false;
	}
	// PID file
	syslog(LOG_DEBUG, "DaemonLinux: opening pid file %s", m_PIDFile.c_str());
	m_PIDFileHandle = open(m_PIDFile.c_str(), O_RDWR | O_CREAT, 0600);
	if (m_PIDFileHandle < 0) {
		syslog(LOG_ERR, "DaemonLinux: could not open pid file %s. Is file in use?", m_PIDFile.c_str());
		return false;
	}
	syslog(LOG_DEBUG, "DaemonLinux: locking pid file");
	if (lockf(m_PIDFileHandle, F_TLOCK, 0) < 0) {
		syslog(LOG_ERR, "DaemonLinux: could not lock pid file %s: %d", m_PIDFile.c_str(), errno);
		return false;
	}
	syslog(LOG_DEBUG, "DaemonLinux: writing pid file");
	std::array<char, 10> pid {{}};
	snprintf(pid.data(), pid.size(), "%d\n", getpid());
	if (write(m_PIDFileHandle, pid.data(), strlen(pid.data())) < 0) {
		syslog(LOG_ERR, "DaemonLinux: could not write pid file %s: %d", m_PIDFile.c_str(), errno);
		return false;
	}
	syslog(LOG_DEBUG, "DaemonLinux: pid file ready");
	// Signal handler
	struct sigaction sa;
	sa.sa_handler = handle_signal;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	sigaction(SIGHUP, &sa, 0);
	sigaction(SIGABRT, &sa, 0);
	sigaction(SIGTERM, &sa, 0);
	sigaction(SIGINT, &sa, 0);
	return DaemonSingleton::Initialize();
}

bool DaemonLinux::Start() {
	return DaemonSingleton::Start();
}

bool DaemonLinux::Stop() {
	syslog(LOG_DEBUG, "DaemonLinux: closing pid file %s", m_PIDFile.c_str());
	if (close(m_PIDFileHandle) == 0) {
		unlink(m_PIDFile.c_str());
	} else {
		syslog(LOG_ERR, "DaemonLinux: could not close pid file %s: %d", m_PIDFile.c_str(), errno);
	}
	return DaemonSingleton::Stop();
}

void DaemonLinux::Reload() {
	DaemonSingleton::Reload();
}

} /* namespace RFN */
