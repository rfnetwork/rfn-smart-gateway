/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#include <mutex>

namespace RFN {

template <typename T>
class RingBuffer {
	T *m_buffer;
	size_t m_head;
	size_t m_tail;
	const size_t m_size;
	size_t m_buflen;
	std::mutex mtx;
public:
	RingBuffer(const size_t size) : m_head(0), m_tail(0), m_size(size), m_buflen(0) {
		m_buffer = new T[size];
	}

	virtual ~RingBuffer() {
		delete [] m_buffer;
	}

	size_t Append(T *data, size_t size) {
		size_t ret = 0;
		if (data == NULL || size < 1)
			return ret;

		mtx.lock();
		for (size_t i = 0; i < size; i++) {
			m_buffer[m_tail] = data[i];
			m_tail = (m_tail + 1) % m_size;
			m_buflen++;
			if (m_buflen > m_size) {
				m_buflen = m_size;
				m_head = (m_head + 1) % m_size;
			}
			ret++;
		}
		mtx.unlock();
		return ret;
	}

	size_t Peek(T *data, size_t size) {
		size_t ret = 0, pos;
		if (data == NULL || size < 1)
			return ret;
		mtx.lock();
		if (size > m_buflen)
			size = m_buflen;
		pos = m_head;
		for (int i = 0; i < (int)size; i++) {
			*(data + i) = m_buffer[pos];
			pos = (pos + 1) % m_size;
			ret++;
		}
		mtx.unlock();
		return ret;
	}

	size_t Get(T *data, size_t size) {
		size_t ret = 0, pos;
		if (data == NULL || size < 1)
			return ret;
		mtx.lock();
		if (size > m_buflen)
			size = m_buflen;
		pos = m_head;
		for (size_t i = 0; i < size; i++) {
			*(data + i) = m_buffer[pos];
			pos = (pos + 1) % m_size;
			ret++;
		}
		m_head = (m_head + size) % m_size;
		m_buflen -= size;
		mtx.unlock();
		return ret;
	}

	size_t Skip(size_t size) {
		size_t ret = 0;
		if (size < 1)
			return ret;
		mtx.lock();
		if (size > m_buflen)
			size = m_buflen;
		m_head = (m_head + size) % m_size;
		m_buflen -= size;
		mtx.unlock();
		return ret;
	}

	void Clear() {
		mtx.lock();
		delete [] m_buffer;
		m_buffer = new T[m_size];
		m_head = 0;
		m_tail = 0;
		m_buflen = 0;
		mtx.unlock();
	}

	size_t Available() {
		size_t ret = 0;
		mtx.lock();
		ret = m_buflen;
		mtx.unlock();
		return ret;
	}

	bool ContainsBitStuffingMessage(size_t *start, size_t *end) {
		mtx.lock();
		size_t available = m_buflen;
		int count = 1;
		*start = *end = 0;
		for (size_t pos = m_head; (size_t)count <= available; pos = (pos + 1) % m_size, count++) {
			if (m_buffer[pos] == 0xAB)
				*start = count;
			else if (m_buffer[pos] == 0xCD) {
				*end = count;
				break;
			}
		}
		mtx.unlock();
		return (*start > 0) && (*end > 0);
	}

	size_t BytesUntill(uint8_t ch) {
		mtx.lock();
		size_t available = m_buflen;
		int count = 1;
		for (size_t pos = m_head; (size_t)count <= available; pos = (pos + 1) % m_size, count++) {
			if (m_buffer[pos] == ch)
				break;
		}
		if (!available || (count > (int)available)) {
			count = 0;
		}
		mtx.unlock();
		return count;
	}
};

} /* namespace RFNetwork */

#endif /* RINGBUFFER_H_ */
