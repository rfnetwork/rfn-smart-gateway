/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <signal.h>
#include "BaseDataProcessor.h"
#include "../RFN-SMART-Gateway.h"

namespace RFN {

BaseDataProcessor::BaseDataProcessor(RFNSMARTGateway *parent)
try :
		m_Exception(__func__), threadNetwork(nullptr), threadRadio(nullptr), threadServer(
				nullptr), threadAccelerometer(nullptr), threadScheduler(nullptr), m_parent(
				parent), running(false), can_start(false), is_connected(false), power_connected(
				false), gpio_RTS(nullptr), gpio_CTS(nullptr), powerDetect(
				nullptr), _socket(nullptr), socketServer(nullptr), serverIP(""), serverPort(
				0), rf_queue(
		MESSAGE_QUEUE_SIZE), serverbuf(MAX_SERVER_DATA_BUFF), network_last_receive_time(
				0), last_send_time(0), last_store_time(0), last_ack_time(0), local_receiver_address(
				0xFFFF), radio_out_msg_num(0), receiver_address_acquired(false), address_acuire_retry(
				0), last_address_acuire_ts(0), enable_radio_network(false), is_configuring(
				false), configuration_state(0), config_reg(0), last_config_msg_time(
				0), last_mesh_children_clear_time(0), config_retry(0), _isConnectedToServer(
				false), reboot_after(0), socket_idle_interval(0), movement_detected(
				false), last_gps_time(0) {
	// Initialize network library
	NL::init();
	last_store_time = network_last_receive_time =
			RFN::Utilities::Utilities::GetUpTimeSeconds();
	last_mesh_sync_time = RFN::Utilities::Utilities::GetUTCTimestamp();
	for (int i = 0; i < PARALLEL_MSGS_SIZE; i++) {
		memset(&send_buf[i], 0, sizeof(QUEUE_ITEM));
	}
}
catch (...) {
	m_Exception.Dispatch();
}

BaseDataProcessor::~BaseDataProcessor() {

}

void BaseDataProcessor::Start() {
	running = true;
	RFN::Utilities::Utilities::exec("mt7688_pinmux set spi_s gpio");
	RFN::Utilities::Utilities::exec("mt7688_pinmux set spi_cs1 gpio");
	RFN::Utilities::Utilities::exec("mt7688_pinmux set pwm1 gpio");
	gpio_RTS = RFN::Utilities::Utilities::GetGPIO(RADIO_SERIAL_RTS_PIN);
	gpio_CTS = RFN::Utilities::Utilities::GetGPIO(RADIO_SERIAL_CTS_PIN);
	powerDetect = RFN::Utilities::Utilities::GetGPIO(GPIO_POWER_DETECT);
	RFN::Utilities::Utilities::SetGPIOInput(gpio_CTS);
	RFN::Utilities::Utilities::SetGPIOOutput(gpio_RTS);
	RFN::Utilities::Utilities::SetGPIOInput(powerDetect);
	RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_OFF);
	// Clear children list
	for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
		children[i].id = children[i].address = children[i].timestamp = 0;
	}
	LoadMessages();
	serverIP = RFN::Utilities::Utilities::GetServerIP();
	serverPort = RFN::Utilities::Utilities::GetServerPort();
	reboot_after = RFN::Utilities::Utilities::GetRebootAfter();
	socket_idle_interval = RFN::Utilities::Utilities::GetSocketIdleInterval();
	threadRadio = new std::thread(
			std::bind(&BaseDataProcessor::RadioThreadFunc, this));
	threadServer = new std::thread(
			std::bind(&BaseDataProcessor::ServerThreadFunc, this));
	threadNetwork = new std::thread(
			std::bind(&BaseDataProcessor::NetworkThreadFunc, this));
	threadAccelerometer = new std::thread(
			std::bind(&BaseDataProcessor::AccelerometerThreadFunc, this));
	threadScheduler = new std::thread(
				std::bind(&BaseDataProcessor::SchedulerThreadFunc, this));
}

void BaseDataProcessor::Stop() {
	running = false;
	DisconnectFromServer();
//	io_service_.stop();
	if (radio_sp.IsOpen())
		radio_sp.Close();
	if (threadServer != nullptr) {
		threadServer->join();
		delete threadServer;
		threadServer = nullptr;
	}
	if (threadNetwork != nullptr) {
		threadNetwork->join();
		delete threadNetwork;
		threadNetwork = nullptr;
	}
	if (threadRadio != nullptr) {
		threadRadio->join();
		delete threadRadio;
		threadRadio = nullptr;
	}
	if (threadAccelerometer != nullptr) {
		threadAccelerometer->join();
		delete threadAccelerometer;
		threadAccelerometer = nullptr;
	}
	if (threadScheduler != nullptr) {
		threadScheduler->join();
		delete threadScheduler;
		threadScheduler = nullptr;
	}
	RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_SYSTEM);
	RFN::Utilities::Utilities::exec("mt7688_pinmux set pwm1 pwm");
	RFN::Utilities::Utilities::exec("mt7688_pinmux set spi_cs1 spi_cs1");
	RFN::Utilities::Utilities::exec("mt7688_pinmux set spi_s spi_s");
}

void BaseDataProcessor::CanStart(bool can_start) {
	this->can_start = can_start;
}

int BaseDataProcessor::GetSignalStrength() {
	return m_parent->GetSignalStrength();
}

void BaseDataProcessor::RadioThreadFunc() {
	RFN::RingBuffer<uint8_t> radiobuf(RADIO_SERIAL_BUFFER_SIZE);
	uint8_t buffer[RADIO_SERIAL_BUFFER_SIZE];
	uint8_t msg[256];
	// Wait until can start thread
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	try {
		// Open serial port
		radio_sp.Open(RADIO_SERIAL_DEVICE);
		radio_sp.SetBaudRate(RADIO_SERIAL_SPEED);
		radio_sp.SetCharacterSize(LibSerial::CharacterSize::CHAR_SIZE_8);
		radio_sp.SetFlowControl(LibSerial::FlowControl::FLOW_CONTROL_NONE);
		radio_sp.SetParity(LibSerial::Parity::PARITY_NONE);
		radio_sp.SetStopBits(LibSerial::StopBits::STOP_BITS_1);
	} catch (const LibSerial::OpenFailed &ex) {
		m_Exception.Dispatch(ex.what());
	}
	RFN::Utilities::Utilities::SetGPIO(gpio_RTS, 0);
	is_configuring = true;

	syslog(LOG_NOTICE,
			"[Radio] Radio processor initialized. Starting configuration...");
	configuration_state = 0;
	config_reg = 0;
	last_config_msg_time = 0;
	config_retry = 0;
	last_ack_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
	int period = RFN::Utilities::Utilities::GetGPSPeriod();
	int threshold = RFN::Utilities::Utilities::GetGPSMovementThreshold();
//	if (running && radio_sp.IsOpen()) {
//		UpdateMeshSettings();
//		CheckStoppingMesh();
//	}
	while (running) {
		try {
			if (radio_sp.IsOpen()) {
				while (radio_sp.IsDataAvailable()) {
					char data_byte;
					radio_sp.get(data_byte);
					radiobuf.Append((uint8_t*) &data_byte, 1);
				}

				size_t start = 0, end = 0;
				if (radiobuf.ContainsBitStuffingMessage(&start, &end)) {
					if (start > 1)
						radiobuf.Skip(start - 1);
					size_t available_size =
							(end == start) ? 0 : (end - start + 1);
					size_t msg_size;
					available_size = radiobuf.Get(buffer, available_size);
					if (available_size > 2
							&& RFN::Utilities::Utilities::BitStuffingDecode(
									buffer, available_size, msg, &msg_size)
							&& msg_size > 2) {
						// Bit-stuffing decoded.
						syslog(LOG_DEBUG, "[Radio] Data Received: %s",
								RFN::Utilities::Utilities::HexArrayToString(msg,
										msg_size).c_str());
						if (msg[0] == 0x96 && msg[1] != 0x65) {
							// RFN new protocol
							syslog(LOG_INFO, "[Radio] RFN Mesh message");
							ProcessRadioMessage(RFN_MESH_PROTOCOL, msg,
									msg_size);
						} else if (msg[1] == 0x65) {
							// RFN old protocol or Telit Mesh protocol
							syslog(LOG_INFO,
									"[Radio] RFN TR/Telit LPM message");
							ProcessRadioMessage(RFN_OLD_PROTOCOL, msg,
									msg_size);
						} else if (msg[0] == 0x69
								&& msg[msg_size - 1] == 0x0D) {
							// Telit telemerty message
							syslog(LOG_INFO, "[Radio] Telit telemetry message");
							ProcessRadioMessage(TELIT_TELEMETRY_PPROTOCOL, msg,
									msg_size);
						} else if (msg[0] == 0x6E
								&& msg[msg_size - 1] == 0x0D) {
							// Telit Hayes message
							syslog(LOG_INFO, "[Radio] Telit Hayes message");
							ProcessRadioMessage(TELIT_HAYES_PROTOCOL, msg,
									msg_size);
						} else {
							// Other protocol
							syslog(LOG_ERR, "Unknown protocol message: %s",
									RFN::Utilities::Utilities::HexArrayToString(
											msg, msg_size).c_str());
						}

						// Forward to connected clients
						SendToClients(msg, msg_size);
					}
				} else if (end > 1) {
					radiobuf.Skip(end - 1);
				}
				long cur_up_time =
						RFN::Utilities::Utilities::GetUpTimeSeconds();
				if (movement_detected) {
					// Reset movement detected flag
					movement_detected = false;
					// Generate GPS message
					if ((threshold > 0) && ((cur_up_time - last_gps_time) > threshold)) {
						threshold = RFN::Utilities::Utilities::GetGPSMovementThreshold();
						GenerateGPSMessage();
					}
				}

				if ((period > 0) && ((cur_up_time - last_gps_time) > period)) {
					period = RFN::Utilities::Utilities::GetGPSPeriod();
					GenerateGPSMessage();
				}
				if (is_configuring) {
					UpdateMeshSettings();
				} else if ((cur_up_time - last_mesh_children_clear_time)
						>= 60) {
					// Time to clear disappeared units
					ClearDisappearedUnits();
					last_mesh_children_clear_time = cur_up_time;
					CheckStoppingMesh();
				}
			}
		} catch (const LibSerial::OpenFailed &ex) {
			m_Exception.Dispatch(ex.what());
		}
		CheckTimeouts(); // Check if need to reboot unit
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	syslog(LOG_NOTICE, "[Radio] Radio processor finished.");
}

void BaseDataProcessor::ServerThreadFunc() {
	// Wait until can start thread
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	socketServer = new NL::Socket(NETWORK_LISTENING_PORT);
	socketServer->blocking(false);
	syslog(LOG_NOTICE, "[Server] Server started.");

	OnAccept onAccept;
	OnRead onRead;
	OnDisconnect onDisconnect;

	group.setCmdOnAccept(&onAccept);
	group.setCmdOnRead(&onRead);
	group.setCmdOnDisconnect(&onDisconnect);
	group.add(socketServer);

	while (running) {
		group.listen(1000);
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	// Close all connected clients
	for (unsigned i = 1; i < (unsigned) group.size(); ++i)
		group.get(i)->disconnect();

	if (socketServer != nullptr)
		delete socketServer;

	syslog(LOG_NOTICE, "[Server] Server finished.");
}

void BaseDataProcessor::AccelerometerThreadFunc() {
	struct gpiod_line *acc_int;
	bool acc_initialize = true;
	int ret;
	struct timespec ts = { 1, 0 };
	struct gpiod_line_event event;

	syslog(LOG_NOTICE, "[Acc] Started.");
	// Initialize accelerometer
	acc_int = RFN::Utilities::Utilities::GetGPIO(ACCELEROMETER_INT_PIN);
	if (!acc_int) {
		acc_initialize = false;
		syslog(LOG_ERR, "[Acc] Can't initialize accelerometer!!!");
	} else {
		// Initialize accelerometer and Register for interrupt
		if (!RFN::Utilities::Utilities::InitializeAccelerometer(
				RFN::Utilities::Utilities::GetAccelerometerSensitivity())
				|| gpiod_line_request_rising_edge_events(acc_int, "Consumer")) {
			acc_initialize = false;
			syslog(LOG_ERR, "[Acc] Can't initialize accelerometer!!!");
		} else
			syslog(LOG_NOTICE,
					"[Acc] Accelerometer initialized (Sensitivity %d).",
					RFN::Utilities::Utilities::GetAccelerometerSensitivity());
	}
	while (running) {
		if (acc_initialize) {
			ret = gpiod_line_event_wait(acc_int, &ts);
			if (ret > 0 && !gpiod_line_event_read(acc_int, &event)) {
				if (event.event_type == GPIOD_LINE_EVENT_RISING_EDGE) {
					// Accelerometer interrupt
					RFN::Utilities::Utilities::ResetAccelerometerInterrupt();
					if (!movement_detected) {
						movement_detected = true;
						syslog(LOG_NOTICE, "[Acc] Movement detected.");
					}
				}
			}
		}
		std::this_thread::sleep_for(std::chrono::microseconds(10));
	}
	syslog(LOG_NOTICE, "[Acc] Accelerometer finished.");
}

void BaseDataProcessor::SchedulerThreadFunc() {
	int tmp_pc;
	int counter = POWER_DETECT_CHECK_PERIOD;
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	syslog(LOG_NOTICE, "[Scheduler] Started.");

	tmp_pc = RFN::Utilities::Utilities::ReadGPIO(
			powerDetect);
	if (tmp_pc > -1) {
		power_connected = !tmp_pc; // Store negative in order to set
	}
	while (running) {
		if (counter >= POWER_DETECT_CHECK_PERIOD) {
			// Check period
			tmp_pc = RFN::Utilities::Utilities::ReadGPIO(
					powerDetect);
			if (power_connected != tmp_pc) {
				// Change detected
				syslog(LOG_NOTICE, "[Scheduler] Power alert (%s).", ((!tmp_pc) ? "Connected" : "Disconnected"));
				power_connected = tmp_pc;
				PowerChangeDetected();
			}
			counter = 0;
		}
		else {
			counter++;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	syslog(LOG_NOTICE, "[Scheduler] Finished.");
}

void BaseDataProcessor::SendToClients(uint8_t *data, size_t size) {
	uint8_t encoded_msg[255];
	size_t encoded_size;
	if (data == nullptr || size < 1)
		return;
	// Encode bit-stuffing
	RFN::Utilities::Utilities::BitStuffingEncode(data, size, encoded_msg, &encoded_size);
	if (encoded_size > 0)
	{
		for (unsigned i = 1; i < (unsigned) group.size(); ++i) {
			try {
				group.get(i)->send(encoded_msg, encoded_size);
			} catch (NL::Exception &ex) {
				group.remove(i);
			}
		}
	}
}

void BaseDataProcessor::DisconnectFromServer() {
	if (_socket != nullptr) {
		_socket->disconnect();
		delete _socket;
		_socket = nullptr;
	}
	_isConnectedToServer = false;
	RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_OFF);
	syslog(LOG_NOTICE, "[Network] Disconnected from server.");
}

bool BaseDataProcessor::IsConnectedToServer() {
	return _isConnectedToServer;
}

bool BaseDataProcessor::EstablishServerConnection() {
	if (serverPort < 1 || serverIP.empty())
		return false;
	try {
		if (_socket == nullptr) {
			_socket = new NL::Socket(serverIP, serverPort);
			_socket->blocking(false);
			_isConnectedToServer = true;
			network_last_receive_time =
					RFN::Utilities::Utilities::GetUpTimeSeconds();
		}
		return true;
	} catch (NL::Exception &e) {
		return false;
	}
}

void BaseDataProcessor::CheckServerSettings() {
	static long prevTs = 0;
	long curUpTime = RFN::Utilities::Utilities::GetUpTimeSeconds();
	if (!prevTs || ((curUpTime - prevTs) > NETWORK_SETTINGS_CHECK_PERIOD)) {
		// Check server settings for change
		syslog(LOG_NOTICE, "[Network] Check server IP/port changing.");
		uint16_t tempPort = RFN::Utilities::Utilities::GetServerPort();
		std::string tempIP = RFN::Utilities::Utilities::GetServerIP();
		if (tempPort != serverPort
				|| strcmp(serverIP.c_str(), tempIP.c_str()) != 0) {
			// Settings were changed, reconnect.
			serverIP = tempIP;
			serverPort = tempPort;
			DisconnectFromServer();
		}
		prevTs = curUpTime;
	}
}

void BaseDataProcessor::ServerSocketSend(uint8_t *data, size_t size) {
	if (size < 1 || _socket == nullptr)
		return;
	try {
		_socket->send(data, size);
		//socket_.send(boost::asio::buffer(data, size));
	} catch (NL::Exception &e) {
		DisconnectFromServer();
	}
}

void BaseDataProcessor::HandleServerStream() {
	uint8_t buffer[MAX_SERVER_DATA_BUFF];
	unsigned int available;
	int i = 0;
	available = serverbuf.Peek(buffer, MAX_SERVER_DATA_BUFF);
	while (available > 31) {
		i = 0;
		while ((available > 31)
				&& (buffer[i] != 0xAA || buffer[i + 1] != 0x55
						|| buffer[i + 2] != 0xAA || buffer[i + 3] != 0x55)) {
			syslog(LOG_NOTICE,
					"[Network] Is not GPRS anchor 0xAA55AA55 (0x%s), Available %d bytes",
					RFN::Utilities::Utilities::HexArrayToString(buffer + i, 4).c_str(),
					available);
			i++;
			available--;
		}
		if (i > 0) {
			// Need to skip bytes
			serverbuf.Skip(i);
			available = serverbuf.Peek(buffer, MAX_SERVER_DATA_BUFF);
			syslog(LOG_NOTICE,
					"[Network] %d bytes were skipped, available %d bytes", i,
					available);
		}
		// Validate message.
		if (available > 31) {
			// Header exists.
			uint8_t msg_len = buffer[4];
			if ((msg_len > 0) && ((size_t) msg_len + 31 <= available)) {
				unsigned char cs = RFN::Utilities::Utilities::CalculateChecksum(
						buffer + 32, msg_len - 2);
				if (((buffer[31] + 1) == msg_len)
						&& (cs == buffer[msg_len + 30])) {
					// Message available for parsing
					uint8_t len = buffer[31];
					uint8_t msg_type = buffer[32];
					//unsigned char msg_num = buffer[33];
					switch (msg_type) {
					case MSG_TYPE_SERVER_ACK:
						uint16_t tmp_msg_num;
						tmp_msg_num = ((uint16_t) (buffer[34])
								+ ((uint16_t) (buffer[35]) << 8));
						last_ack_time =
								RFN::Utilities::Utilities::GetUpTimeSeconds();
						for (int i = 0; i < PARALLEL_MSGS_SIZE; i++) {
							if (send_buf[i].size > 0
									&& send_buf[i].message_number
											== tmp_msg_num) {
								// Acknowledge received. Remove item.
								send_buf[i].size = 0;
								send_buf[i].retries = 0;
								send_buf[i].last_sent_time = 0;
								syslog(LOG_NOTICE,
										"[Network] Message #%d acknowledge received from server",
										tmp_msg_num);
							}
						}
						break;
					case MSG_TYPE_SERVER_TRANSIENT:
						// Forward data to radio
						if (len > 3) {
							syslog(LOG_NOTICE,
									"[Network] Transient message received. Forwarding to radio...");
							syslog(LOG_DEBUG, "%s",
									RFN::Utilities::Utilities::HexArrayToString(
											buffer + 34, len - 3).c_str());
							SendToRadioRaw((buffer + 34), len - 3);
						} else {
							syslog(LOG_ERR,
									"[Network] Wrong transient message size!");
						}
						break;
					case MSG_TYPE_SERVER_SEND_SMS:
						if (len > 5 && buffer[34] > 0
								&& buffer[35 + buffer[34]] > 0) {
							syslog(LOG_NOTICE,
									"[Network] Sending SMS to %s: %s",
									buffer + 35, buffer + (36 + buffer[34]));
							// Send SMS
							std::string number((char*) ((buffer) + 35));
							std::string message(
									(char*) ((buffer) + (36 + buffer[34])));
							m_parent->SendSMS(number, message);
						}
						break;
					default:
						ProcessServerMessage(buffer, msg_len);
						break;
					}
				} else {
					syslog(LOG_NOTICE,
							"[Network] Wrong message length or checksum!!! The message will be discarded");
					syslog(LOG_NOTICE,
							"[Network]\t(Checksum 0x%02X, calculated 0x%02X)",
							buffer[msg_len + 30], cs);
				}
				serverbuf.Skip(msg_len + 31);
			} else {
				// Not enough bytes
				break;
			}
		}
		available = serverbuf.Peek(buffer, MAX_SERVER_DATA_BUFF);
	}
}

void BaseDataProcessor::SendServerMessages() {
	long cur_time = RFN::Utilities::Utilities::GetUTCTimestamp();
	FillSendBuffer();
	for (int i = 0; i < PARALLEL_MSGS_SIZE; i++) {
		if (send_buf[i].size > 0
				&& ((cur_time - send_buf[i].last_sent_time) > SEND_MSG_TIME_OUT)) {
			unsigned short receiver_id = 0;
			unsigned char rssi = 0xFF;
			if (!send_buf[i].isLocal && send_buf[i].size > 4
					&& send_buf[i].data != NULL
					&& send_buf[i].data[1] == 0x65) {
				rssi = send_buf[i].data[0];
				memcpy(&receiver_id, send_buf[i].data + 2, 2);
				SendGPRSMessage(send_buf[i].timestamp,
						send_buf[i].message_number, receiver_id, rssi,
						send_buf[i].data + 4, send_buf[i].size - 4);
				send_buf[i].last_sent_time = cur_time;
				send_buf[i].retries++;
			} else if (send_buf[i].isLocal
					&& send_buf[i].size > 0 && send_buf[i].data != NULL) {
				rssi = 0xFF;
				SendGPRSMessage(send_buf[i].timestamp,
						send_buf[i].message_number, local_receiver_address,
						rssi, send_buf[i].data, send_buf[i].size);
				send_buf[i].last_sent_time = cur_time;
				send_buf[i].retries++;
			}
		}
	}
}

void BaseDataProcessor::SendGPRSMessage(long timestamp, uint16_t message_num,
		uint16_t receiver_id, uint8_t rssi, uint8_t *data, size_t data_size) {
	long long device_imei = std::stoll(RFN::Utilities::Utilities::GetIMEI());
	uint8_t *s_buffer = new uint8_t[255];
	if (_socket != nullptr && s_buffer != nullptr) {
		s_buffer[0] = s_buffer[2] = 0xAA;
		s_buffer[1] = s_buffer[3] = 0x55;
		s_buffer[4] = data_size;
		memcpy(s_buffer + 5, &message_num, 2); // Msg number
		memcpy(s_buffer + 7, &timestamp, 4); // Time
		memcpy(s_buffer + 11, &device_imei, 8); // Unit IMEI
		memcpy(s_buffer + 19, &receiver_id, 2); // Receiver ID
		s_buffer[21] = 0;
		s_buffer[22] = 0;
		s_buffer[23] = rssi;
		// Copy data
		if (data_size > 0 && data != NULL) {
			memcpy(s_buffer + 31, data, data_size);
		}

		try {
			syslog(LOG_DEBUG, "Sending to server: %s",
					RFN::Utilities::Utilities::HexArrayToString(s_buffer,
							31 + data_size).c_str());
			_socket->send(s_buffer, 31 + data_size);

			last_send_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
			syslog(LOG_NOTICE, "[Network] Message #%u was sent", message_num);
		} catch (NL::Exception &ex) {
			syslog(LOG_ERR, "[Network] Can't send message #%u (%s)",
					message_num, ex.what());
			DisconnectFromServer();
		}
		delete s_buffer;
	}
}

void BaseDataProcessor::SendToRadioRaw(uint8_t *data, size_t size) {
	// Send only if serial port is opened and Flow control RTS pin is low
	if (radio_sp.IsOpen() && (!RFN::Utilities::Utilities::ReadGPIO(gpio_RTS))) {
		syslog(LOG_DEBUG, "[Radio] Data Sent: %s",
				RFN::Utilities::Utilities::HexArrayToString(data, size).c_str());
		radio_sp.write((const char*) data, size);
		radio_sp.DrainWriteBuffer();
	}
}

void BaseDataProcessor::ProcessRadioMessage(enum protocols protocol,
		uint8_t *data, size_t size) {
	if ((protocol == RFN_MESH_PROTOCOL) && (size > 4) && (data[1] == 1)) {
		// RFN-Mesh protocol version 1, validate message
		if (data[2] == (size - 7)) {
			// Size is OK, validate CRC
			uint16_t received_crc, tmp_crc;
			memcpy(&received_crc, data + size - 2, 2);
			tmp_crc = RFN::Utilities::Utilities::CalculateCRC16Xmodem(data,
					size - 2);
			if (tmp_crc == received_crc) {
				// Valid CRC, handler protocol message
				bool res = HandleRFNMeshV1Message(data[3], data[4], data + 5,
						size - 7);

				if (res) {
					// Enable or disable network according to queue fill status
					CheckStoppingMesh();
				}
			} else {
				// CRC checksum error
				syslog(LOG_ERR,
						"[Radio] CRC checksum failed (Calculated: 0x%04X, Received: 0x%04X): %s\r\n",
						tmp_crc, received_crc,
						RFN::Utilities::Utilities::HexArrayToString(data, size).c_str());
			}
		} else {
			syslog(LOG_ERR,
					"Message size and payload size doesn't match (Message size %d, payload size %d): %s",
					size, data[2],
					RFN::Utilities::Utilities::HexArrayToString(data, size).c_str());
		}
	} else if ((protocol == RFN_OLD_PROTOCOL)) {
		// RF-Networks old protocol or Telit LPM protocol
		size_t msg_start = 1, msg_size;
		if ((size > 0) && (size > 3 + msg_start)) {
			// Valid size
			uint8_t rssi = (msg_start > 0) ? data[msg_start - 1] : 0xFF;
			uint16_t address = (((uint16_t) data[msg_start + 2] << 8)
					+ data[msg_start + 1]) & 0xFFFF;
			msg_size = data[msg_start + 3];
			if (msg_size >= (size - msg_start - 3)) {
				syslog(LOG_ERR,
						"[Radio] Wrong mesh message size!!! The message will be discarded.");
				return;
			}
			if (msg_size < 4) {
				syslog(LOG_ERR,
						"[Radio] Wrong receiver message size(%d)!!! The message will be discarded.",
						msg_size);
				return;
			}
			// Receiver message size is OK. Calculate checksum
			uint8_t cs = RFN::Utilities::Utilities::CalculateChecksum(
					&data[msg_start + 4], msg_size - 1);
			if (cs != data[msg_start + 3 + msg_size]) {
				syslog(LOG_ERR,
						"[Radio] Wrong checksum!!! Expected 0x%02X, Calculated 0x%02X.",
						data[msg_start + 3 + msg_size], cs);
				return;
			}
			uint8_t msg_type = data[msg_start + 4] & 0xFF;
			uint8_t msg_num = data[msg_start + 5] & 0xFF;
			syslog(LOG_NOTICE,
					"[Radio] Mesh message #%d  (Type: 0x%02X, RSSI: 0x%02X) received from 0x%04X.",
					msg_num, msg_type, rssi, address);
			if (msg_type != MSG_TYPE_SERVER_ACK) {
				if (!receiver_address_acquired) {
					if (msg_type == MSG_TYPE_SERVER_RECEIVER_MSG
							&& ((size > msg_start + 9)
									&& (data[msg_start + 9] == 0x02)
									&& (data[msg_start + 10] == 0x00))) {
						local_receiver_address = (uint16_t) data[msg_start + 1]
								+ (((uint16_t) data[msg_start + 2]) << 8);
						memcpy(radio_version, data + msg_start + 12, 3);
						syslog(LOG_NOTICE,
								"[Radio] Receiver address acquired: %d(0x%04X), Radio Version: %d.%d.%d.",
								local_receiver_address, local_receiver_address,
								radio_version[0], radio_version[1],
								radio_version[2]);

						receiver_address_acquired = true;
					}
				}

				// Add to queue
				if (rf_queue.Enqueue(data, size, false)) {
					syslog(LOG_NOTICE,
							"[Radio] Mesh message #%d was added to queue.",
							(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
				} else {
					syslog(LOG_NOTICE,
							"[Radio] Can't add mesh message to queue!!!");
				}
			}
		} else {
			syslog(LOG_ERR, "Message size is not valid (%d)", size);
		}
	}
}

void BaseDataProcessor::PowerChangeDetected() {

}

bool BaseDataProcessor::HandleRFNMeshV1Message(uint8_t msg_type,
		uint8_t msg_num, uint8_t *msg, size_t size) {
	bool res = false;
	uint8_t tag_msg[21] = { 0xFF, 0x65, 0xFF, 0xFF, 0x10, 0x01, 0x00, 0x0C,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00 };
	syslog(LOG_INFO, "[Radio] Message #%d received of type %d(0x%02X)", msg_num,
			msg_type, msg_type);
	switch (msg_type & 0xF0) {
	case 0x10:
		// Configuration
		switch (msg_type & 0x0F) {
		case 1:
			// Get register response
			if (size > 10) {
				uint16_t reg;
				uint32_t val;
				memcpy(&reg, msg + 4, 2);
				memcpy(&val, msg + 6, 4);
				syslog(LOG_NOTICE, "[Radio] Register %u response %u", reg, val);
				if (reg == 70) {
					// Register 70 - Version
					radio_version[0] = (val >> 16) & 0xFF;
					radio_version[1] = (val >> 8) & 0xFF;
					radio_version[2] = (val >> 0) & 0xFF;
					syslog(LOG_NOTICE, "[Radio] Radio version: %u.%u.%u",
							radio_version[0], radio_version[1],
							radio_version[2]);
					receiver_address_acquired = true;
				}
				if (is_configuring && config_reg == reg) {
					last_config_msg_time =
							RFN::Utilities::Utilities::GetUpTimeSeconds() - 3;
					configuration_state++;
					config_retry = 0;
				}
			}
			break;
		case 3:
			// Set register response
			if (size > 6) {
				uint16_t stored_address, reg;
				uint32_t id;
				memcpy(&stored_address, msg, 2);
				memcpy(&reg, msg + 4, 2);
				id = GetUnitIDByShortAddress(stored_address);
				syslog(LOG_NOTICE,
						"[Radio] Unit %u (0x%08X) set register %d response: %d\r\n",
						id, id, reg, msg[6]);
				if (is_configuring && config_reg == reg) {
					last_config_msg_time =
							RFN::Utilities::Utilities::GetUpTimeSeconds() - 3;
					configuration_state++;
					config_retry = 0;
				}
			}
			break;
		case 4:
			// Get time request, generate response
			SendMeshTimeResponse();
			syslog(LOG_NOTICE,
					"[Radio] Sync time request received, updating time...");
			break;
		}
		break;
	case 0x20:
		// Data
		switch (msg_type & 0x0F) {
		case 0:
			if (msg[13] == 1 && size == 27) { // Only one tag message
			// Old protocol tag message
				uint32_t tag_id, created_ts;
				memcpy(&created_ts, msg + 5, 4);
				memcpy(&tag_id, msg + 16, 4);
				syslog(LOG_NOTICE, "[Radio] [%ddBm] Tag %u Time: %s, %s",
						-msg[0], tag_id,
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(),
						RFN::Utilities::Utilities::HexArrayToString(msg + 14,
								13).c_str());
				// Enqueue old tag message
				tag_msg[0] = msg[0]; // RSSI
				memcpy(tag_msg + 7, msg + 14, 13); // Copy tag message
				tag_msg[19] = (((uint8_t) tag_msg[19] + 120) << 1); // Convert RSSI
				tag_msg[20] = RFN::Utilities::Utilities::CalculateChecksum(
						tag_msg + 5, 15); // Checksum
				// Add to queue
				if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
						created_ts)) {
					syslog(LOG_NOTICE,
							"[Radio] Mesh message #%d was added to queue.",
							(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
				} else {
					syslog(LOG_NOTICE,
							"[Radio] Can't add mesh message to queue!!!");
				}
			} else {
				syslog(LOG_NOTICE, "[Radio] Data message: %s",
						RFN::Utilities::Utilities::HexArrayToString(msg, size).c_str());
			}
			break;
		case 1:
			// Data sensor
			uint16_t stored_address;
			uint32_t id, created_ts, received_ts;
			memcpy(&stored_address, msg + 1, 2);
			id = GetUnitIDByShortAddress(stored_address);
			memcpy(&created_ts, msg + 5, 4);
			memcpy(&received_ts, msg + 9, 4);
			UpdateChildLastSentTS(id, stored_address);
			// Handle sensor message according sensor type
			switch (msg[13]) {
			case 0x24:
				// SI7021
				uint16_t hum, temp;
				memcpy(&hum, msg + 14, 2);
				memcpy(&temp, msg + 16, 2);
				syslog(LOG_NOTICE,
						"[Radio] [%s][-%ddBm] SI7021 sensor %d: Temperature - %.2fC, Humidity - %.2f%%",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), msg[0], id, temp / 256.0F,
						hum / 512.0F);
				tag_msg[0] = msg[0]; // RSSI
				tag_msg[7] = 0x0C;
				tag_msg[8] = 0x05; // Type - TemperatureRH
				memcpy(tag_msg + 9, &id, 4);
				memset(tag_msg + 13, 0, 6);
				memcpy(tag_msg + 14, &hum, 2);
				memcpy(tag_msg + 16, &temp, 2);
				tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
				tag_msg[20] = RFN::Utilities::Utilities::CalculateChecksum(
						tag_msg + 5, 15); // Checksum
				// Add to queue
				if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
						created_ts)) {
					syslog(LOG_NOTICE,
							"[Radio] Mesh message #%d was added to queue.",
							(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
				} else {
					syslog(LOG_NOTICE,
							"[Radio] Can't add mesh message to queue!!!");
				}
				break;
			case 0x25:
				// BME280
				BME_DATA bme_data;
				float bm_temp, bm_press, bm_hum;
				memcpy(&bme_data, msg + 14, 5);
				bm_temp = bme_data.temperature / 16.0F - 40;
				bm_press = (bme_data.pressure * 32 + 10624000) / 256000.0F;
				bm_hum = bme_data.humidity / 8.0F;
				syslog(LOG_NOTICE,
						"[Radio] [%s][-%ddBm] BME280 sensor %d: Temperature - %.2fC, Pressure - %.2fmmHg, Humidity - %.2f%%",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), msg[0], id, bm_temp,
						bm_press, bm_hum);
				tag_msg[0] = msg[0]; // RSSI
				tag_msg[7] = 0x0C;
				tag_msg[8] = 0x04; // Type - Pressure
				memcpy(tag_msg + 9, &id, 4);
				memset(tag_msg + 13, 0, 6);
				memcpy(tag_msg + 14, msg + 14, 5);
				tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
				tag_msg[20] = RFN::Utilities::Utilities::CalculateChecksum(
						tag_msg + 5, 15); // Checksum
				// Add to queue
				if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
						created_ts)) {
					syslog(LOG_NOTICE,
							"[Radio] Mesh message #%d was added to queue.",
							(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
				} else {
					syslog(LOG_NOTICE,
							"[Radio] Can't add mesh message to queue!!!");
				}
				break;
			case 0x26:
				// Analog
				memcpy(&temp, msg + 14, 2); // Analog A
				memcpy(&hum, msg + 16, 2);  // Analog B
				syslog(LOG_NOTICE,
						"[Radio] [%s][-%ddBm] Analog sensor %d: Analog A - %d, Analog B - %d, External Voltage - %.2fV",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), msg[0], id, temp, hum,
						((((temp / 16.0F) * 92406) + 2047) / 4095) * 16);
				break;
			case 0x28:
				// Accelerometer
				syslog(LOG_NOTICE,
						"[Radio] [%s][-%ddBm] Accelerometer sensor %d: Orientation - %d",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), msg[0], id, msg[14]);
				tag_msg[0] = msg[0]; // RSSI
				tag_msg[7] = 0x0C;
				tag_msg[8] = 0x09; // Type - Movement
				memcpy(tag_msg + 9, &id, 4);
				memset(tag_msg + 13, 0, 6);
				tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
				tag_msg[20] = RFN::Utilities::Utilities::CalculateChecksum(
						tag_msg + 5, 15); // Checksum
				// Add to queue
				if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
						created_ts)) {
					syslog(LOG_NOTICE,
							"[Radio] Mesh message #%d was added to queue.",
							(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
				} else {
					syslog(LOG_NOTICE,
							"[Radio] Can't add mesh message to queue!!!");
				}
				break;
			case 0x29:
				// GPIO
				syslog(LOG_NOTICE,
						"[Radio] [%s][-%ddBm] GPIO sensor %d: GPIO%d - %d",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), msg[0], id, msg[14],
						msg[15]);
				tag_msg[0] = msg[0]; // RSSI
				tag_msg[7] = 0x0C;
				if (!(msg[14] & 0x07)) {
					// Not supported GPIOs, do nothing

				} else {
					// Set GPIO value
					uint8_t gpio_val = (msg[15] << 5) & 0xFF;
					if (msg[14] & 0x02) {
						// GPIO 7 trigger
						tag_msg[8] = gpio_val | 0x06; // Push button 06

						memcpy(tag_msg + 9, &id, 4);
						memset(tag_msg + 13, 0, 6);
						tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
						tag_msg[20] =
								RFN::Utilities::Utilities::CalculateChecksum(
										tag_msg + 5, 15); // Checksum
						// Add to queue
						if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
								created_ts)) {
							syslog(LOG_NOTICE,
									"[Radio] Mesh message #%d was added to queue.",
									(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
						} else {
							syslog(LOG_NOTICE,
									"[Radio] Can't add mesh message to queue!!!");
						}
					}
					if (msg[14] & 0x04) {
						// GPIO 8 trigger
						tag_msg[8] = gpio_val | 0x1D; // Push button 29

						memcpy(tag_msg + 9, &id, 4);
						memset(tag_msg + 13, 0, 6);
						tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
						tag_msg[20] =
								RFN::Utilities::Utilities::CalculateChecksum(
										tag_msg + 5, 15); // Checksum
						// Add to queue
						if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
								created_ts)) {
							syslog(LOG_NOTICE,
									"[Radio] Mesh message #%d was added to queue.",
									(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
						} else {
							syslog(LOG_NOTICE,
									"[Radio] Can't add mesh message to queue!!!");
						}
					}
					if (msg[14] & 0x01) {
						// GPIO 9 trigger
						tag_msg[8] = gpio_val | 0x0C; // Push button 12

						memcpy(tag_msg + 9, &id, 4);
						memset(tag_msg + 13, 0, 6);
						tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
						tag_msg[20] =
								RFN::Utilities::Utilities::CalculateChecksum(
										tag_msg + 5, 15); // Checksum
						// Add to queue
						if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
								created_ts)) {
							syslog(LOG_NOTICE,
									"[Radio] Mesh message #%d was added to queue.",
									(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
						} else {
							syslog(LOG_NOTICE,
									"[Radio] Can't add mesh message to queue!!!");
						}
					}
				}
				break;
			case 0x30:
				// DS18B20
				memcpy(&temp, msg + 14, 2);
				syslog(LOG_NOTICE,
						"[Radio] [%s][-%ddBm] DS18B20 sensor %d: Temperature - %.2fC",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), msg[0], id, temp / 16.0F);
				// Add tag temperature message
				tag_msg[0] = msg[0]; // RSSI
				tag_msg[7] = 0x0C;
				tag_msg[8] = 0x03; // Type - Temperature
				memcpy(tag_msg + 9, &id, 4);
				memset(tag_msg + 13, 0, 6);
				memcpy(tag_msg + 16, &temp, 2);
				tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
				tag_msg[20] = RFN::Utilities::Utilities::CalculateChecksum(
						tag_msg + 5, 15); // Checksum
				// Add to queue
				if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
						created_ts)) {
					syslog(LOG_NOTICE,
							"[Radio] Mesh message #%d was added to queue.",
							(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
				} else {
					syslog(LOG_NOTICE,
							"[Radio] Can't add mesh message to queue!!!");
				}
				break;
			case 0x31:
				// Parking tag
				PARKING_DATA parking_data;
				memcpy(&parking_data, msg + 14, sizeof(PARKING_DATA));
				syslog(LOG_NOTICE,
						"[Radio] [%s][-%ddBm] Parking sensor %d: State - %d",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), msg[0], id,
						parking_data.state);
				// Add tag distance message
				tag_msg[0] = msg[0]; // RSSI
				tag_msg[7] = 0x0C;
				tag_msg[8] = 0x0D; // Type - Distance
				memcpy(tag_msg + 9, &id, 4);
				memset(tag_msg + 13, 0, 6);
				memcpy(tag_msg + 16, &parking_data, 2);
				tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
				tag_msg[20] = RFN::Utilities::Utilities::CalculateChecksum(
						tag_msg + 5, 15); // Checksum
				// Add to queue
				if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false,
						created_ts)) {
					syslog(LOG_NOTICE,
							"[Radio] Mesh message #%d was added to queue.",
							(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
				} else {
					syslog(LOG_NOTICE,
							"[Radio] Can't add mesh message to queue!!!");
				}
				break;
			default:
				// Unknown
				syslog(LOG_NOTICE, "[Radio] [%s][%ddBm] Data sensor %d: %s",
						RFN::Utilities::Utilities::TimestampUTCToString(
								created_ts).c_str(), -msg[0], id,
						RFN::Utilities::Utilities::HexArrayToString(msg, size).c_str());
				break;
			}
			break;
		}
		break;
	case 0x30:
		// Diagnostic
		switch (msg_type & 0x0F) {
		case 3:
			// Do not answer, if not connected, to make tags not send data
			if (!IsConnectedToServer())
				break;
			uint32_t id;
			uint16_t short_addr;
			memcpy(&id, msg, 4);
			memcpy(&short_addr, msg + 8, 2);
			uint16_t stored_address = GetShortAddressByUnitID(id);
			if (stored_address > 0)
				syslog(LOG_NOTICE,
						"[Radio] Requested short address for unit %u (0x%08X) is %u (0x%04X)",
						id, id, stored_address, stored_address);
			else {
				stored_address = short_addr;
				syslog(LOG_NOTICE,
						"[Radio] Requested short address for unit %u (0x%08X) is %u (0x%04X)",
						id, id, stored_address, stored_address);
			}
			SendGetShortAdressResponse(stored_address, msg, size);
			break;
		}
		break;
	case 0x40:
		// Events
		switch (msg_type & 0x0F) {
		case 1:
			// Check-In
			uint32_t id, created_ts;
			uint16_t stored_address;
			memcpy(&id, msg, 4);
			memcpy(&stored_address, msg + 4, 2);
			memcpy(&created_ts, msg + 23, 4);
			float voltage = (((int16_t) msg[15]) / 62.5F);
			syslog(LOG_NOTICE,
					"[Radio] Check-In received from %u (0x%08X) - address %u (0x%04X), Voltage  - %.2fV",
					id, id, stored_address, stored_address, voltage);
			UpdateChildShortAddress(id, stored_address);
			// Add periodic message
			tag_msg[0] = msg[7]; // RSSI
			tag_msg[7] = 0x0C;
			tag_msg[8] = 0x01; // Type - Periodic
			memcpy(tag_msg + 9, &id, 4);
			memset(tag_msg + 13, 0, 6);
			tag_msg[16] = msg[15];
			tag_msg[19] = (((-(int8_t) msg[0]) + 120) << 1); // Convert RSSI
			tag_msg[20] = RFN::Utilities::Utilities::CalculateChecksum(
					tag_msg + 5, 15); // Checksum
			// Add to queue
			if (rf_queue.Enqueue(tag_msg, sizeof(tag_msg), false, created_ts)) {
				syslog(LOG_NOTICE,
						"[Radio] Mesh message #%d was added to queue.",
						(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
			} else {
				syslog(LOG_NOTICE,
						"[Radio] Can't add mesh message to queue!!!");
			}
			break;
		}
		break;
	default:
		syslog(LOG_NOTICE, "[Radio] Unhandled message of type %d(0x%02X)",
				msg_type, msg_type);
		break;
	}
	return res;
}

void BaseDataProcessor::RequestLocalRadioAddress() {
	if (!receiver_address_acquired
			&& ((RFN::Utilities::Utilities::GetUpTimeSeconds()
					- last_address_acuire_ts) > 5)
			&& (address_acuire_retry < 3)) {
		// Address not acquired, get radio version
		uint8_t get_sw_ver_msg[] = { 0x65, 0x00, 0x00, 0x09, 0xAA, 0xAA, 0x06,
				0x00, 0x00, 0x02, 0x00, 0x00, 0x5C };
		SendToRadioBitStuffing(get_sw_ver_msg, sizeof(get_sw_ver_msg));
		last_address_acuire_ts = RFN::Utilities::Utilities::GetUpTimeSeconds();
		address_acuire_retry++;
	}
}

void BaseDataProcessor::SendToRadioBitStuffing(uint8_t *data, size_t size) {
	uint8_t encoded_data[256];
	size_t new_size;
	RFN::Utilities::Utilities::BitStuffingEncode(data, size, encoded_data,
			&new_size);
	if (new_size > 0) {
		SendToRadioRaw(encoded_data, new_size);
	}
}

void BaseDataProcessor::FillSendBuffer() {
	QUEUE_ITEM item;
	int index = GetSendBufferEmptyIndex();
	size_t msgs_count = rf_queue.GetQueueMessageNumber();

	while (index > -1 && msgs_count > 0) {
		item = rf_queue.Dequeue();
		if (item.size < 1)
			break;
		// Copy data
		memcpy(send_buf[index].data, item.data, item.size);
		send_buf[index].size = item.size;
		send_buf[index].isLocal = item.isLocal;
		send_buf[index].last_sent_time = item.last_sent_time;
		send_buf[index].message_number = item.message_number;
		send_buf[index].retries = item.retries;
		send_buf[index].timestamp = item.timestamp;
		index = GetSendBufferEmptyIndex();
		msgs_count = rf_queue.GetQueueMessageNumber();
	}
}

int BaseDataProcessor::GetSendBufferEmptyIndex() {
	for (int i = 0; i < PARALLEL_MSGS_SIZE; i++) {
		if (send_buf[i].size == 0)
			return i;
		else if (send_buf[i].retries > MESSAGE_SEND_MAX_RETRIES) {
			// Remove item
			send_buf[i].size = 0;
			send_buf[i].retries = 0;
			send_buf[i].last_sent_time = 0;
			return i;
		}
	}
	return -1;
}

void BaseDataProcessor::StoreMessages() {
	rf_queue.SetSystemTimestamp(
			(uint32_t) RFN::Utilities::Utilities::GetUTCTimestamp());
	syslog(LOG_NOTICE, "Time saved: %s",
			RFN::Utilities::Utilities::TimestampUTCToString(
					(long) rf_queue.GetSystemTimestamp()).c_str());
	rf_queue.SaveToFile("/mnt/mmcblk0p1/queue.dat");

	// Save children list
	std::ofstream childrenFile("/mnt/mmcblk0p1/children.dat",
			std::ios::binary | std::ofstream::out | std::ofstream::trunc);
	if (childrenFile) {
		for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
			childrenFile.write((char*) &children[i], sizeof(CHILD_NODE));
		}
		childrenFile.close();
	}
}

void BaseDataProcessor::LoadMessages() {
	// Load children list
	int i = 0;
	std::ifstream childrenFile("/mnt/mmcblk0p1/children.dat",
			std::ios::binary | std::ifstream::in);
	if (childrenFile.is_open()) {
		while (childrenFile.good() && i < MAX_CHILDREN_NUM) {
			childrenFile.read((char*) &children[i++], sizeof(CHILD_NODE));
		}
		childrenFile.close();
		std::remove("/mnt/mmcblk0p1/children.dat");
	}

	// Load queue
	rf_queue.LoadFromFile("/mnt/mmcblk0p1/queue.dat");

	//Time Update
	time_t cur_time = RFN::Utilities::Utilities::GetUTCTimestamp();
	if ((cur_time < 1356998400L
			|| ((long) (cur_time) < (long) (rf_queue.GetSystemTimestamp())))
			&& rf_queue.GetSystemTimestamp() > 1356998400L) {
		// 01/01/2013
		// Need to update time
		std::string cmd("date -s '");
		char buf[20];
		time_t t = (time_t) (rf_queue.GetSystemTimestamp()) + 60;
		std::tm *_tm = std::gmtime(&t);
		std::strftime(buf, 20, "%Y-%m-%d %X", _tm);
		cmd += std::string(buf);
		cmd += "'";
		syslog(LOG_NOTICE, "Updating stored time: %s", cmd.c_str());
		system(cmd.c_str());
		sleep(3);
		system("/etc/init.d/sysntpd restart");
	}
}

uint16_t BaseDataProcessor::GetShortAddressByUnitID(uint32_t id) {
	for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
		if (children[i].id == id)
			return children[i].address;
	}
	return (id == 0 || id == 0xFFFFFFFFU) ? 0xFFFF : 0;
}

uint32_t BaseDataProcessor::GetUnitIDByShortAddress(uint16_t address) {
	for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
		if (children[i].address == address)
			return children[i].id;
	}
	return (address == 0 || address == 0xFFFF) ? 0xFFFFFFFFU : 0;
}

uint16_t BaseDataProcessor::GetChildrenNumber() {
	uint16_t count = 0;
	for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
		if (children[i].address > 0)
			count++;
	}
	return count;
}

void BaseDataProcessor::UpdateChildShortAddress(uint32_t id,
		uint16_t short_address) {
	int first_empty = MAX_CHILDREN_NUM + 1;
	for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
		if (children[i].id == id) {
			children[i].address = short_address;
			children[i].timestamp =
					RFN::Utilities::Utilities::GetUpTimeSeconds();
			// Delete all units that have te same short address
			for (int j = 0; j < MAX_CHILDREN_NUM; j++) {
				if (children[j].id != id
						&& children[j].address == short_address) {
					// Delete
					children[j].id = children[j].address =
							children[j].timestamp = 0;
				}
			}
			return;
		} else if ((children[i].id == 0) && (first_empty > MAX_CHILDREN_NUM)) {
			first_empty = i;
		}
	}
	if (first_empty < MAX_CHILDREN_NUM) {
		children[first_empty].id = id;
		children[first_empty].address = short_address;
		children[first_empty].timestamp =
				RFN::Utilities::Utilities::GetUpTimeSeconds();
	}
}

void BaseDataProcessor::UpdateChildLastSentTS(uint32_t id,
		uint16_t short_address) {
	for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
		if (children[i].id == id && children[i].address == short_address) {
			children[i].timestamp =
					RFN::Utilities::Utilities::GetUpTimeSeconds();
			return;
		}
	}
}

void BaseDataProcessor::ClearDisappearedUnits() {
	uint32_t cur_ts = RFN::Utilities::Utilities::GetUpTimeSeconds();
	for (int i = 0; i < MAX_CHILDREN_NUM; i++) {
		if (children[i].timestamp
				&& ((cur_ts - children[i].timestamp)
						> (MESH_CHILDREN_TIMEOUT_MIN * 60))) {
			children[i].timestamp = 0;
		}
	}
}

void BaseDataProcessor::CheckStoppingMesh() {
	size_t queue_size = rf_queue.GetQueueMessageNumber();

	if (enable_radio_network
			&& ((queue_size * 100 / MESSAGE_QUEUE_SIZE) >= 80)) {
		// Need to stop radio network
		enable_radio_network = false;
		EnableMeshSendData(enable_radio_network);
		syslog(LOG_NOTICE, "[Radio] Buffer almost full, stopping radio data.");
	} else if (!enable_radio_network
			&& ((queue_size * 100 / MESSAGE_QUEUE_SIZE) <= 60)) {
		// Can start radio network
		enable_radio_network = true;
		EnableMeshSendData(enable_radio_network);
		syslog(LOG_NOTICE, "[Radio] Buffer almost empty, starting radio data.");
	}
}

void BaseDataProcessor::EnableMeshSendData(bool enable) {
	uint16_t reg, tmp_crc;
	uint32_t val;
	uint8_t msg[] = { 0x96, 0x01, 0x0A, 0x12, 0x00, 0xFF, 0xFF, 0xFF, 0xFF,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	// Enable/Disable send by setting coordinator's registors 10 & 11
	reg = 0;
	val = (enable) ? 11 : 10;
	memcpy(msg + 9, &reg, 2);
	memcpy(msg + 11, &val, 4);
	tmp_crc = RFN::Utilities::Utilities::CalculateCRC16Xmodem(msg,
			sizeof(msg) - 2);
	memcpy(msg + sizeof(msg) - 2, &tmp_crc, 2);
	SendToRadioBitStuffing(msg, sizeof(msg));
}

void BaseDataProcessor::UpdateMeshSettings() {
	long cur_up_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
	if ((cur_up_time - last_config_msg_time) < 3) {
		return;
	} else if (config_retry > 2) {
		config_retry = 0;
		configuration_state++;
	}
	switch (configuration_state) {
	case 0:
		// Disable mesh network
		config_reg = 0;
		enable_radio_network = false;
		last_config_msg_time = cur_up_time;
		config_retry = 0;
		configuration_state++; // No need to wait for an acknowledgment, go to next step
		EnableMeshSendData(enable_radio_network);
		break;
	case 1:
		// Set channel
		config_reg = 3;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg,
				RFN::Utilities::Utilities::GetMeshChannel());
		break;
	case 2:
		// Set advertising channel
		config_reg = 15;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg,
				RFN::Utilities::Utilities::GetMeshAdvChannel());
		break;
	case 3:
		// Set Sync Word
		config_reg = 4;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg,
				RFN::Utilities::Utilities::GetMeshSyncWord());
		break;
	case 4:
		// Set Network ID
		config_reg = 6;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg,
				RFN::Utilities::Utilities::GetMeshNetworkID());
		break;
	case 5:
		// Set frames in cycle
		config_reg = 8;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg,
				RFN::Utilities::Utilities::GetMeshFramesInCycle());
		break;
	case 6:
		// Set base time
		config_reg = 10;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg,
				RFN::Utilities::Utilities::GetMeshBaseTime());
		break;
	case 7:
		// Set super-frame to 15
		config_reg = 9;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg, 15);
		break;
	case 8:
		// Set time divider 30 seconds
		config_reg = 17;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendSetRegisterRequestMessage(0xFFFFFFFFU, config_reg, 30);
		break;
	case 9:
		// Get version
		config_reg = 70;
		last_config_msg_time = cur_up_time;
		config_retry++;
		SendGetRegisterRequestMessage(0xFFFFFFFFU, config_reg);
		break;
	case 10:
		// Enable mesh network
		config_reg = 0;
		enable_radio_network = true;
		last_config_msg_time = cur_up_time;
		config_retry = 0;
		configuration_state++; // No need to wait for an acknowledgment, go to next step
		EnableMeshSendData(enable_radio_network);
		break;
	case 11:
		// Check if need to stop mesh network according message buffer
		config_reg = 0;
		last_config_msg_time = cur_up_time;
		config_retry = 0;
		configuration_state++; // No need to wait for an acknowledgment, go to next step
		CheckStoppingMesh();
		break;
	case 12:
		// Get network address for non mesh network
		RequestLocalRadioAddress();
		config_reg = 0;
		last_config_msg_time = cur_up_time;
		config_retry = 0;
		configuration_state++; // No need to wait for an acknowledgment, go to next step
		is_configuring = false;
		break;
	}
}

void BaseDataProcessor::SendSetRegisterRequestMessage(uint32_t address,
		uint16_t reg, uint32_t value) {
	uint16_t tmp_crc;
	uint8_t msg[] = { 0x96, 0x01, 0x0A, 0x12, 0x00, 0xFF, 0xFF, 0xFF, 0xFF,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	memcpy(msg + 5, &address, 4);
	memcpy(msg + 9, &reg, 2);
	memcpy(msg + 11, &value, 4);
	tmp_crc = RFN::Utilities::Utilities::CalculateCRC16Xmodem(msg,
			sizeof(msg) - 2);
	memcpy(msg + sizeof(msg) - 2, &tmp_crc, 2);
	SendToRadioBitStuffing(msg, sizeof(msg));
}

void BaseDataProcessor::SendGetRegisterRequestMessage(uint32_t address,
		uint16_t reg) {
	uint16_t tmp_crc;
	uint8_t msg[] = { 0x96, 0x01, 0x06, 0x10, 0x30, 0xFF, 0xFF, 0xFF, 0xFF,
			0x00, 0x00, 0x00, 0x00 };
	memcpy(msg + 5, &address, 4);
	memcpy(msg + 9, &reg, 2);
	tmp_crc = RFN::Utilities::Utilities::CalculateCRC16Xmodem(msg,
			sizeof(msg) - 2);
	memcpy(msg + sizeof(msg) - 2, &tmp_crc, 2);
	SendToRadioBitStuffing(msg, sizeof(msg));
}

void BaseDataProcessor::SendGetShortAdressResponse(uint16_t stored_address,
		uint8_t *payload, size_t size) {
	uint8_t msg[] = { 0x96, 0x01, 0x0B, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	uint16_t tmp_crc;
	msg[4] = radio_out_msg_num++;
	memcpy(msg + 5, payload, 9); // Copy ID, Address, Destination Address, IsRouter - total 9 bytes
	memcpy(msg + 14, &stored_address, 2); // Saved address
	tmp_crc = RFN::Utilities::Utilities::CalculateCRC16Xmodem(msg,
			sizeof(msg) - 2);
	memcpy(msg + sizeof(msg) - 2, &tmp_crc, 2);
	SendToRadioBitStuffing(msg, sizeof(msg));
}

std::string BaseDataProcessor::GetRadioVerstionStr() {
	return RFN::Utilities::Utilities::StringFormat("%u.%u.%u", radio_version[0],
			radio_version[1], radio_version[2]);
}

void BaseDataProcessor::CheckTimeouts() {
	// Check socket activity
	if ((last_send_time - last_ack_time) > 30) {
		// Socket inactive
		syslog(LOG_NOTICE,
				"[Network] Network activity timeout. Closing connection to server...");
		DisconnectFromServer();
		last_send_time = last_ack_time;
	}
	// Check if network is down and need to reboot the system.
	// It doesn't matter if the socket is opened or closed.
	if (reboot_after > 0 && socket_idle_interval > 0
			&& (RFN::Utilities::Utilities::GetUpTimeSeconds()
					- network_last_receive_time)
					> (socket_idle_interval * 60 * reboot_after)) {
		syslog(LOG_NOTICE, "[Network] Network inactive!!! Rebooting unit...");
		SafeReboot();
	}

	if ((RFN::Utilities::Utilities::GetUTCTimestamp() - last_mesh_sync_time)
			> 60) {
		// Sync time
		SendMeshTimeResponse();
		last_mesh_sync_time = RFN::Utilities::Utilities::GetUTCTimestamp();
		syslog(LOG_NOTICE, "[Radio] Updating time...");
	}
}

void BaseDataProcessor::SafeReboot() {
	signal(SIGTERM, SIG_DFL);
	StoreMessages();
	std::this_thread::sleep_for(std::chrono::seconds(3));
	RFN::Utilities::Utilities::exec("reboot");
}

void BaseDataProcessor::SendMeshTimeResponse() {
	uint8_t msg[] = { 0x96, 0x01, 0x04, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00 };
	long cur_ts = RFN::Utilities::Utilities::GetUTCTimestamp();
	msg[4] = radio_out_msg_num++;
	memcpy(msg + 5, &cur_ts, 4);
	uint16_t tmp_crc = RFN::Utilities::Utilities::CalculateCRC16Xmodem(msg,
			sizeof(msg) - 2);
	memcpy(msg + 9, &tmp_crc, 2);
	SendToRadioBitStuffing(msg, sizeof(msg));
}

void BaseDataProcessor::GenerateGPSMessage() {
	// Get GPS location
	uint8_t msg[] = { 0x10, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	struct gps_data_t gps_data;
	float latitude = NAN, longitute = NAN, altitude = NAN;
	long cur_up_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
	int8_t hdop = 127;
	if (RFN::Utilities::Utilities::GetGPS(&gps_data) == -1) {
		syslog(LOG_ERR, "[GPS] Can't get GPS location: Code - %d, Reason - %s",
		errno, gps_errstr(errno));
	}
	if ((gps_data.status == STATUS_FIX) && (gps_data.fix.mode == MODE_2D || gps_data.fix.mode == MODE_3D) &&
            !isnan(gps_data.fix.latitude) &&
            !isnan(gps_data.fix.longitude)) {
		latitude = gps_data.fix.latitude;
		longitute = gps_data.fix.longitude;
		altitude = gps_data.fix.altitude;
		hdop = (int8_t)gps_data.dop.hdop;
		syslog(LOG_NOTICE, "[GPS] Latitude: %f, Longitude: %f, Altitude: %f", latitude, longitute, altitude);
	} else {
		syslog(LOG_NOTICE, "[GPS] GPS data not available");
	}
	memcpy(msg + 3, &latitude, 4);
	memcpy(msg + 7, &longitute, 4);
	memcpy(msg + 11, &altitude, 4);
	msg[15] = hdop;
	msg[16] = RFN::Utilities::Utilities::CalculateChecksum(
			msg + 1, 15); // Checksum
	// Add to queue
	if (rf_queue.Enqueue(msg, sizeof(msg), true)) {
		syslog(LOG_NOTICE,
				"[GPS] GPS message #%d was added to queue.",
				(((int) rf_queue.GetCounter() - 1) & 0xFFFF));
	} else {
		syslog(LOG_NOTICE,
				"[GPS] Can't add GPS message to queue!!!");
	}
	last_gps_time = cur_up_time;
}

} /* namespace RFN */
