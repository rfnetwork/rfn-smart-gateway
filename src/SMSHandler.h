/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_SMSHANDLER_H_
#define SRC_SMSHANDLER_H_

#include <thread>
#include <unistd.h>
#include <dirent.h>
#include <string>
#include <future>
#include <gsmlib/gsm_me_ta.h>
#include <gsmlib/gsm_unix_serial.h>
#include "Exception.h"
#include "Defines.h"
#include "Utilities.h"

namespace RFN {

class RFNSMARTGateway;

class SMSHandler : public gsmlib::GsmEvent {
public:
	SMSHandler(RFNSMARTGateway *parent);
	virtual ~SMSHandler();

	/**
	 * @brief Start radio handler
	 */
	void Start();
	/**
	 * @brief Stop radio handler
	 */
	void Stop();

	struct IncomingMessage
	{
	  // used if new message is put into store
	  int _index;                   // -1 means message want send directly
	  std::string _storeName;
	  // used if SMS message was sent directly to TA
	  gsmlib::SMSMessageRef _newSMSMessage;
	  // used if CB message was sent directly to TA
	  gsmlib::CBMessageRef _newCBMessage;
	  // used in both cases
	  gsmlib::GsmEvent::SMSMessageType _messageType;

	  IncomingMessage() : _index(-1), _storeName(""), _messageType(gsmlib::GsmEvent::SMSMessageType::NormalSMS) {}
	};

	void SendSMS(std::string number, std::string message);

	int GetSignalStrength();

protected:
	void SMSThreadFunc();
	// inherited from GsmEvent
	void SMSReception(gsmlib::SMSMessageRef newMessage,
	                    gsmlib::GsmEvent::SMSMessageType messageType);
	void CBReception(gsmlib::CBMessageRef newMessage);
	void SMSReceptionIndication(std::string storeName, unsigned int index,
	                              gsmlib::GsmEvent::SMSMessageType messageType);
	void ringIndication();

	void ProcessSMSQueue();
	void HandleSMS(std::string number, std::string message);

	/// @brief Exception dispatcher
	Exception m_Exception;
	std::thread *thread;
	volatile bool running;
	RFNSMARTGateway *m_parent;
	gsmlib::MeTa *modem;
	gsmlib::MEInfo info;
	//std::vector<IncomingMessage> newMessages;
	std::string receiveStoreName;
	gsmlib::SMSStoreRef store;
	int signal_strength;
	uint8_t concatMsgID;
	//volatile bool needToProcessMessages;
};

} /* namespace RFN */

#endif /* SRC_SMSHANDLER_H_ */
