/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include <iostream>
#include <zlib.h>
#include "TelitDataProcessor.h"

namespace RFN {

TelitDataProcessor::TelitDataProcessor(RFNSMARTGateway *parent) :
		BaseDataProcessor(parent), client(nullptr), waiting_for_response(false), allowed_devices_list_loaded(
				false), info_sent(false), tags_list_offset(0), sending_attempts(
				0) {
	syslog(LOG_DEBUG, "Creating TelitDataProcessor...");
}

TelitDataProcessor::~TelitDataProcessor() {
	syslog(LOG_DEBUG, "Destroying TelitDataProcessor...");
}

void TelitDataProcessor::Start() {
	BaseDataProcessor::Start();
}

void TelitDataProcessor::Stop() {
	BaseDataProcessor::Stop();
}

void TelitDataProcessor::NetworkThreadFunc() {
	long last_connect_time = 0;
	memset(&item, 0, sizeof(QUEUE_ITEM));
	// Wait until can start thread
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	syslog(LOG_NOTICE, "[Network] Telit Data processor initialized.");

	token = RFN::Utilities::Utilities::GetTelitToken();
	cloudURL = RFN::Utilities::Utilities::GetTelitCloudURL();
	appID = RFN::Utilities::Utilities::GetTelitAppID();

	while (running) {
		CheckServerSettings();
		if (client == nullptr) {
			// Create new client instance
			if (can_start
					&& ((RFN::Utilities::Utilities::GetUpTimeSeconds()
							- last_connect_time) > 10)) {
				last_connect_time =
						RFN::Utilities::Utilities::GetUpTimeSeconds();
				tags_list_offset = 0;
				ConnectToCloud();
			}
			if (((RFN::Utilities::Utilities::GetUpTimeSeconds()
					- last_store_time) > 300)) {
				// Not connected to server. Store messages periodically.
				syslog(LOG_NOTICE,
						"[Network] Not connected to server. Storing messages to file...");
				StoreMessages();
				last_store_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
			}
		} else if (client && client->is_connected()) {
			network_last_receive_time =
					RFN::Utilities::Utilities::GetUpTimeSeconds();

			if (receiver_address_acquired || address_acuire_retry > 2) {
				if (!info_sent && !waiting_for_response) {
					// Send gateway info to server
					waiting_for_response = true;
					PublishGatewayInfo();
				}
				if (!allowed_devices_list_loaded && !waiting_for_response
						&& !tags_list_offset) {
					// Load allowed device list
					waiting_for_response = true;
					RequestAllowedDeviceList();
				}
			}

			if (allowed_devices_list_loaded) {
				if (item.size > 5) {
					long cur_time =
							RFN::Utilities::Utilities::GetUTCTimestamp();
					if (!item.last_sent_time
							|| (cur_time - item.last_sent_time)
									> SEND_MSG_TIME_OUT) {
						if (item.isLocal) {
							switch (item.data[1]) { // Message type
							case 0x1C:
								// GPS message
								item.last_sent_time = cur_time;
								if (item.size < 16) {
									rf_queue.Dequeue();
									item = rf_queue.GetHead();
								} else {
									float lat, lon;
									memcpy(&lat, item.data + 3, 4);
									memcpy(&lon, item.data + 7, 4);
									syslog(LOG_NOTICE,
											"[Network] GPS Message (Lat: %f, Long: %f)",
											lat, lon);
									SendLocation(lat, lon);
								}
								break;
							}
						} else {
							switch (item.data[5]) { // Message type
							case 0x01:
								// Tag messages send it
								item.last_sent_time = cur_time;
								SendTagMessage(&item);

								break;
							case 0xAA:
								// Receiver message
								syslog(LOG_NOTICE,
										"[Network] Queue Receiver Message (Type: 0x%02X, OpCode: 0x%02X)",
										item.data[10], item.data[11]);

								// Send
								item.last_sent_time = cur_time;
								SendReceiverMessage(&item);
								break;
							}
						}
					}
				} else if (item.size > 0 && item.size <= 5) {
					// Wrong message size discard message
					rf_queue.Dequeue();
					item = rf_queue.GetHead();
				} else {
					// Get next item
					item = rf_queue.GetHead();
				}
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	syslog(LOG_NOTICE, "[Network] Telit Data processor finished.");
}

void TelitDataProcessor::ProcessServerMessage(uint8_t *data, size_t size) {

}

void TelitDataProcessor::ConnectToCloud() {
	std::string imei = RFN::Utilities::Utilities::GetIMEI();
	if (!cloudURL.empty() && !appID.empty() && !token.empty(), !imei.empty()) {
		syslog(LOG_NOTICE, "[Network] Connecting to Telit cloud...");
		try {
			std::string host = RFN::Utilities::Utilities::StringFormat(
			TELIT_CLOUD_URL_TMP, cloudURL.c_str(), MOSQUITTO_PORT);
			client = new MQTTCloud(host, appID);
			client->Connected.connect([=]() {
				// Connected, subscribe to "reply" topic
				_isConnectedToServer = true;
				RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_ON);
				syslog(LOG_NOTICE, "[Network] Connected to Telit cloud.");
#if USE_ZLIB == 1
				client->subscribe("replyz/#", TELIT_QOS);
#else
				client->subscribe("reply/#", TELIT_QOS);
#endif
				client->subscribe("notify/#", TELIT_QOS);
			});
			client->Disconnected.connect([=]() {
				_isConnectedToServer = false;
				RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_OFF);
				if (client) {
					delete client;
					client = nullptr;
				}
				syslog(LOG_NOTICE, "[Network] Disconnected from Telit cloud.");
			});
			client->MessageReceived.connect(
					[=](std::string topic, const char *payload,
							size_t payload_len) {
#if USE_ZLIB == 1
						std::string t(topic);
						std::string p(payload);
						if (payload_len > 0
								&& t.find("replyz/") != std::string::npos)
							p = string_uncompress((uint8_t*) payload,
									payload_len, 2048);
						RFN::Utilities::Utilities::replace(t, "replyz/",
								"reply/");

						syslog(LOG_DEBUG, "Reply (%s): %s", t.c_str(),
								p.c_str());
						HandleServerData(t, p);
#else
				syslog(LOG_DEBUG, "Reply (%s): %s", topic, payload);
				HandleServerData(string(topic), string(payload));
#endif
					});
			if (!client->connect(imei, token)) {
				// Can't connect
				if (client) {
					delete client;
					client = nullptr;
				}
			}
		} catch (const mqtt::exception &exc) {
			syslog(LOG_ERR, "[Network] Error %s %s", exc.what(),
					exc.get_error_str().c_str());
			if (client) {
				delete client;
				client = nullptr;
			}
		} catch (std::exception &ex) {
			syslog(LOG_ERR, "[Network] Error %s", ex.what());
			if (client) {
				delete client;
				client = nullptr;
			}
		}
	}
}

void TelitDataProcessor::CloudSendMessage(std::string topic,
		std::string payload) {
	if (client != nullptr) {
#if USE_ZLIB == 1
		// Compress
		size_t compressed_size;
		RFN::Utilities::Utilities::replace(topic, "api/", "apiz/");
		syslog(LOG_DEBUG, "Request (%s): %s", topic.c_str(), payload.c_str());
		try {
			payload = string_compress(payload, &compressed_size);
		} catch (std::exception &ex) {
			syslog(LOG_ERR, "Error %s", ex.what());
		}
		if (compressed_size > 0)
			client->publish_binary(topic, payload.c_str(), compressed_size,
					TELIT_QOS);
#else
		syslog(LOG_DEBUG, "Request (%s): %s", topic.c_str(), payload.c_str());
		client->publish(topic.c_str(), payload.c_str(), TELIT_QOS);
#endif
	}
}

void TelitDataProcessor::HandleServerData(std::string topic,
		std::string payload) {
	// Publishing answer
	boost::property_tree::ptree in;
	if (strcmp(topic.c_str(), "reply/publish") == 0) {
		waiting_for_response = false;
		if (strstr(payload.c_str(), "true") > 0) {
			syslog(LOG_NOTICE, "[Network] Publish success");
			sending_attempts = 0;
			if (!info_sent)
				info_sent = true;
			else {
				// Remove from queue
				rf_queue.Dequeue();
				item = rf_queue.GetHead();
			}
		} else {
			syslog(LOG_ERR, "[Network] Publishing Error!!!");
		}
	} else if (strcmp(topic.c_str(), "notify/mailbox_activity") == 0) {
		// Mailbox notification received, read first
		std::string reply =
				"{\"1\" : { \"command\": \"mailbox.check\",\"params\": { \"autoComplete\": false, \"limit\": 1 }}}";
		CloudSendMessage("api/reply", reply);

		syslog(LOG_NOTICE, "Mailbox mailbox_activity !!!");
	} else if (strcmp(topic.c_str(), "reply/reply") == 0) {
		// Mailbox check reply
		std::istringstream iss(payload);
		try {
			boost::property_tree::read_json(iss, in);
			boost::optional<boost::property_tree::ptree&> messages =
					in.get_child_optional("1.params.messages.");
			if (messages) {
				BOOST_FOREACH(boost::property_tree::ptree::value_type &v, messages.get()) {
					std::string id = v.second.get("id", "");
					std::string cmd = v.second.get("command", "");
					if (cmd == "method.exec") {
						std::string method = v.second.get_child("params").get(
								"method", "");
						uint32_t tag_id = 0;
						std::string params_val = "";
						syslog(LOG_NOTICE,
								"Remote command received: %s (ID: %s)",
								method.c_str(), id.c_str());
						boost::optional<boost::property_tree::ptree&> params =
								v.second.get_child("params").get_child_optional(
										"params.");
						if (params) {
							tag_id = params.get().get<uint32_t>("tagid", 0);
//							BOOST_FOREACH(boost::property_tree::ptree::value_type &v1, params.get()) {
//								syslog(LOG_NOTICE, "?? %s", v1.first.c_str());
//							}
						}
						if (method == "addtag" && tag_id > 0) {
							// Add tag to allowed list
							allowed_tags.push_back(tag_id);
						} else if (method == "removetag") {
							// Remove tag from allowed list
							allowed_tags.erase(
									std::remove(allowed_tags.begin(),
											allowed_tags.end(), tag_id),
									allowed_tags.end());
						} else if (method == "gettags") {
							// Get tags list
							if (allowed_tags.empty())
								params_val =
										"\"completion_var\": \"notagsinlist.\"";
							else {
								params_val = "\"completion_var\": [";
								for (size_t i = 0; i < allowed_tags.size();
										i++) {
									params_val +=
											RFN::Utilities::Utilities::StringFormat(
													"%s\"%u\"",
													(i > 0) ? "," : "",
													allowed_tags[i]);
								}
								params_val += "]";
							}
						}
						// Send acknowledge
						std::string ack =
								"{\"1\":{\"command\": \"mailbox.ack\", \"params\": {";
						ack += RFN::Utilities::Utilities::StringFormat(
								"\"id\":\"%s\"", id.c_str());
						ack +=
								RFN::Utilities::Utilities::StringFormat(
										", \"errorCode\": 0, \"errorMessage\": \"\", \"params\": { %s }}}}",
										params_val.c_str());
						CloudSendMessage("api/reply", ack);
					}
				}
			}
		} catch (boost::exception &e) {
			syslog(LOG_ERR, "Reply: %s",
					boost::diagnostic_information(e).c_str());
		}
	}

	// GPS
	if (strcmp(topic.c_str(), "reply/gps") == 0) {
		waiting_for_response = false;
		if (item.isLocal && item.size > 1 && item.data[1] == 0x1C) {
			rf_queue.Dequeue();
			item = rf_queue.GetHead();
		}
	}

	// Allowed device list reply
	if (strcmp(topic.c_str(), "reply/thing/list") == 0) {
		waiting_for_response = false;
		//syslog(LOG_NOTICE, "[Network] Response: Topic - %s, Payload - %s", topic, payload);
		try {
			boost::property_tree::ptree jsontree;
			std::stringstream ss;
			ss << payload;
			boost::property_tree::read_json(ss, jsontree);

			if (tags_list_offset == 0) {
				allowed_tags.clear();
				syslog(LOG_NOTICE, "The list of allowed tags:");
			}
			int total_count = jsontree.get<int>("1.params.count", 0);
			for (boost::property_tree::ptree::value_type &it : jsontree.get_child(
					"1.params.result")) {
				for (boost::property_tree::ptree::value_type &it1 : it.second) {
					syslog(LOG_NOTICE, "Tag %s allowed",
							it1.second.data().c_str());
					allowed_tags.push_back(
							(uint32_t) atoi(it1.second.data().c_str()));
				}
			}
			tags_list_offset += 10;

			if ((total_count > 0) && (tags_list_offset < total_count)
					&& (tags_list_offset < 250)) {
				CloudSendMessage("api/thing/list",
						RFN::Utilities::Utilities::StringFormat(
								"{\"1\":{\"command\" : \"thing.search\", \"params\": {\"query\": \"defKey:tag\", \"limit\": 10, \"offset\": %d,  \"show\": [\"key\"]}}}",
								tags_list_offset).c_str());
				waiting_for_response = true;
			} else {
				syslog(LOG_NOTICE, "Loaded %d tags. Available memory %lu bytes",
						allowed_tags.size(),
						RFN::Utilities::Utilities::GetAvailableMemory());
				allowed_devices_list_loaded = true;
			}
		} catch (std::exception const &e) {
			syslog(LOG_ERR, "[%s - Network] Allowed devices reply error: %s",
					RFN::Utilities::Utilities::TimestampUTCToString(
							RFN::Utilities::Utilities::GetUTCTimestamp()).c_str(),
					e.what());
		}
	}
}

void TelitDataProcessor::RequestAllowedDeviceList() {
	tags_list_offset = 0;
	syslog(LOG_NOTICE, "[Network] Requesting allowed tag list...");
	CloudSendMessage("api/thing/list",
			"{\"1\":{\"command\" : \"thing.search\", \"params\": {\"query\": \"defKey:tag\", \"limit\": 10, \"offset\":0,  \"show\": [\"key\"], \"hideFields\": true}}}");
}

void TelitDataProcessor::PublishGatewayInfo() {
	std::string topic("api/publish");
	std::string data =
			RFN::Utilities::Utilities::StringFormat(
					"{ \"1\": { \"command\" : \"attribute.batch\",\"params\" : {\"ts\":\"%s\", \"thingKey\": \"%s\",\"data\":[",
					RFN::Utilities::Utilities::GetRFCTimestamp().c_str(),
					RFN::Utilities::Utilities::GetIMEI().c_str());
	data += RFN::Utilities::Utilities::StringFormat(
			"{\"key\": \"netAddr\",\"value\": \"%u\"},",
			local_receiver_address);
	data += RFN::Utilities::Utilities::StringFormat(
			"{\"key\": \"version\",\"value\": \"%d.%d.%d\"},",
			SOFTWARE_VERSION_MAJOR, SOFTWARE_VERSION_MINOR,
			SOFTWARE_VERSION_BUILD);

	data += RFN::Utilities::Utilities::StringFormat(
			"{\"key\": \"rdversion\",\"value\": \"%d.%d.%d\"}",
			radio_version[0], radio_version[1], radio_version[2]);

	data += "]}}}";
	syslog(LOG_NOTICE, "[Network] Publishing gateway information");
	CloudSendMessage(topic.c_str(), data.c_str());
}

void TelitDataProcessor::CheckServerSettings() {
	static long prevTs = 0;
	long curUpTime = RFN::Utilities::Utilities::GetUpTimeSeconds();
	if (!prevTs || ((curUpTime - prevTs) > NETWORK_SETTINGS_CHECK_PERIOD)) {
		// Check server settings for change
		syslog(LOG_NOTICE, "[Network] Check MQTT settings changing.");
		std::string tempToken = RFN::Utilities::Utilities::GetTelitToken();
		std::string tempCloudURL =
				RFN::Utilities::Utilities::GetTelitCloudURL();
		std::string tempAppID = RFN::Utilities::Utilities::GetTelitAppID();

		if ((strcmp(token.c_str(), tempToken.c_str()) != 0)
				|| (strcmp(cloudURL.c_str(), tempCloudURL.c_str()) != 0)
				|| (strcmp(appID.c_str(), tempAppID.c_str()) != 0)) {
			// Settings were changed, reconnect.
			token = tempToken;
			cloudURL = tempCloudURL;
			appID = tempAppID;
			if (client) {
				client->disconnect();
				delete client;
				client = nullptr;
			}
			allowed_devices_list_loaded = false;
			waiting_for_response = false;
			tags_list_offset = 0;
			allowed_tags.clear();
		}
		prevTs = curUpTime;
	}
}

void TelitDataProcessor::SendReceiverMessage(QUEUE_ITEM *item) {
	std::string data =
			RFN::Utilities::Utilities::StringFormat(
					"{ \"1\": { \"command\" : \"property.batch\",\"params\" : {\"ts\":\"%s\", \"thingKey\": \"%s\",\"data\":[",
					RFN::Utilities::Utilities::GetRFCTimestamp(item->timestamp).c_str(),
					RFN::Utilities::Utilities::GetIMEI().c_str());

	data += RFN::Utilities::Utilities::StringFormat(
			"{\"key\": \"rssi\",\"value\": %d}", -GetSignalStrength());
	if ((item->data[10] == 0x0D && item->data[11] == 0x0A)
			|| (item->data[10] == 0x0E
					&& (item->data[11] == 0x00 || item->data[11] == 0x01))) {
		// Voltage
		float voltage =
				(item->data[10] == 0x0E) ?
						((((uint16_t) item->data[16] << 8) + item->data[15])
								& 0xFFFF) * 0.001F :
						((((uint16_t) item->data[14] << 8) + item->data[13])
								& 0xFFFF) * 0.001F;
		data += RFN::Utilities::Utilities::StringFormat(
				",{\"key\": \"voltage\",\"value\": %.2f}", voltage);
	} else if (item->data[10] == 0x0D && item->data[11] == 0x10) {
		// External Voltage
		float ext_voltage = ((((uint16_t) item->data[14] << 8) + item->data[13])
				& 0xFFFF) * 0.001F;
		data += RFN::Utilities::Utilities::StringFormat(
				",{\"key\": \"externalvoltage\",\"value\": %.2f}", ext_voltage);
	} else if ((item->data[10] == 0x0D && item->data[11] == 0x0B)
			|| (item->data[10] == 0x0E
					&& (item->data[11] == 0x00 || item->data[11] == 0x01))) {
		// Temperature
		float temperature =
				(item->data[10] == 0x0E) ?
						item->data[17] * 0.5F : item->data[13] * 1.0F;
		data += RFN::Utilities::Utilities::StringFormat(
				",{\"key\": \"temperature\",\"value\": %.2f}", temperature);
	}
	data += "]}}}";

	syslog(LOG_NOTICE, "[Network] Publishing Receiver message");
	CloudSendMessage("api/publish", data);
}

void TelitDataProcessor::SendTagMessage(QUEUE_ITEM *item) {
	uint32_t tag_id;
	bool contains = false;
	std::string str_msg_type = "";
	std::string data = "{";

	for (uint8_t start_index = 7;
			start_index < item->size && start_index < item->data[4] + 4;
			start_index = start_index + 13) {
		memcpy(&tag_id, &item->data[start_index + 2], 4);
		if (ThingInList(tag_id)) {
			contains = true;
			data +=
					RFN::Utilities::Utilities::StringFormat(
							"%s\"%d\" : { \"command\" : \"property.batch\",\"params\" : {\"ts\":\"%s\", \"thingKey\": \"%u\",\"data\":[",
							((start_index == 7) ? "" : ","),
							((start_index - 7) / 13) + 1,
							RFN::Utilities::Utilities::GetRFCTimestamp(
									item->timestamp).c_str(), tag_id);
			uint8_t msg_type = item->data[start_index + 1] & 0x1F;
			data += RFN::Utilities::Utilities::StringFormat(
					"{\"key\": \"msgtype\",\"value\": %d}", msg_type);
			data += RFN::Utilities::Utilities::StringFormat(
					",{\"key\": \"gpio7\",\"value\": %d}",
					(item->data[start_index + 1] & 0x80) >> 7);
			data += RFN::Utilities::Utilities::StringFormat(
					",{\"key\": \"gpio8\",\"value\": %d}",
					(item->data[start_index + 1] & 0x20) >> 5);
			data += RFN::Utilities::Utilities::StringFormat(
					",{\"key\": \"gpio9\",\"value\": %d}",
					(item->data[start_index + 1] & 0x40) >> 6);
			data += RFN::Utilities::Utilities::StringFormat(
					",{\"key\": \"msgcount\",\"value\": %d}",
					item->data[start_index + 6]);

			if (msg_type == 3) {
				// Temperature
				str_msg_type = "Temperature";
				float voltage = (uint8_t) item->data[start_index + 11] / 62.5F;
				float temp = (((uint16_t) item->data[start_index + 10] << 8)
						+ (uint16_t) item->data[start_index + 9]) / 16.0F;
				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"voltage\", \"value\": %.2f}", voltage);
				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"temperature\", \"value\": %.3f}", temp);
			} else if (msg_type == 6 || msg_type == 12 || msg_type == 29
					|| msg_type == 1) {
				// Push button GPIO 9 or GPIO7 or GPIO8(Magnetic) or Periodic
				str_msg_type = (msg_type == 1) ? "Periodic" : "Push button";
				float voltage = 0;
				if (msg_type == 6) {
					uint16_t analogA = (((uint16_t) item->data[start_index + 10]
							<< 8) + (uint16_t) item->data[start_index + 9]);
					uint16_t analogB = (((uint16_t) item->data[start_index + 8]
							<< 8) + (uint16_t) item->data[start_index + 7]);
					data += RFN::Utilities::Utilities::StringFormat(
							",{\"key\": \"analog1\", \"value\": %u}", analogA);
					data += RFN::Utilities::Utilities::StringFormat(
							",{\"key\": \"analog2\", \"value\": %u}", analogB);
				}

				if (msg_type == 1) {
					voltage = ((uint8_t) item->data[start_index + 9]) / 62.5F;
				}
				if (voltage >= 1.9F && voltage <= 3.5F) {
					data += RFN::Utilities::Utilities::StringFormat(
							",{\"key\": \"voltage\", \"value\": %.2f}",
							voltage);
				}
			} else if (msg_type == 9) {
				// Movement
				str_msg_type = "Movement";
				int8_t x = (int8_t) item->data[start_index + 9];
				int8_t y = (int8_t) item->data[start_index + 10];
				int8_t z = (int8_t) item->data[start_index + 11];
				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"x\", \"value\": %d}", x);
				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"y\", \"value\": %d}", y);
				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"z\", \"value\": %d}", z);
			} else if (msg_type == 14 || msg_type == 17) {
				// Analog / Analog counter
				str_msg_type = "Analog";
				float voltage = (int8_t) item->data[start_index + 11] / 62.5F;
				uint16_t analogA =
						(((int16_t) item->data[start_index + 10] << 8)
								+ (int16_t) item->data[start_index + 9]);
				uint16_t analogB = (((int16_t) item->data[start_index + 8] << 8)
						+ (int16_t) item->data[start_index + 7]);
				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"analog1\", \"value\": %d}", analogA);
				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"%s\", \"value\": %d}",
						(msg_type == 17) ? "counter" : "analog2", analogB);
				if (voltage >= 1.9F && voltage <= 3.5F) {
					data += RFN::Utilities::Utilities::StringFormat(
							",{\"key\": \"voltage\", \"value\": %.2f}",
							voltage);
				}
			} else if (msg_type == 2) {
				// Counter
				str_msg_type = "Counter";
				float voltage = (int8_t) item->data[start_index + 11] / 62.5F;
				uint32_t counterVal = (((uint32_t) item->data[start_index + 10]
						<< 24) + ((uint32_t) item->data[start_index + 9] << 16)
						+ ((uint32_t) item->data[start_index + 8] << 8)
						+ (uint32_t) item->data[start_index + 7]);

				data += RFN::Utilities::Utilities::StringFormat(
						",{\"key\": \"counter\", \"value\": %lu}", counterVal);
				if (voltage >= 1.9F && voltage <= 3.5F) {
					data += RFN::Utilities::Utilities::StringFormat(
							",{\"key\": \"voltage\", \"value\": %.2f}",
							voltage);
				}
			}

			data += RFN::Utilities::Utilities::StringFormat(
					",{\"key\": \"imei\",\"value\": %s}",
					RFN::Utilities::Utilities::GetIMEI().c_str());
			data += RFN::Utilities::Utilities::StringFormat(
					",{\"key\": \"rssi\",\"value\": %.1f}",
					(item->data[start_index + 12] / 2.0f) - 120);
			data += "]}}";
//			syslog(LOG_NOTICE, "Ind %d %s", start_index,
//					RFN::Utilities::Utilities::HexArrayToString(
//							&item->data[start_index], 13).c_str());
			syslog(LOG_NOTICE, "[Network] Publishing Tag %u %smessage", tag_id,
					(str_msg_type.empty()) ? "" : (str_msg_type + " ").c_str());
		} else {
			syslog(LOG_NOTICE,
					"[Network] Tag %u message received. Tag is not in allowed list, discarding message",
					tag_id);
		}
	}
	data += "}";

	if (contains) {
		CloudSendMessage("api/publish", data);
	} else {
		rf_queue.Dequeue();
		*item = rf_queue.GetHead();
	}
}

void TelitDataProcessor::SendLocation(float latitude, float longitude) {
	std::string location_msg =
			"{\"1\":{\"command\":\"location.publish\",\"params\":";
	location_msg += "{\"thingKey\":\"" + RFN::Utilities::Utilities::GetIMEI()
			+ "\",\"lat\":";
	location_msg += RFN::Utilities::Utilities::StringFormat("%.6f", latitude)
			+ ",\"lng\":";
	location_msg += RFN::Utilities::Utilities::StringFormat("%.6f", longitude)
			+ ",\"fixType\": \"manual\",\"fixAcc\" : 900}}}";

	CloudSendMessage("api/gps", location_msg.c_str());
}

#if USE_ZLIB == 1
std::string TelitDataProcessor::string_compress(std::string const &s,
		size_t *size) {
	*size = 0;
	unsigned int sourceSize = s.size();
	const char *source = s.c_str();

	unsigned long dsize = sourceSize + (sourceSize * 0.1f) + 16;
	char *destination = new char[dsize];

	int result = compress((unsigned char*) destination, &dsize,
			(const unsigned char*) source, sourceSize);

	if (result != Z_OK)
		throw std::runtime_error(
				RFN::Utilities::Utilities::StringFormat(
						"Compress error occured! Error code: %s", result));

	*size = dsize;
	return std::string(destination, dsize);
}

std::string TelitDataProcessor::string_uncompress(uint8_t *s, size_t size,
		unsigned long len) {
	char *destination = new char[len];

	int result = uncompress((unsigned char*) destination, &len, s, size);

	if (result != Z_OK)
		throw std::runtime_error(
				RFN::Utilities::Utilities::StringFormat(
						"Uncompress error occured! Error code: %s", result));

	return std::string(destination, len);
}

#endif

} /* namespace RFN */

