/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SMSHandler.h"
#include "RFN-SMART-Gateway.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/range/iterator_range.hpp>
#include <gsmlib/gsm_error.h>
#include <gsmlib/gsm_event.h>

namespace RFN {

SMSHandler::SMSHandler(RFNSMARTGateway *parent)
try :
		m_Exception(__func__), thread(nullptr), running(false), m_parent(
				parent), modem(nullptr), signal_strength(0), concatMsgID(0) {

}
catch (...) {
	m_Exception.Dispatch();
}

SMSHandler::~SMSHandler() {
	if (modem != nullptr)
		delete modem;
}

void SMSHandler::Start() {
	running = true;
	thread = new std::thread(std::bind(&SMSHandler::SMSThreadFunc, this));
}

void SMSHandler::Stop() {
	// Stop and destroy thread
	running = false;
	if (thread != nullptr) {
		thread->join();
		delete thread;
		thread = nullptr;
	}
}

void SMSHandler::SMSThreadFunc() {
	gsmlib::UnixSerialPort *port = nullptr;
	long prevTs = 0;
	int i, initializeRetries = 0;
	struct timeval timeoutVal;
	timeoutVal.tv_sec = 5;
	timeoutVal.tv_usec = 0;

	syslog(LOG_NOTICE, "[SMS] Initialized.");

	if (RFN::Utilities::Utilities::NoModemDefined()) {
		std::string imei = std::to_string(
				RFN::Utilities::Utilities::GetMacAddress());
		RFN::Utilities::Utilities::SetIMEI(imei);
		syslog(LOG_NOTICE, "[SMS] MAC address %s is set as IMEI", imei.c_str());
		m_parent->GetProcessor()->CanStart(true);
	}

	while (running) {
		if (!RFN::Utilities::Utilities::NoModemDefined()) {
			long curUpTime = RFN::Utilities::Utilities::GetUpTimeSeconds();
			if ((initializeRetries < SMS_MAX_INIT_RETRIES)
					&& (!prevTs
							|| ((curUpTime - prevTs) >= SMS_CHECK_INTERVAL_SEC))) {
				prevTs = curUpTime;
				if (modem == nullptr) {
					try {
						port = new gsmlib::UnixSerialPort(SMS_MODEM_DEVICE,
						B115200);
						if (port) {
							//port->setTimeOut(5);
							modem = new gsmlib::MeTa(port);
							std::string dummy1, dummy2;
							modem->getSMSStore(dummy1, dummy2,
									receiveStoreName);
							modem->setMessageService(1);
							modem->setSMSRoutingToTA(true, true, true);
							modem->setEventHandler(this);
						}
					} catch (gsmlib::GsmException &ex) {
						if (ex.getErrorCode() == 310) {
							// SIM not inserted, reset retries
							syslog(LOG_ERR, "SIM not present");
						} else {
							syslog(LOG_ERR, "Can't initialize modem (%s)",
									gsmlib::getMEErrorText(ex.getErrorCode()).c_str());
							if (modem)
								delete modem;
							modem = nullptr;

							if (port)
								delete port;
							port = nullptr;
						}
					}
					if (modem) {
						info = modem->getMEInfo();
						if (RFN::Utilities::Utilities::GetIMEI()
								!= info._serialNumber
								&& !info._serialNumber.empty()) {
							RFN::Utilities::Utilities::SetIMEI(
									info._serialNumber);
						}
						syslog(LOG_NOTICE, "[SMS] Modem %s %s, IMEI %s",
								info._manufacturer.c_str(), info._model.c_str(),
								info._serialNumber.c_str());
						m_parent->GetProcessor()->CanStart(true);
						signal_strength = modem->getSignalStrength();
						ProcessSMSQueue();
					} else {
						initializeRetries++;
						syslog(LOG_ERR, "Can't initialize modem!!!");

						if (initializeRetries >= SMS_MAX_INIT_RETRIES) {
							std::string imei = std::to_string(
									RFN::Utilities::Utilities::GetMacAddress());
							RFN::Utilities::Utilities::SetIMEI(imei);
							syslog(LOG_NOTICE,
									"[SMS] MAC address %s is set as IMEI",
									imei.c_str());
							m_parent->GetProcessor()->CanStart(true);
						} else {
							for (i = 0; i < 10 && running; i++)
								std::this_thread::sleep_for(
										std::chrono::seconds(1));
						}
					}
				} else {
					signal_strength = modem->getSignalStrength();
					ProcessSMSQueue();
				}

				if (modem)
					modem->waitEvent(&timeoutVal);
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	syslog(LOG_NOTICE, "[SMS] Finished.");
}

void SMSHandler::SMSReception(gsmlib::SMSMessageRef newMessage,
		gsmlib::GsmEvent::SMSMessageType messageType) {

}

void SMSHandler::CBReception(gsmlib::CBMessageRef newMessage) {

}

void SMSHandler::SMSReceptionIndication(std::string storeName,
		unsigned int index, gsmlib::GsmEvent::SMSMessageType messageType) {

}

void SMSHandler::ringIndication() {
	syslog(LOG_NOTICE, "RINGING!!!");
}

void SMSHandler::ProcessSMSQueue() {
	gsmlib::SMSStoreRef store = modem->getSMSStore(receiveStoreName);
	store->setCaching(false);
	for (gsmlib::SMSStore::iterator s = store->begin();
			s != store->end() && running; ++s) {
		if (!s->empty()) {
			if (!s->message().isnull()) {
				// Handle SMS
				std::string number = s->message()->address().toString();
				std::string msg = s->message()->userData();
				store->erase(s);
				if (boost::starts_with(number, "+")) // Ignore service messages
					HandleSMS(number, msg);
			} else {
				store->erase(s);
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
	}
}

void SMSHandler::HandleSMS(std::string number, std::string message) {
	std::string reply = "Syntax error";
	bool need_to_reload = false;
	bool need_to_reset = false;
	syslog(LOG_NOTICE, "SMS received from %s: %s", number.c_str(),
			message.c_str());
	// Validate message
	if (message.size() > 4) {
		std::string pass = message.substr(0, 4);
		std::string content = message.substr(4, message.size() - 4);
		std::string lower_content = std::string(content);
		boost::algorithm::to_lower(lower_content);
		if (pass == RFN::Utilities::Utilities::GetSMSPass()) {
			// Pass OK
			if (boost::starts_with(content, "0:")) {
				// Set APN
				std::string cmd = content;
				boost::replace_first(cmd, "0:", "");
				std::vector<std::string> strs;
				boost::split(strs, cmd, boost::is_any_of(","));
				if (strs.size() == 3 && !strs[0].empty()) {
					if (strs[1].empty())
						strs[1] = "";
					if (strs[2].empty())
						strs[2] = "";
					if (RFN::Utilities::Utilities::Contains3GSetting()) {
						// Set 3G
						RFN::Utilities::Utilities::exec("ifdown 3G");
						reply = (RFN::Utilities::Utilities::Set3GSettings(
								strs[0], strs[1], strs[2])) ?
								content + " OK" : "";
						RFN::Utilities::Utilities::exec("ifup 3G");
					} else if (RFN::Utilities::Utilities::Contains4GSetting()) {
						// Set 4G
						RFN::Utilities::Utilities::exec("ifdown 4G");
						reply = (RFN::Utilities::Utilities::Set4GSettings(
								strs[0], strs[1], strs[2])) ?
								content + " OK" : "";
						RFN::Utilities::Utilities::exec("ifup 4G");
					} else {
						reply = "No cellular data connection available";
					}
				}
			} else if (boost::starts_with(content, "1:")) {
				// Set server IP
				std::string cmd = content;
				boost::replace_first(cmd, "1:", "");
				std::vector<std::string> strs;
				boost::split(strs, cmd, boost::is_any_of(","));
				if (strs.size() == 2 && !strs[0].empty() && !strs[1].empty()) {
					reply = (RFN::Utilities::Utilities::SetServerIP(strs[0])
							&& RFN::Utilities::Utilities::SetServerPort(
									(uint16_t) std::stoi(strs[1]))) ?
							content + " OK" : "Can't set server port";
				}
			} else if (boost::starts_with(lower_content, "mode:")) {
				// Set working mode
				std::string cmd = lower_content;
				boost::replace_first(cmd, "mode:", "");
				WorkingMode old_mode = RFN::Utilities::Utilities::GetMode();
				int new_mode =
						(!RFN::Utilities::Utilities::StringIsNumber(cmd)) ?
								-1 : std::stoi(cmd);
				if (new_mode < 0 || new_mode >= MODE_MAX
						|| !RFN::Utilities::Utilities::SetMode(
								(WorkingMode) new_mode)) {
					reply = "Can't set working mode";
				} else {
					reply = content + " OK";
					if (old_mode != new_mode)
						std::async(&RFNSMARTGateway::ChangeMode, m_parent,
								(WorkingMode) new_mode);
					//m_parent->ChangeMode((WorkingMode)new_mode);
				}
			} else if (boost::starts_with(lower_content, "info")) {
				// Get info
				reply = "";
				reply +=
						RFN::Utilities::Utilities::StringFormat(
								"0: %s,%s,%s\r\n",
								RFN::Utilities::Utilities::GetAPN().c_str(),
								RFN::Utilities::Utilities::GetCellularUsername().c_str(),
								RFN::Utilities::Utilities::GetCellularPassword().c_str());
				reply += RFN::Utilities::Utilities::StringFormat("1: %s,%d\r\n",
						RFN::Utilities::Utilities::GetServerIP().c_str(),
						RFN::Utilities::Utilities::GetServerPort());
				reply += RFN::Utilities::Utilities::StringFormat("Mode: %s\r\n",
						RFN::Utilities::Utilities::WorkingModeToString(
								RFN::Utilities::Utilities::GetMode()).c_str());
				reply +=
						RFN::Utilities::Utilities::StringFormat(
								"Accelerometer Sensitivity: %d\r\n",
								RFN::Utilities::Utilities::GetAccelerometerSensitivity());
				reply +=
						RFN::Utilities::Utilities::StringFormat(
								"GPS: %d,%d\r\n",
								RFN::Utilities::Utilities::GetGPSMovementThreshold(),
								RFN::Utilities::Utilities::GetGPSPeriod());
				reply += RFN::Utilities::Utilities::StringFormat(
						"\r\nCloud URL: %s\r\n",
						RFN::Utilities::Utilities::GetTelitCloudURL().c_str());
				reply += RFN::Utilities::Utilities::StringFormat(
						"App ID: %s\r\n",
						RFN::Utilities::Utilities::GetTelitAppID().c_str());
				reply += RFN::Utilities::Utilities::StringFormat(
						"App Token: %s\r\n",
						RFN::Utilities::Utilities::GetTelitToken().c_str());
				reply += RFN::Utilities::Utilities::StringFormat(
						"Mesh settings: %d,%d,%d,%d,%d,%d",
						RFN::Utilities::Utilities::GetMeshChannel(),
						RFN::Utilities::Utilities::GetMeshAdvChannel(),
						RFN::Utilities::Utilities::GetMeshBaseTime(),
						RFN::Utilities::Utilities::GetMeshFramesInCycle(),
						RFN::Utilities::Utilities::GetMeshSyncWord(),
						RFN::Utilities::Utilities::GetMeshNetworkID());
			} else if (boost::starts_with(lower_content, "signal")) {
				// Get reception level
				reply = RFN::Utilities::Utilities::StringFormat(
						"Signal: -%ddBm", GetSignalStrength());
			} else if (boost::starts_with(lower_content, "fwver")) {
				// Get modem firmware version
				reply = RFN::Utilities::Utilities::StringFormat(
						"Firmware Version: %s", info._revision.c_str());
			} else if (boost::starts_with(lower_content, "ver")) {
				// Get version
				std::string rd_ver("");
				BaseDataProcessor *p = m_parent->GetProcessor();
				if (p) {
					rd_ver = RFN::Utilities::Utilities::StringFormat(
							", Radio: %s", p->GetRadioVerstionStr().c_str());
				}
				reply = RFN::Utilities::Utilities::StringFormat(
						"RFN-Smart-Gateway-%s%s", SOFTWARE_VERSION,
						rd_ver.c_str());
			} else if (boost::starts_with(lower_content, "imei")) {
				// Get modem IMEI
				reply = RFN::Utilities::Utilities::StringFormat("IMEI: %s",
						info._serialNumber.c_str());
			} else if (boost::starts_with(lower_content, "model")) {
				// Get modem model
				reply = RFN::Utilities::Utilities::StringFormat("Model: %s %s",
						info._manufacturer.c_str(), info._model.c_str());
			} else if (boost::starts_with(lower_content, "np:")) {
				// Change password
				std::string pass =
						(content.size() > 3) ?
								content.substr(3, content.size() - 3) : "";
				if (pass.empty() || pass.length() != 4) {
					reply = "New password should be 4 characters long";
				} else {
					reply = (RFN::Utilities::Utilities::SetSMSPass(pass)) ?
							content + " OK" : "Can't store password";
				}
			} else if (boost::starts_with(lower_content, "cloudurl:")) {
				// Set Telit Cloud application URL
				std::string url =
						(content.size() > 9) ?
								content.substr(9, content.size() - 9) : "";
				if (url.empty()) {
					reply = "URL can not be empty";
				} else {
					reply = (RFN::Utilities::Utilities::SetTelitCloudURL(url)) ?
							content + " OK" : "Can't store URL";
				}
			} else if (boost::starts_with(lower_content, "apptoken:")) {
				// Set Telit Cloud application token
				std::string token =
						(content.size() > 9) ?
								content.substr(9, content.size() - 9) : "";
				if (token.empty()) {
					reply = "Token can not be empty";
				} else {
					reply = (RFN::Utilities::Utilities::SetTelitToken(token)) ?
							content + " OK" : "Can't store token";
				}
			} else if (boost::starts_with(lower_content, "appid:")) {
				// Set Telit Cloud application ID
				std::string appID =
						(content.size() > 9) ?
								content.substr(6, content.size() - 6) : "";
				if (appID.empty()) {
					reply = "Application ID can not be empty";
				} else {
					reply = (RFN::Utilities::Utilities::SetTelitAppID(appID)) ?
							content + " OK" : "Can't store application ID";
				}
			} else if (boost::starts_with(lower_content, "accsence:")) {
				// Set accelerometer sensitivity
				std::string cmd = lower_content;
				boost::replace_first(cmd, "accsence:", "");
				int new_sensitivity =
						(!RFN::Utilities::Utilities::StringIsNumber(cmd)) ?
								-1 : std::stoi(cmd);
				if (new_sensitivity > -1 && new_sensitivity < 128) {
					reply =
							(RFN::Utilities::Utilities::SetAccelerometerSensitivity(
									new_sensitivity)) ?
									content + " OK" :
									"Can't set accelerometer sensitivity";
				} else {
					reply =
							"Accelerometer sensitivity should be between 0 and 127";
				}
			} else if (boost::starts_with(lower_content, "gps:")) {
				// GPS threshold and period
				std::string cmd = content;
				boost::replace_first(cmd, "gps:", "");
				std::vector<std::string> strs;
				boost::split(strs, cmd, boost::is_any_of(","));
				if (strs.size() == 2 && !strs[0].empty() && !strs[1].empty()) {
					int threshold = RFN::Utilities::Utilities::StringIsNumber(strs[0])? std::stoi(strs[0]) : -1;
					int period = RFN::Utilities::Utilities::StringIsNumber(strs[0])? std::stoi(strs[1]) : -1;

					reply = (threshold > -1 && threshold < 86400 && period > -1 && period < 86400 &&
							RFN::Utilities::Utilities::SetGPSMovementThreshold((uint32_t)threshold) &&
							RFN::Utilities::Utilities::SetGPSPeriod((uint32_t)period)) ? "OK" : "Can't set GPS settings";
				}
			} else if (boost::starts_with(lower_content, "gps")) {
				// Get GPS location
				struct gps_data_t gps_data;
				if (RFN::Utilities::Utilities::GetGPS(&gps_data) != -1 && (gps_data.status == STATUS_FIX) && (gps_data.fix.mode == MODE_2D || gps_data.fix.mode == MODE_3D) &&
						!isnan(gps_data.fix.latitude) &&
						!isnan(gps_data.fix.longitude)) {
					reply = RFN::Utilities::Utilities::StringFormat("Position:\r\nhttp://maps.google.com/?q=%.10f,%.10f", gps_data.fix.latitude, gps_data.fix.longitude);
				} else {
					reply = "Can't retrieve GPS location";
				}
			} else if (boost::starts_with(lower_content, "upgraderadio:")) {
				// Update radio firmware
				std::string cmd = lower_content;
				boost::replace_first(cmd, "upgraderadio:", "");
				int fw_type =
						(!RFN::Utilities::Utilities::StringIsNumber(cmd)) ?
								-1 : std::stoi(cmd);
				reply = (m_parent->UpdateRadioFW(fw_type)) ?
						content + " OK" : "Can't upgrade radio";
			} else if (boost::starts_with(lower_content, "mesh:")) {
				// Mesh settings
				std::string cmd = lower_content;
				boost::replace_first(cmd, "mesh:", "");
				std::vector<std::string> strs;
				boost::split(strs, cmd, boost::is_any_of(","));
				if (strs.size() == 6 && !strs[0].empty() && !strs[1].empty()
						&& !strs[2].empty() && !strs[3].empty()
						&& !strs[4].empty() && !strs[5].empty()) {
					reply = (RFN::Utilities::Utilities::SetMeshChannel(
							(uint8_t) std::stoi(strs[0]))
							&& RFN::Utilities::Utilities::SetMeshAdvChannel(
									(uint8_t) std::stoi(strs[1]))
							&& RFN::Utilities::Utilities::SetMeshBaseTime(
									(uint8_t) std::stoi(strs[2]))
							&& RFN::Utilities::Utilities::SetMeshFramesInCycle(
									(uint8_t) std::stoi(strs[3]))
							&& RFN::Utilities::Utilities::SetMeshSyncWord(
									(uint16_t) std::stoi(strs[4]))
							&& RFN::Utilities::Utilities::SetMeshNetworkID(
									(uint8_t) std::stoi(strs[5]))) ?
							content + " OK" : "Can't set server port";
				}
				need_to_reload = true;
			} else if (boost::starts_with(lower_content, "reset")) {
				// Reset
				reply = content + " OK";
				need_to_reset = true;
			} else {
				reply = "Unknown command";
			}
		} else {
			reply = "Wrong password";
		}
	}

	//syslog(LOG_NOTICE, "SMS response to %s: %s", number.c_str(), reply.c_str());
	SendSMS(number, reply);
	if (need_to_reload) {
		std::this_thread::sleep_for(std::chrono::seconds(3));
		m_parent->RestartProcessor();
		m_parent->GetProcessor()->CanStart(true);
	}
	if (need_to_reset) {
		// Safe reset
		syslog(LOG_NOTICE, "[SMS] Rebooting unit...");
		m_parent->GetProcessor()->SafeReboot();
	}
}

void SMSHandler::SendSMS(std::string number, std::string message) {
	syslog(LOG_NOTICE, "Sending SMS to %s: %s", number.c_str(),
			message.c_str());
	gsmlib::Ref<gsmlib::SMSSubmitMessage> submitSMS =
			new gsmlib::SMSSubmitMessage();
	gsmlib::Address destAddr(number);
	submitSMS->setDestinationAddress(destAddr);
	modem->sendSMSs(submitSMS, message, false, concatMsgID++);
}

int SMSHandler::GetSignalStrength() {
	return signal_strength;
}

} /* namespace RFN */
