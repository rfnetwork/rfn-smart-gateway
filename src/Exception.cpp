/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Exception.h"

namespace RFN {

Exception::Exception(const char* message) : m_Message(message) {}

void Exception::Dispatch(const char* message)
{
	// Message to log
	std::string log;

	// Begin formatting. Ctor string is usually a class name.
	if (!m_Message.empty())
	{
		log += m_Message + ": ";
	}

	// Complete the dispatched message
	std::string msg(message);
	if (!msg.empty())
	{
		log += msg;
		log += ": ";
	}

	// Throw original exception
	try {
		throw;
	} catch (const boost::program_options::error& ex) {
		syslog(LOG_ERR, "%sprogram option exception: '%s'", log.c_str(), ex.what());
	} catch (const std::exception& ex) {
		syslog(LOG_ERR, "%sstandard exception: '%s'", log.c_str(), ex.what());
	} catch (...) {
		syslog(LOG_ERR, "%sunknown exception", log.c_str());
	}
}

} /* namespace RFN */

