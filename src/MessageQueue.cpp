/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MessageQueue.h"
#include "Utilities.h"

MessageQueue::MessageQueue(size_t size) : queue_size(size), queue_items_number(0), counter(0), timestamp(0) {

}

MessageQueue::~MessageQueue() {
	// Empty queue
	msg_queue.empty();
}

unsigned short MessageQueue::GetCounter() {
	unsigned short ret;
	mtx.lock();
	ret = counter;
	mtx.unlock();
	return ret;
}

size_t MessageQueue::GetQueueMessageNumber() {
	size_t ret;
	mtx.lock();
	ret = queue_items_number;
	mtx.unlock();
	return ret;
}

uint32_t MessageQueue::GetSystemTimestamp() {
	uint32_t ret;
	mtx.lock();
	ret = timestamp;
	mtx.unlock();
	return ret;
}

void MessageQueue::SetSystemTimestamp(uint32_t ts) {
	mtx.lock();
	timestamp = ts;
	mtx.unlock();
}

size_t MessageQueue::Enqueue(unsigned char *data, size_t size, bool isLocal, long cur_timestamp) {
	QUEUE_ITEM item;

	if (size < 1 || size > MAX_RF_BUFF || data == NULL)
		return 0; // Wrong parameters passed

	mtx.lock();
	if (queue_items_number >= this->queue_size) {
		// Queue is full. Pop first item
		msg_queue.pop();
		queue_items_number--;
	}
	// Add item.
	item.timestamp = cur_timestamp;
	item.isLocal = isLocal;
	item.size = size;
	if (memcpy(item.data, data, size) == NULL) {
		// Failed to copy data.
		mtx.unlock();
		return 0;
	}
	item.message_number = counter;
	item.retries = 0;
	item.last_sent_time = 0;
	msg_queue.push(item);
	queue_items_number++;
	counter++;
	mtx.unlock();
	return size;
}

QUEUE_ITEM MessageQueue::Dequeue() {
	QUEUE_ITEM ret;
	ret.size = 0;
	mtx.lock();
	if (queue_items_number > 0) {
		ret = msg_queue.front();
		msg_queue.pop();
		queue_items_number--;
	}
	mtx.unlock();
	return ret;
}

QUEUE_ITEM MessageQueue::GetHead() {
	QUEUE_ITEM ret;
	ret.size = 0;
	mtx.lock();
	if (queue_items_number > 0) {
		ret = msg_queue.front();
	}
	mtx.unlock();
	return ret;
}

void MessageQueue::SaveToFile(const char *filename) {
	std::queue<QUEUE_ITEM> tmp_q = msg_queue; //copy the original queue to the temporary queue

	//if (tmp_q.empty())
	//	return;

	std::ofstream myfile(filename, std::ios::binary | std::ofstream::out | std::ofstream::trunc);

	myfile.write((char *)&queue_size,sizeof(queue_size));
	myfile.write((char *)&queue_items_number,sizeof(queue_items_number));
	myfile.write((char *)&counter,sizeof(counter));
	myfile.write((char *)&timestamp,sizeof(timestamp));
	while (!tmp_q.empty())
	{
		QUEUE_ITEM item = tmp_q.front();
		myfile.write((char *)&item.size,sizeof(item.size));
		myfile.write((char *)&item.isLocal,sizeof(item.isLocal));
		myfile.write((char *)&item.message_number ,sizeof(item.message_number));
		myfile.write((char *)&item.timestamp,sizeof(item.timestamp));
		myfile.write((char *)item.data,item.size);

	    tmp_q.pop();
	}
	myfile.close();
}

void MessageQueue::LoadFromFile(const char *filename) {
	std::ifstream myfile(filename, std::ios::binary | std::ifstream::in);
	if (myfile.is_open()) {
		myfile.read((char*)&queue_size, sizeof(queue_size));
		myfile.read((char*)&queue_items_number, sizeof(queue_items_number));
		myfile.read((char*)&counter, sizeof(counter));
		myfile.read((char *)&timestamp,sizeof(timestamp));
		while (!myfile.eof()) {
			QUEUE_ITEM item;
			myfile.read((char*)&item.size, sizeof(item.size));
			myfile.read((char*)&item.isLocal, sizeof(item.isLocal));
			myfile.read((char*)&item.message_number, sizeof(item.message_number));
			myfile.read((char*)&item.timestamp, sizeof(item.timestamp));
			myfile.read((char*)item.data, item.size);

			mtx.lock();
			if (queue_items_number >= this->queue_size) {
				// Queue is full. Pop first item
				msg_queue.pop();
				queue_items_number--;
			}
			item.retries = 0;
			item.last_sent_time = 0;
			msg_queue.push(item);
			mtx.unlock();
		}

		myfile.close();

		std::remove(filename);
		syslog(LOG_NOTICE, "%d messages were loaded to queue", queue_items_number);
	}
}
