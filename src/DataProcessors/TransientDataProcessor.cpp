/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "TransientDataProcessor.h"

namespace RFN {

TransientDataProcessor::TransientDataProcessor(RFNSMARTGateway *parent) :
		BaseDataProcessor(parent) {
	syslog(LOG_DEBUG, "Creating TransientDataProcessor...");
}

TransientDataProcessor::~TransientDataProcessor() {
	syslog(LOG_DEBUG, "Destroying TransientDataProcessor...");
}

void TransientDataProcessor::Start() {
	BaseDataProcessor::Start();
}

void TransientDataProcessor::Stop() {
	BaseDataProcessor::Stop();
}

void TransientDataProcessor::NetworkThreadFunc() {
	uint8_t incoming_data_buffer[MAX_SERVER_DATA_BUFF];
	// Wait until can start thread
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	syslog(LOG_NOTICE, "[Network] Transient Data processor initialized.");

	while (running) {
		CheckServerSettings();
		if (_socket == nullptr) {
			// Not connected, need to establish connection to server
			syslog(LOG_NOTICE, "[Network] Connecting to %s:%u.",
					serverIP.c_str(), serverPort);
			if (EstablishServerConnection()) {
				// Connected
				RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_ON);
				network_last_receive_time = last_send_time = last_ack_time =
						RFN::Utilities::Utilities::GetUpTimeSeconds();
				syslog(LOG_ERR, "[Network] Connected to server.");
			} else {
				syslog(LOG_ERR, "[Network] Can't connect to %s:%u.",
						serverIP.c_str(), serverPort);
				// Wait 10 seconds till next attempt
				for (int i = 0; i < 10 && running; i++)
					std::this_thread::sleep_for(std::chrono::seconds(1));
			}
		} else {
			// Socket connected, process stream
			int bytes_received = -1;
			do {
				bytes_received = _socket->read((char*) incoming_data_buffer,
				NETWORK_BUFF_SIZE);
				if (bytes_received > 0) {
					// Data received. Add to buffer
					network_last_receive_time =
							RFN::Utilities::Utilities::GetUpTimeSeconds();
					serverbuf.Append(incoming_data_buffer, bytes_received);
					if ((MAX_SERVER_DATA_BUFF - serverbuf.Available()) > 512)
						break;
				} else if (!bytes_received) {
					// Can't read from socket. Close it.
					DisconnectFromServer();
				}
			} while (bytes_received > 0);

			HandleServerStream();
			SendServerMessages();
		}

		if ((_socket == nullptr)
				&& ((RFN::Utilities::Utilities::GetUpTimeSeconds()
						- last_store_time) > 300)) {
			// Not connected to server. Store messages periodically.
			syslog(LOG_NOTICE,
					"[Network] Not connected to server. Storing messages to file...");
			StoreMessages();
			last_store_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
		}
		if ((last_send_time - last_ack_time) > 30) {
			// Acknowledge timeout
			DisconnectFromServer();
			syslog(LOG_NOTICE,
					"[Network] Acknowledge timeout. Closing socket...");
		}
		// Check if network is down and need to reboot the system.
		// It doesn't matter if the socket is opened or closed.
		//TODO: implement reboot
//		if (reboot_after > 0 && socket_idle_interval > 0
//				&& (RFN::Utilities::Utilities::GetUpTimeSeconds() - network_last_receive_time)
//						> (socket_idle_interval * 60 * reboot_after)) {
//			// Reboot
//			syslog(LOG_NOTICE,
//					"[Network] Network inactive!!! Rebooting unit...");
//			RebootDevice();
//		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	syslog(LOG_NOTICE, "[Network] Transient Data processor finished.");
}

void TransientDataProcessor::ProcessRadioMessage(enum protocols protocol, uint8_t *data, size_t size) {
	// Wrap all incoming messages with transparent message and add to queue
	static unsigned char t_msg_num = 0;
	uint8_t tmp_data[255] = { 0xFF, 0x65, 0x00, 0x00, 0x00, 0xCC, 0x00, 0x00 };
	memcpy(tmp_data + 7, data, size);
	tmp_data[4] = size + 3;
	tmp_data[6] = t_msg_num++;
	tmp_data[size + 7] = RFN::Utilities::Utilities::CalculateChecksum(
			(tmp_data + 5), tmp_data[4] - 1);
	std::string time_str = RFN::Utilities::Utilities::TimestampUTCToString(
			RFN::Utilities::Utilities::GetUTCTimestamp());

	// Add to queue
	if (rf_queue.Enqueue(tmp_data, size + 8, false)) {
		syslog(LOG_NOTICE,
				"[%s - RF] Transparent message #%d was added to queue.",
				time_str.c_str(), (((int) rf_queue.GetCounter() - 1) & 0xFFFF));
	} else {
		syslog(LOG_NOTICE, "[%s - RF] Can't add message to queue!!!",
				time_str.c_str());
	}
}

void TransientDataProcessor::ProcessServerMessage(uint8_t *data, size_t size) {

}

} /* namespace RFN */
