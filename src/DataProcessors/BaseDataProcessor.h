/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_DATAPROCESSORS_BASEDATAPROCESSOR_H_
#define SRC_DATAPROCESSORS_BASEDATAPROCESSOR_H_

#include <set>
#include <thread>
#include <libserial/SerialPort.h>
#include <libserial/SerialStream.h>
#include <netlink/socket.h>
#include <netlink/socket_group.h>
#include "../Exception.h"
#include "../Defines.h"
#include "../Utilities.h"
#include "../RingBuffer.h"
#include "../MessageQueue.h"

#define SOCKET_CONNECTION_TIMEOUT_SECONDS 10

using LibSerial::SerialPort;
using LibSerial::SerialStream;

namespace RFN {

class RFNSMARTGateway;

class OnAccept: public NL::SocketGroupCmd {
public:
	void exec(NL::Socket *socket, NL::SocketGroup *group, void *reference) {

		NL::Socket *newConnection = socket->accept();
		newConnection->blocking(false);
		group->add(newConnection);
		syslog(LOG_NOTICE, "Incoming connection from %s:%d",
				newConnection->hostTo().c_str(), newConnection->portTo());
	}

	virtual ~OnAccept() {

	}
};

class OnDisconnect: public NL::SocketGroupCmd {
public:
	void exec(NL::Socket *socket, NL::SocketGroup *group, void *reference) {

		group->remove(socket);
		syslog(LOG_NOTICE, "%s disconnected...", socket->hostTo().c_str());
		delete socket;
	}

	virtual ~OnDisconnect() {

	}
};

class OnRead: public NL::SocketGroupCmd {
public:
	void exec(NL::Socket *socket, NL::SocketGroup *group, void *reference) {

//		cout << "\nREAD -- ";
//		cout.flush();
		char buffer[256];
		buffer[255] = '\0';
		socket->read(buffer, 255);
//		size_t msgLen = strlen(buffer);
//		cout << "Message from " << socket->hostTo() << ":" << socket->portTo() << ". Text received: " << buffer;
//		cout.flush();
	}

	virtual ~OnRead() {

	}
};

class BaseDataProcessor {
protected:
	static const int PARALLEL_MSGS_SIZE = 10;
	static const int SEND_MSG_TIME_OUT = 20;
	static const int MSG_TYPE_SERVER_ACK = 2;
	static const int MSG_TYPE_SERVER_RECEIVER_MSG = 170;
	static const int MSG_TYPE_SERVER_TRANSIENT = 204;
	static const int MSG_TYPE_SERVER_SEND_SMS = 9;
	static const int MSG_TYPE_SERVER_GPS = 28;
public:
	enum protocols {
		RFN_OLD_PROTOCOL = 0,
		TELIT_TELEMETRY_PPROTOCOL,
		TELIT_HAYES_PROTOCOL,
		RFN_MESH_PROTOCOL
	};
	typedef struct {
		uint32_t id;
		uint16_t address;
		uint32_t timestamp;
	} CHILD_NODE;
	typedef struct __attribute__((__packed__)) {
		uint16_t temperature:11;
		uint16_t humidity:10;
		uint32_t pressure:19;
	} BME_DATA;
	typedef struct {
		uint8_t state;
		uint8_t reserved;
	} PARKING_DATA;
	BaseDataProcessor(RFNSMARTGateway *parent);
	virtual ~BaseDataProcessor();

	/**
	 * @brief Start radio handler
	 */
	virtual void Start();
	/**
	 * @brief Stop radio handler
	 */
	virtual void Stop();

	virtual void CanStart(bool can_start);

	virtual bool IsConnectedToServer();

	int GetSignalStrength();

	std::string GetRadioVerstionStr();

	virtual void StoreMessages();
	virtual void LoadMessages();

	virtual void SafeReboot();

protected:
	virtual void RadioThreadFunc();
	virtual void NetworkThreadFunc() = 0;
	virtual void ServerThreadFunc();
	virtual void AccelerometerThreadFunc();
	virtual void SchedulerThreadFunc();
	virtual void ProcessRadioMessage(enum protocols protocol, uint8_t *data, size_t size);
	virtual bool HandleRFNMeshV1Message(uint8_t msg_type, uint8_t msg_num, uint8_t *msg, size_t size);
	virtual void PowerChangeDetected();
	virtual void ProcessServerMessage(uint8_t *data, size_t size) = 0;
	virtual void HandleServerStream();
	virtual void SendServerMessages();
	virtual void SendGPRSMessage(long timestamp, uint16_t message_num,
			uint16_t receiver_id, uint8_t rssi, uint8_t *data,
			size_t data_size);
	virtual void SendToRadioBitStuffing(uint8_t *data, size_t size);
	virtual void SendToRadioRaw(uint8_t *data, size_t size);
	virtual void RequestLocalRadioAddress();

	virtual void DisconnectFromServer();
	virtual bool EstablishServerConnection();
	virtual void ServerSocketSend(uint8_t *data, size_t size);
	virtual void CheckServerSettings();
	virtual void FillSendBuffer();
	virtual int GetSendBufferEmptyIndex();

	void SendToClients(uint8_t *data, size_t size);

	uint16_t GetShortAddressByUnitID(uint32_t id);
	uint32_t GetUnitIDByShortAddress(uint16_t address);
	uint16_t GetChildrenNumber();
	void UpdateChildShortAddress(uint32_t id, uint16_t short_address);
	void UpdateChildLastSentTS(uint32_t id, uint16_t short_address);
	void ClearDisappearedUnits();
	void CheckStoppingMesh();
	void EnableMeshSendData(bool enable);
	void UpdateMeshSettings();
	void SendSetRegisterRequestMessage(uint32_t address, uint16_t reg, uint32_t value);
	void SendGetRegisterRequestMessage(uint32_t address, uint16_t reg);
	void SendGetShortAdressResponse(uint16_t stored_address, uint8_t *payload, size_t size);
	void SendMeshTimeResponse();
	void GenerateGPSMessage();

	virtual void CheckTimeouts();

	/// @brief Exception dispatcher
	Exception m_Exception;
	std::thread *threadNetwork;
	std::thread *threadRadio;
	std::thread *threadServer;
	std::thread *threadAccelerometer;
	std::thread *threadScheduler;
	RFNSMARTGateway* m_parent;
	volatile bool running;
	volatile bool can_start;
	volatile bool is_connected;
	volatile bool power_connected;
	struct gpiod_line *gpio_RTS;
	struct gpiod_line *gpio_CTS;
	struct gpiod_line *powerDetect;
	SerialStream radio_sp;
	NL::Socket *_socket;
	NL::Socket *socketServer;
	NL::SocketGroup group;
	std::string serverIP;
	uint16_t serverPort;
	MessageQueue rf_queue;
	QUEUE_ITEM send_buf[PARALLEL_MSGS_SIZE];
	RFN::RingBuffer<uint8_t> serverbuf;
	long network_last_receive_time;
	long last_send_time;
	long last_store_time;
	long last_ack_time;
	uint16_t local_receiver_address;
	uint8_t radio_out_msg_num;
	bool receiver_address_acquired;
	uint8_t address_acuire_retry;
	long last_address_acuire_ts;
	uint8_t radio_version[3];
	CHILD_NODE children[MAX_CHILDREN_NUM];
	bool enable_radio_network;
	bool is_configuring;
	uint8_t configuration_state;
	uint16_t config_reg;
	long last_config_msg_time;
	long last_mesh_children_clear_time;
	uint8_t config_retry;
	bool _isConnectedToServer;
	int reboot_after;
	int socket_idle_interval;
	long last_mesh_sync_time;
	volatile bool movement_detected;
	long last_gps_time;
};

} /* namespace RFN */

#endif /* SRC_DATAPROCESSORS_BASEDATAPROCESSOR_H_ */
