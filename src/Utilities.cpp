/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Utilities.h"

namespace RFN {

namespace Utilities {

struct gpiod_chip* Utilities::chip = 0;
int Utilities::i2c_handler = -1;

namespace config {

std::map<std::string, std::string> mapArgs;
std::map<std::string, std::vector<std::string> > mapMultiArgs;

void OptionParser(int argc, const char* const argv[]) {
	mapArgs.clear();
	mapMultiArgs.clear();

	for (int i = 1; i < argc; i++) {
		std::string strKey(argv[i]);
		std::string strValue;
		size_t has_data = strKey.find('=');
		if (has_data != std::string::npos) {
			strValue = strKey.substr(has_data + 1);
			strKey = strKey.substr(0, has_data);
		}

		if (strKey[0] != '-')
			break;

		mapArgs[strKey] = strValue;
		mapMultiArgs[strKey].push_back(strValue);
	}
}

const char* GetCharArg(const std::string& strArg, const std::string& nDefault) {
	if (mapArgs.count(strArg))
		return mapArgs[strArg].c_str();
	return nDefault.c_str();
}

std::string GetArg(const std::string& strArg, const std::string& strDefault) {
	if (mapArgs.count(strArg))
		return mapArgs[strArg];
	return strDefault;
}

int GetArg(const std::string& strArg, int nDefault) {
	if (mapArgs.count(strArg))
		return atoi(mapArgs[strArg].c_str());
	return nDefault;
}

} /* namespace config */

} /* namespace Utilities */

} /* namespace RFN */

