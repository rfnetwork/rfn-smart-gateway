/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_MESSAGEQUEUE_H_
#define SRC_MESSAGEQUEUE_H_

#include <mutex>
#include <queue>
#include <string.h>
#include <string>
#include <fstream>
#include <ostream>
#include <istream>
#include <stdint.h>
#include <syslog.h>
#include "Utilities.h"

#define MAX_RF_BUFF   					120

typedef struct {
	long timestamp;
	bool isLocal;
	size_t size;
	unsigned char data[MAX_RF_BUFF];
	unsigned short message_number;
	unsigned char retries;
	long last_sent_time;
} QUEUE_ITEM, *QUEUE_ITEM_PTR;

class MessageQueue {
	std::queue<QUEUE_ITEM> msg_queue;
	std::mutex mtx;
	size_t queue_size;
	size_t queue_items_number;
	unsigned short counter;
	uint32_t timestamp;
public:
	MessageQueue(size_t size);
	virtual ~MessageQueue();
	unsigned short GetCounter();
	size_t GetQueueMessageNumber();
	size_t Enqueue(unsigned char *data, size_t size, bool isLocal, long cur_timestamp = RFN::Utilities::Utilities::GetUTCTimestamp());
	QUEUE_ITEM Dequeue();
	QUEUE_ITEM GetHead();
	void SaveToFile(const char *filename);
	void LoadFromFile(const char *filename);
	uint32_t GetSystemTimestamp();
	void SetSystemTimestamp(uint32_t ts);
};

#endif /* SRC_MESSAGEQUEUE_H_ */
