/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_MQTTCLOUD_H_
#define SRC_MQTTCLOUD_H_

#include <mqtt/async_client.h>
#include <string>
#include <syslog.h>
#include <boost/signals2.hpp>

namespace RFN {

class MQTTCloud: public virtual mqtt::callback,
		public virtual mqtt::iaction_listener {
public:
	MQTTCloud(std::string host, std::string clientID = "");
	virtual ~MQTTCloud();

	bool connect(std::string user = "", std::string pass = "");
	bool connect_ssl(std::string user = "", std::string pass = "",
			std::string trust_store_path = "", std::string key_store_path = "",
			std::string private_key_path = "");
	void disconnect();
	bool is_connected();

	void subscribe(std::string topic, int qos = 0);
	void publish_binary(std::string topic, const char *payload,
			size_t payload_len, int qos = 0, bool retained =
					mqtt::message::DFLT_RETAINED);
	bool publish(std::string topic, std::string payload, int qos = 0,
			bool retained = mqtt::message::DFLT_RETAINED);

	boost::signals2::signal<void()> Connected;
	boost::signals2::signal<void()> Disconnected;
	boost::signals2::signal<
			void(std::string topic, const char *payload, size_t payload_len)> MessageReceived;
protected:
	mqtt::async_client *client;

	void on_failure(const mqtt::token &tok) override;
	void on_success(const mqtt::token &tok) override;
	void connection_lost(const std::string &cause) override;
	void message_arrived(mqtt::const_message_ptr msg) override;
	void delivery_complete(mqtt::delivery_token_ptr token) override;
};

} /* namespace RFN */

#endif /* SRC_MQTTCLOUD_H_ */
