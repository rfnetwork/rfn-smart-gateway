/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "BridgeDataProcessor.h"

namespace RFN {

BridgeDataProcessor::BridgeDataProcessor(RFNSMARTGateway *parent) :
		BaseDataProcessor(parent) {
	syslog(LOG_DEBUG, "Creating BridgeDataProcessor...");
}

BridgeDataProcessor::~BridgeDataProcessor() {
	syslog(LOG_DEBUG, "Destroying BridgeDataProcessor...");
}

void BridgeDataProcessor::Start() {
	BaseDataProcessor::Start();
}

void BridgeDataProcessor::Stop() {
	BaseDataProcessor::Stop();
}

void BridgeDataProcessor::RadioThreadFunc() {
	RFN::RingBuffer<uint8_t> radiobuf(RADIO_SERIAL_BUFFER_SIZE);
	uint8_t buffer[RADIO_SERIAL_BUFFER_SIZE];
	// Wait until can start thread
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	try {
		// Open serial port
		radio_sp.Open(RADIO_SERIAL_DEVICE);
		radio_sp.SetBaudRate(RADIO_SERIAL_SPEED);
		radio_sp.SetCharacterSize(LibSerial::CharacterSize::CHAR_SIZE_8);
		radio_sp.SetFlowControl(LibSerial::FlowControl::FLOW_CONTROL_NONE);
		radio_sp.SetParity(LibSerial::Parity::PARITY_NONE);
		radio_sp.SetStopBits(LibSerial::StopBits::STOP_BITS_1);
	} catch (const LibSerial::OpenFailed &ex) {
		m_Exception.Dispatch(ex.what());
	}
	RFN::Utilities::Utilities::SetGPIO(gpio_RTS, 0);

	syslog(LOG_NOTICE, "[Radio] Radio processor initialized.");
	while (running) {
		try {
			if (radio_sp.IsOpen()) {
				while (radio_sp.IsDataAvailable()) {
					char data_byte;
					radio_sp.get(data_byte);
					radiobuf.Append((uint8_t*) &data_byte, 1);
				}

				size_t available_size = radiobuf.Available();
				if (available_size > 0) {
					available_size = radiobuf.Get(buffer,
							(available_size > RADIO_SERIAL_BUFFER_SIZE) ?
									RADIO_SERIAL_BUFFER_SIZE : available_size);
					syslog(LOG_NOTICE, "[Radio] %s: %s", ((_socket != nullptr)? "Sending to server" : "Received"),
							RFN::Utilities::Utilities::HexArrayToString(buffer, available_size).c_str());
					// Forward to connected clients
					SendToClients(buffer, available_size);
					// Send to server
					ServerSocketSend(buffer, available_size);
				}
			}
		} catch (const LibSerial::OpenFailed &ex) {
			m_Exception.Dispatch(ex.what());
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	syslog(LOG_NOTICE, "[Radio] Radio processor finished.");
}

void BridgeDataProcessor::NetworkThreadFunc() {
	uint8_t incoming_data_buffer[MAX_SERVER_DATA_BUFF];
	// Wait until can start thread
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	syslog(LOG_NOTICE, "[Network] Bridge Data processor initialized.");

	while (running) {
		CheckServerSettings();

		if (_socket == nullptr) {
			// Not connected, need to establish connection to server
			syslog(LOG_NOTICE, "[Network] Connecting to %s:%u.",
					serverIP.c_str(), serverPort);
			if (EstablishServerConnection()) {
				// Connected
				RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_ON);
				syslog(LOG_ERR, "[Network] Connected to server.");
			} else {
				syslog(LOG_ERR, "[Network] Can't connect to %s:%u.",
						serverIP.c_str(), serverPort);
				// Wait 10 seconds till next attempt
				for (int i = 0; i < 10 && running; i++)
					std::this_thread::sleep_for(std::chrono::seconds(1));
			}
		} else {
			// Socket connected, process stream
			int bytes_received = -1;
			do {
				bytes_received = _socket->read((char *)incoming_data_buffer, NETWORK_BUFF_SIZE);
				if (bytes_received > 0) {
					// Data received. Pass to radio
					network_last_receive_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
					SendToRadioRaw(incoming_data_buffer, bytes_received);
				} else if (!bytes_received) {
					// Can't read from socket. Close it.
					DisconnectFromServer();
				}
			} while (bytes_received > 0);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	DisconnectFromServer();
	syslog(LOG_NOTICE, "[Network] Bridge Data processor finished.");
}

void BridgeDataProcessor::ProcessRadioMessage(enum protocols protocol, uint8_t *data, size_t size) {
	// Do nothing, just need to implement this function
}

void BridgeDataProcessor::ProcessServerMessage(uint8_t *data, size_t size) {
	// Do nothing, just need to implement this function
}

} /* namespace RFN */
