/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MQTTCloud.h"
#include "Utilities.h"

namespace RFN {

MQTTCloud::MQTTCloud(std::string host, std::string clientID) {
	client = new mqtt::async_client(host, clientID);

	client->set_callback(*this);
	if (!client)
		throw new std::runtime_error("Can't allocate MQTT Client");
	syslog(LOG_INFO, "%s initialized", mqtt::VERSION_STR.c_str());
}

MQTTCloud::~MQTTCloud() {
	disconnect();
	delete client;
}

bool MQTTCloud::connect(std::string user, std::string pass) {
	mqtt::connect_options options(user, pass);

	// First need to disconnect if connected
	disconnect();

	options.set_keep_alive_interval(20);

	mqtt::token_ptr conntok = client->connect(options);
	conntok->wait();

	if (client->is_connected()) {
		Connected();
		return true;
	}
	return false;
}

bool MQTTCloud::connect_ssl(std::string user, std::string pass,
		std::string trust_store_path, std::string key_store_path,
		std::string private_key_path) {
	mqtt::connect_options options(user, pass);
	mqtt::ssl_options sslopts;

	// First need to disconnect if connected
	disconnect();

	// Apply SSL certificates
	if (!trust_store_path.empty())
		sslopts.set_trust_store(trust_store_path);
	if (!key_store_path.empty())
		sslopts.set_key_store(key_store_path);
	if (!private_key_path.empty())
		sslopts.set_private_key(private_key_path);
	options.set_ssl(sslopts);

	options.set_keep_alive_interval(20);

	mqtt::token_ptr conntok = client->connect(options);
	conntok->wait();

	if (client->is_connected()) {
		Connected();
		return true;
	}
	return false;
}

void MQTTCloud::disconnect() {
	if (client && client->is_connected()) {
		client->disconnect();
	}
}

bool MQTTCloud::is_connected() {
	return (client && client->is_connected());
}

void MQTTCloud::subscribe(std::string topic, int qos) {
	client->subscribe(topic, qos, nullptr, *this);
}

void MQTTCloud::publish_binary(std::string topic, const char *payload,
		size_t payload_len, int qos, bool retained) {
	mqtt::binary pl(payload, payload_len);

	syslog(LOG_DEBUG, "Publishing binary to topic %s, %s", topic.c_str(),
			RFN::Utilities::Utilities::HexArrayToString((uint8_t*) payload,
					payload_len).c_str());
	client->publish(topic, pl, qos, retained)->wait_for(
			std::chrono::seconds(10));
}

bool MQTTCloud::publish(std::string topic, std::string payload, int qos,
		bool retained) {
	syslog(LOG_DEBUG, "Publishing binary to topic %s, %s", topic.c_str(),
			payload.c_str());
	return client->publish(topic, payload, qos, retained)->wait_for(
			std::chrono::seconds(10));
}

void MQTTCloud::on_failure(const mqtt::token &tok) {
	//syslog(LOG_NOTICE, "on_failure");
}

void MQTTCloud::on_success(const mqtt::token &tok) {
	//syslog(LOG_NOTICE, "on_success");
}

void MQTTCloud::connection_lost(const std::string &cause) {
	Disconnected();
	//syslog(LOG_NOTICE, "connection_lost");
}

void MQTTCloud::message_arrived(mqtt::const_message_ptr msg) {
	MessageReceived(msg->get_topic(), msg->get_payload().data(),
			msg->get_payload().length());
	//syslog(LOG_NOTICE, "message_arrived");
}

void MQTTCloud::delivery_complete(mqtt::delivery_token_ptr token) {
	//syslog(LOG_NOTICE, "delivery_complete");
}

} /* namespace RFN */
