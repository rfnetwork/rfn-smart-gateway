/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_DATAPROCESSORS_AWSDATAPROCESSOR_H_
#define SRC_DATAPROCESSORS_AWSDATAPROCESSOR_H_

#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include "../MQTTCloud.h"
#include "BaseDataProcessor.h"

#define AWS_MOSQUITTO_PORT 		(8883)
#define AWS_CLOUD_URL_TMP 		"ssl://%s:%d"
#define AWS_QOS					1
#define AWS_TRUST_STORE_PATH	"/root/aws.ca"
#define AWS_KEY_STORE_PATH		"/root/aws.crt"
#define AWS_PRIVATE_KEY_PATH	"/root/aws.key"

namespace RFN {

class AWSDataProcessor : public BaseDataProcessor {
protected:
	MQTTCloud *client;
	std::string cloudURL;

	volatile bool waiting_for_response;
	volatile bool allowed_devices_list_loaded;
	volatile bool info_sent;
	QUEUE_ITEM item;
	std::vector<uint32_t> allowed_tags;

	virtual void NetworkThreadFunc();
	virtual void ProcessServerMessage(uint8_t *data, size_t size);

	virtual void CheckServerSettings();
	void ConnectToCloud();

	bool CloudSendMessage(std::string topic, std::string payload);
	void HandleServerData(std::string topic, std::string payload);
	virtual void PublishGatewayInfo();
	virtual void RequestAllowedDeviceList();
	virtual bool ThingInList(unsigned long id) {
		return std::find(allowed_tags.begin(), allowed_tags.end(), id) != allowed_tags.end();
	};

	void SendReceiverMessage(QUEUE_ITEM *item);
	void SendTagMessage(QUEUE_ITEM *item);
	void SendGPSMessage(QUEUE_ITEM *item);
public:
	AWSDataProcessor(RFNSMARTGateway *parent);
	virtual ~AWSDataProcessor();

	/**
	 * @brief Start radio handler
	 */
	virtual void Start();
	/**
	 * @brief Stop radio handler
	 */
	virtual void Stop();
};

} /* namespace RFN */

#endif /* SRC_DATAPROCESSORS_AWSDATAPROCESSOR_H_ */
