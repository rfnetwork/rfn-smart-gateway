/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "RFN-SMART-Gateway.h"
#include "DataProcessors/DataProcessorFactory.h"

namespace RFN {

RFNSMARTGateway::RFNSMARTGateway(const std::vector<std::string> &args) :
		mode(MODE_UNKNOWN), sms(this), processor(nullptr) {

}

RFNSMARTGateway::~RFNSMARTGateway() {

}

// Note: we'd love Instance RAII but singleton needs to be daemonized (if applicable) before initialization
void RFNSMARTGateway::Initialize() {
	syslog(LOG_DEBUG, "Instance: initializing");
}

void RFNSMARTGateway::Start() {
	syslog(LOG_DEBUG, "Instance: starting");
	// Log the banner
	syslog(LOG_NOTICE, "%s (version %s)", APP_NAME, SOFTWARE_VERSION);
	syslog(LOG_NOTICE, "Build time: %s %s", __DATE__, __TIME__);

	// Validate configuration file
	ValidateConfigFile();

	processor = RFN::DataProcessorFactory::createDataSender(this, mode);
	sms.Start();
	if (processor)
		processor->Start();
}

void RFNSMARTGateway::Stop() {
	syslog(LOG_DEBUG, "Instance: stopping");

	if (processor)
		processor->Stop();
	sms.Stop();
	RFN::Utilities::Utilities::CloseGPIOs();
	RFN::Utilities::Utilities::I2CClose();
}

void RFNSMARTGateway::Reload() {
	syslog(LOG_DEBUG, "Instance: reloading");
	// Stop services
	if (processor) {
		processor->Stop();
		delete processor;
	}
	sms.Stop();

	RFN::Utilities::Utilities::CloseGPIOs();
	RFN::Utilities::Utilities::I2CClose();

	// Validate configuration file
	ValidateConfigFile();

	// Start services
	sms.Start();
	processor = RFN::DataProcessorFactory::createDataSender(this, mode);
	if (processor)
		processor->Start();
}

void RFNSMARTGateway::ChangeMode(WorkingMode) {
	std::this_thread::sleep_for(std::chrono::seconds(5));
	syslog(LOG_NOTICE, "ChangeMode");
	processor->Stop();
	delete processor;
	RFN::Utilities::Utilities::CloseGPIOs();
	mode = RFN::Utilities::Utilities::GetMode();
	processor = RFN::DataProcessorFactory::createDataSender(this, mode);
	if (processor) {
		processor->Start();
		processor->CanStart(true);
	}
}

void RFNSMARTGateway::SendSMS(std::string number, std::string message) {
	sms.SendSMS(number, message);
}

int RFNSMARTGateway::GetSignalStrength() {
	return sms.GetSignalStrength();
}

bool RFNSMARTGateway::UpdateRadioFW(uint8_t type) {
	std::string fw_file;
	switch (type) {
	case 1:
		// RF-Networks Tag receiver
		fw_file = "/tmp/RadioFW.hex";
		// Download file
		if (RFN::Utilities::Utilities::exec(
				RFN::Utilities::Utilities::StringFormat(
						"curl -s -u rf:Q!123456 -f ftp://rfnetwork.cloudapp.net/AppZone/TagReceiver.hex -o %s",
						fw_file.c_str())).result_code) {
			// Download failed
			syslog(LOG_ERR, "Failed downloading radio firmware!!!");
			return false;
		}
		// Flash firmware file
		processor->Stop();
		delete processor;

		if (RFN::Utilities::Utilities::exec(
				RFN::Utilities::Utilities::StringFormat(
						"lrffp %s /dev/ttyS1 -b", fw_file.c_str())).result_code) {
			syslog(LOG_ERR, "Failed downloading radio firmware!!!");
		} else {
			syslog(LOG_NOTICE, "Radio firmware was flashed successfully.");
		}
		RFN::Utilities::Utilities::exec(
				RFN::Utilities::Utilities::StringFormat("rm %s",
						fw_file.c_str()));
		mode = RFN::Utilities::Utilities::GetMode();
		processor = RFN::DataProcessorFactory::createDataSender(this, mode);
		if (processor) {
			processor->Start();
			processor->CanStart(true);
		}
		break;
	case 4:
		// RF-Networks Mesh
		fw_file = "/tmp/RadioFW.hex";
		// Download file
		if (RFN::Utilities::Utilities::exec(
				RFN::Utilities::Utilities::StringFormat(
						"curl -s -u rf:Q!123456 -f ftp://rfnetwork.cloudapp.net/AppZone/RFN-Mesh/RFN-Mesh.hex -o %s",
						fw_file.c_str())).result_code) {
			// Download failed
			syslog(LOG_ERR, "Failed downloading radio firmware!!!");
			return false;
		}
		// Flash firmware file
		processor->Stop();
		delete processor;

		if (RFN::Utilities::Utilities::exec(
				RFN::Utilities::Utilities::StringFormat(
						"lrffp %s /dev/ttyS1 -b", fw_file.c_str())).result_code) {
			syslog(LOG_ERR, "Failed downloading radio firmware!!!");
		} else {
			syslog(LOG_NOTICE, "Radio firmware was flashed successfully.");
		}
		RFN::Utilities::Utilities::exec(
				RFN::Utilities::Utilities::StringFormat("rm %s",
						fw_file.c_str()));
		mode = RFN::Utilities::Utilities::GetMode();
		processor = RFN::DataProcessorFactory::createDataSender(this, mode);
		if (processor) {
			processor->Start();
			processor->CanStart(true);
		}
		break;
	default:
		// Not supported firmware
		return false;
	}
	return true;
}

void RFNSMARTGateway::RestartProcessor() {
	processor->Stop();
	delete processor;
	mode = RFN::Utilities::Utilities::GetMode();
	processor = RFN::DataProcessorFactory::createDataSender(this, mode);
	if (processor) {
		processor->Start();
		processor->CanStart(true);
	}
}

void RFNSMARTGateway::ValidateConfigFile() {
	RFN::Utilities::Utilities::CheckConfigFile();
	mode = RFN::Utilities::Utilities::GetMode();

	if (mode == MODE_UNKNOWN) {
		syslog(LOG_ERR, "Can't load mode!!! Setting default: %d", DEFAULT_MODE);
		RFN::Utilities::Utilities::SetMode(DEFAULT_MODE);
		mode = DEFAULT_MODE;
	}

	if (RFN::Utilities::Utilities::GetSMSPass().empty()) {
		syslog(LOG_ERR, "Can't load SMS password!!! Setting default: '%s'",
		DEFAULT_SMS_PASSWORD);
		RFN::Utilities::Utilities::SetSMSPass(DEFAULT_SMS_PASSWORD);
	}

	if (RFN::Utilities::Utilities::GetServerIP().empty()) {
		syslog(LOG_ERR, "Can't load Server IP!!! Setting default: '%s'",
		DEFAULT_SERVER_IP);
		RFN::Utilities::Utilities::SetServerIP(DEFAULT_SERVER_IP);
	}

	if (!RFN::Utilities::Utilities::GetServerPort()) {
		syslog(LOG_ERR, "Can't load Server port!!! Setting default: %d",
		DEFAULT_SERVER_PORT);
		RFN::Utilities::Utilities::SetServerPort(DEFAULT_SERVER_PORT);
	}

	if (RFN::Utilities::Utilities::GetTelitCloudURL().empty()) {
		syslog(LOG_ERR, "Can't load Telit cloud URL!!! Setting default: '%s'",
		DEFAULT_TELIT_CLOUD_URL);
		RFN::Utilities::Utilities::SetTelitCloudURL(DEFAULT_TELIT_CLOUD_URL);
	}

	if (RFN::Utilities::Utilities::GetTelitAppID().empty()) {
		syslog(LOG_ERR,
				"Can't load Telit cloud application ID!!! Setting default: '%s'",
				DEFAULT_TELIT_APP_ID);
		RFN::Utilities::Utilities::SetTelitAppID(DEFAULT_TELIT_APP_ID);
	}

	if (RFN::Utilities::Utilities::GetTelitToken().empty()) {
		syslog(LOG_ERR, "Can't load Telit cloud token!!! Setting default: '%s'",
		DEFAULT_TELIT_TOKEN);
		RFN::Utilities::Utilities::SetTelitToken(DEFAULT_TELIT_TOKEN);
	}

	if (RFN::Utilities::Utilities::GetAWSCloudURL().empty()) {
		syslog(LOG_ERR, "Can't load AWS cloud URL!!! Setting default: '%s'",
		DEFAULT_AWS_CLOUD_URL);
		RFN::Utilities::Utilities::SetAWSCloudURL(DEFAULT_AWS_CLOUD_URL);
	}

	if (RFN::Utilities::Utilities::GetMeshChannel() == 0xFF) {
		syslog(LOG_ERR, "Can't load Mesh channel!!! Setting default: %d",
		MESH_DEFAULT_CHANNEL);
		RFN::Utilities::Utilities::SetMeshChannel(MESH_DEFAULT_CHANNEL);
	}

	if (RFN::Utilities::Utilities::GetMeshAdvChannel() == 0xFF) {
		syslog(LOG_ERR,
				"Can't load Mesh advertising channel!!! Setting default: %d",
				MESH_DEFAULT_ADV_CHANNEL);
		RFN::Utilities::Utilities::SetMeshAdvChannel(MESH_DEFAULT_ADV_CHANNEL);
	}

	if (RFN::Utilities::Utilities::GetMeshBaseTime() == 0xFF) {
		syslog(LOG_ERR, "Can't load Mesh base time!!! Setting default: %d ms",
				125 << MESH_DEFAULT_BASE_TIME);
		RFN::Utilities::Utilities::SetMeshBaseTime(MESH_DEFAULT_BASE_TIME);
	}

	if (RFN::Utilities::Utilities::GetMeshFramesInCycle() == 0xFF) {
		syslog(LOG_ERR,
				"Can't load Mesh frames in cycle!!! Setting default: %d",
				16 << MESH_DEFAULT_FRAMES_IN_CYCLE);
		RFN::Utilities::Utilities::SetMeshFramesInCycle(
				MESH_DEFAULT_FRAMES_IN_CYCLE);
	}

	if (RFN::Utilities::Utilities::GetMeshSyncWord() == 0) {
		syslog(LOG_ERR, "Can't load Mesh sync word!!! Setting default: 0x%04X",
		MESH_DEFAULT_SYNCWORD);
		RFN::Utilities::Utilities::SetMeshSyncWord(MESH_DEFAULT_SYNCWORD);
	}

	if (RFN::Utilities::Utilities::GetMeshNetworkID() == 0xFF) {
		syslog(LOG_ERR,
				"Can't load Mesh network ID!!! Setting default: %d (0x%02X)",
				MESH_DEFAULT_NETWORK_ID, MESH_DEFAULT_NETWORK_ID);
		RFN::Utilities::Utilities::SetMeshNetworkID(MESH_DEFAULT_NETWORK_ID);
	}

	if (RFN::Utilities::Utilities::GetRebootAfter() < 0) {
		syslog(LOG_ERR, "Can't load Reboot After!!! Setting default: %d",
		DEFAULT_REBOOT_AFTER);
		RFN::Utilities::Utilities::SetRebootAfter(DEFAULT_REBOOT_AFTER);
	}

	if (RFN::Utilities::Utilities::GetSocketIdleInterval() < 0) {
		syslog(LOG_ERR,
				"Can't load Socket Idle interval!!! Setting default: %d",
				DEFAULT_SOCKET_IDLE);
		RFN::Utilities::Utilities::SetSocketIdleInterval(DEFAULT_SOCKET_IDLE);
	}

	if (RFN::Utilities::Utilities::GetAccelerometerSensitivity() == 0xFF) {
		syslog(LOG_ERR,
				"Can't load Accelerometer sensitivity!!! Setting default: %d",
				ACCELEROMETER_DEFAULT_SENS);
		RFN::Utilities::Utilities::SetAccelerometerSensitivity(
				ACCELEROMETER_DEFAULT_SENS);
	}

	if (RFN::Utilities::Utilities::GetGPSPeriod() < 0) {
		syslog(LOG_ERR, "Can't load GPS period!!! Setting default: %d",
		DEFAULT_GPS_PERIOD_SECS);
		RFN::Utilities::Utilities::SetGPSPeriod(DEFAULT_GPS_PERIOD_SECS);
	}

	if (RFN::Utilities::Utilities::GetGPSMovementThreshold() < 0) {
		syslog(LOG_ERR,
				"Can't load GPS movement threshold!!! Setting default: %d",
				DEFAULT_GPS_THRESHOLD_SECS);
		RFN::Utilities::Utilities::SetGPSMovementThreshold(
				DEFAULT_GPS_THRESHOLD_SECS);
	}
}

BaseDataProcessor* RFNSMARTGateway::GetProcessor() {
	return processor;
}

}
