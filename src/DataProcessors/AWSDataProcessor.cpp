/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "AWSDataProcessor.h"

namespace RFN {

AWSDataProcessor::AWSDataProcessor(RFNSMARTGateway *parent) :
		BaseDataProcessor(parent), client(nullptr), waiting_for_response(false), allowed_devices_list_loaded(
				false), info_sent(false) {

	syslog(LOG_DEBUG, "Creating AWSDataProcessor...");
}

AWSDataProcessor::~AWSDataProcessor() {
	syslog(LOG_DEBUG, "Destroying AWSDataProcessor...");
}

void AWSDataProcessor::Start() {
	BaseDataProcessor::Start();
}

void AWSDataProcessor::Stop() {
	BaseDataProcessor::Stop();
}

void AWSDataProcessor::NetworkThreadFunc() {
	long last_connect_time = 0;
//	memset(&item, 0, sizeof(QUEUE_ITEM));

// Wait until can start thread
	while (running && !this->can_start) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	cloudURL = RFN::Utilities::Utilities::GetAWSCloudURL();
	syslog(LOG_NOTICE, "[Network] AWS Data processor initialized.");

	while (running) {
		CheckServerSettings();
		if (client == nullptr) {
			// Create new client instance
			if (can_start
					&& ((RFN::Utilities::Utilities::GetUpTimeSeconds()
							- last_connect_time) > 10)) {
				last_connect_time =
						RFN::Utilities::Utilities::GetUpTimeSeconds();
//				tags_list_offset = 0;
				ConnectToCloud();
			}
			if (((RFN::Utilities::Utilities::GetUpTimeSeconds()
					- last_store_time) > 300)) {
				// Not connected to server. Store messages periodically.
				syslog(LOG_NOTICE,
						"[Network] Not connected to server. Storing messages to file...");
				StoreMessages();
				last_store_time = RFN::Utilities::Utilities::GetUpTimeSeconds();
			}
		} else if (client && client->is_connected()) {
			network_last_receive_time =
					RFN::Utilities::Utilities::GetUpTimeSeconds();

			if (receiver_address_acquired || address_acuire_retry > 2) {
				if (!info_sent) {
					// Send gateway info to server
					PublishGatewayInfo();
				}
			}

			if (!allowed_devices_list_loaded && !waiting_for_response) {
				// Load allowed device list
				waiting_for_response = true;
				RequestAllowedDeviceList();
			}

			if (allowed_devices_list_loaded) {
				if (item.size > 5) {
					long cur_time =
							RFN::Utilities::Utilities::GetUTCTimestamp();
					if (!item.last_sent_time
							|| (cur_time - item.last_sent_time)
									> SEND_MSG_TIME_OUT) {
						if (item.isLocal && item.size > 0) {
							// Local message
							switch (item.data[1]) { // Message type
							case 0x1C:
								// GPS message
								item.last_sent_time = cur_time;
								SendGPSMessage(&item);
								break;
							}
						} else {
							// Radio message
							switch (item.data[5]) { // Message type
							case 0x01:
								// Tag messages send it
								item.last_sent_time = cur_time;
								SendTagMessage(&item);

								break;
							case 0xAA:
								// Receiver message
								syslog(LOG_NOTICE,
										"[Network] Queue Receiver Message (Type: 0x%02X, OpCode: 0x%02X)",
										item.data[10], item.data[11]);

								// Send
								item.last_sent_time = cur_time;
								SendReceiverMessage(&item);
								break;
							}
						}
						rf_queue.Dequeue();
						item = rf_queue.GetHead();
					}
				} else if (item.size > 0 && item.size <= 5) {
					// Wrong message size discard message
					rf_queue.Dequeue();
					item = rf_queue.GetHead();
				} else {
					// Get next item
					item = rf_queue.GetHead();
				}
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	syslog(LOG_NOTICE, "[Network] AWS Data processor finished.");
}

void AWSDataProcessor::ProcessServerMessage(uint8_t *data, size_t size) {

}

void AWSDataProcessor::CheckServerSettings() {
	static long prevTs = 0;
	long curUpTime = RFN::Utilities::Utilities::GetUpTimeSeconds();
	if (!prevTs || ((curUpTime - prevTs) > NETWORK_SETTINGS_CHECK_PERIOD)) {
		// Check server settings for change
		syslog(LOG_NOTICE, "[Network] Check MQTT settings changing.");
		std::string tempCloudURL = RFN::Utilities::Utilities::GetAWSCloudURL();

		if (strcmp(cloudURL.c_str(), tempCloudURL.c_str()) != 0) {
			// Settings were changed, reconnect.
			cloudURL = tempCloudURL;
			if (client) {
				client->disconnect();
				delete client;
				client = nullptr;
			}
			allowed_devices_list_loaded = false;
			waiting_for_response = false;
//			tags_list_offset = 0;
			allowed_tags.clear();
		}
		prevTs = curUpTime;
	}
}

void AWSDataProcessor::ConnectToCloud() {
	std::string imei = RFN::Utilities::Utilities::GetIMEI();

	std::ifstream tstore(AWS_TRUST_STORE_PATH);
	std::ifstream kstore(AWS_KEY_STORE_PATH);
	std::ifstream pstore(AWS_PRIVATE_KEY_PATH);

	if (tstore.good() && kstore.good() && pstore.good() && !cloudURL.empty()) {
		syslog(LOG_NOTICE, "[Network] Connecting to AWS IoT Core...");
		try {
			std::string host = RFN::Utilities::Utilities::StringFormat(
			AWS_CLOUD_URL_TMP, cloudURL.c_str(), AWS_MOSQUITTO_PORT);
			std::string clientID = RFN::Utilities::Utilities::StringFormat(
					"Gateway-%s", imei.c_str());
			client = new MQTTCloud(host, clientID);
			client->Connected.connect(
					[=]() {
						// Connected, subscribe to "#" topic
						_isConnectedToServer = true;
						RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_ON);
						syslog(LOG_NOTICE,
								"[Network] Connected to AWS IoT Core.");
						client->subscribe(
								RFN::Utilities::Utilities::StringFormat(
										"$aws/things/%s/shadow/#",
										imei.c_str()), AWS_QOS);
					});
			client->Disconnected.connect([=]() {
				_isConnectedToServer = false;
				RFN::Utilities::Utilities::SetLedStatus(LED_STATUS_OFF);
				if (client) {
					delete client;
					client = nullptr;
				}
				syslog(LOG_NOTICE, "[Network] Disconnected from AWS IoT Core.");
			});
			client->MessageReceived.connect(
					[=](std::string topic, const char *payload, size_t payload_len) {
						std::string pl(payload, payload_len);
						syslog(LOG_DEBUG, "Received %s: %s", topic.c_str(),
								pl.c_str());
						HandleServerData(topic, pl);
					});

			if (!client->connect_ssl("", "", AWS_TRUST_STORE_PATH,
			AWS_KEY_STORE_PATH, AWS_PRIVATE_KEY_PATH)) {
				// Can't connect
				syslog(LOG_ERR,
						"[Network] Can't connected to AWS IoT Core !!!");
				if (client) {
					delete client;
					client = nullptr;
				}
			}
		} catch (const mqtt::exception &exc) {
			syslog(LOG_ERR, "[Network] Error %s %s", exc.what(),
					exc.get_error_str().c_str());
			if (client) {
				delete client;
				client = nullptr;
			}
		} catch (std::exception &ex) {
			syslog(LOG_ERR, "[Network] Error %s", ex.what());
			if (client) {
				delete client;
				client = nullptr;
			}
		}
	} else {
		syslog(LOG_ERR,
				"[Network] Missing parameters for connection to AWS IoT Core !!!");
	}
}

bool AWSDataProcessor::CloudSendMessage(std::string topic,
		std::string payload) {
	if (client != nullptr && client->is_connected()) {
		syslog(LOG_DEBUG, "Request (%s): %s", topic.c_str(), payload.c_str());
		return client->publish(topic.c_str(), payload.c_str(), AWS_QOS);
	}
	return false;
}

void AWSDataProcessor::HandleServerData(std::string topic, std::string payload) {
	std::string t = topic;
	std::string imei = RFN::Utilities::Utilities::GetIMEI();
	boost::property_tree::ptree in;
	RFN::Utilities::Utilities::RemoveSub(t, RFN::Utilities::Utilities::StringFormat("$aws/things/%s/shadow", imei.c_str()));

	if (!t.compare("/get/accepted")) {
		// Shadow get
		allowed_tags.clear();
		std::istringstream iss(payload);
		boost::property_tree::read_json(iss, in);
		boost::optional<boost::property_tree::ptree&> messages =
				in.get_child_optional("state.desired.tags.");
		if (messages) {
			BOOST_FOREACH(boost::property_tree::ptree::value_type &v, messages.get()) {
				uint32_t tag_id = (uint32_t) stoi(v.second.data());

				if (tag_id > 0 && !ThingInList(tag_id)) {
					syslog(LOG_NOTICE, "[Network] Tag %u allowed", tag_id);
					allowed_tags.push_back(tag_id);
				}
			}
		}
		waiting_for_response = false;
		allowed_devices_list_loaded = true;
		syslog(LOG_NOTICE, "[Network] Allowed tags list loaded. Total %u tags", allowed_tags.size());
	} else if (!t.compare("/update/accepted")) {
		// Update allowed tags list
		allowed_tags.clear();
		std::istringstream iss(payload);
		boost::property_tree::read_json(iss, in);
		boost::optional<boost::property_tree::ptree&> messages =
				in.get_child_optional("state.desired.tags.");
		if (messages) {
			BOOST_FOREACH(boost::property_tree::ptree::value_type &v, messages.get()) {
				uint32_t tag_id = (uint32_t) stoi(v.second.data());

				if (tag_id > 0 && !ThingInList(tag_id)) {
					syslog(LOG_NOTICE, "[Network] Tag %u allowed", tag_id);
					allowed_tags.push_back(tag_id);
				}
			}
		}
		syslog(LOG_NOTICE, "[Network] Allowed tags list updated. Total %u tags", allowed_tags.size());
	}
	//else
	//	syslog(LOG_NOTICE,"%s", t.c_str());
}

void AWSDataProcessor::PublishGatewayInfo() {
	std::string imei = RFN::Utilities::Utilities::GetIMEI();
	std::string topic = RFN::Utilities::Utilities::StringFormat(
			"$aws/things/%s/attributes", imei.c_str());
	std::string data = "{";
	data += RFN::Utilities::Utilities::StringFormat("\"netAddr\": \"%u\",",
			local_receiver_address);
	data += RFN::Utilities::Utilities::StringFormat(
			"\"version\": \"%d.%d.%d\",",
			SOFTWARE_VERSION_MAJOR, SOFTWARE_VERSION_MINOR,
			SOFTWARE_VERSION_BUILD);
	data += RFN::Utilities::Utilities::StringFormat(
			"\"rdversion\": \"%d.%d.%d\"", radio_version[0], radio_version[1],
			radio_version[2]);
	data += "}";
	syslog(LOG_NOTICE, "[Network] Publishing gateway information");
	info_sent = CloudSendMessage(topic, data);
}

void AWSDataProcessor::RequestAllowedDeviceList() {
	std::string imei = RFN::Utilities::Utilities::GetIMEI();
	std::string topic = RFN::Utilities::Utilities::StringFormat(
			"$aws/things/%s/shadow/get", imei.c_str());
	syslog(LOG_NOTICE, "[Network] Requesting allowed tag list...");

	CloudSendMessage(topic, "");
}

void AWSDataProcessor::SendReceiverMessage(QUEUE_ITEM *item) {
	std::string imei = RFN::Utilities::Utilities::GetIMEI();
	std::string topic = RFN::Utilities::Utilities::StringFormat(
			"$aws/things/%s/data", imei.c_str());
	std::string data = "{";
	data += RFN::Utilities::Utilities::StringFormat("\"timestamp\": \"%s\",",
			RFN::Utilities::Utilities::GetRFCTimestamp(item->timestamp).c_str());
	data += RFN::Utilities::Utilities::StringFormat("\"rssi\": %d",
			-GetSignalStrength());
	if ((item->data[10] == 0x0D && item->data[11] == 0x0A)
			|| (item->data[10] == 0x0E
					&& (item->data[11] == 0x00 || item->data[11] == 0x01))) {
		// Voltage
		float voltage =
				(item->data[10] == 0x0E) ?
						((((uint16_t) item->data[16] << 8) + item->data[15])
								& 0xFFFF) * 0.001F :
						((((uint16_t) item->data[14] << 8) + item->data[13])
								& 0xFFFF) * 0.001F;
		data += RFN::Utilities::Utilities::StringFormat(
				",\"voltage\": %.2f", voltage);
	} else if (item->data[10] == 0x0D && item->data[11] == 0x10) {
		// External Voltage
		float ext_voltage = ((((uint16_t) item->data[14] << 8) + item->data[13])
				& 0xFFFF) * 0.001F;
		data += RFN::Utilities::Utilities::StringFormat(
				",\"externalvoltage\": %.2f", ext_voltage);
	} else if ((item->data[10] == 0x0D && item->data[11] == 0x0B)
			|| (item->data[10] == 0x0E
					&& (item->data[11] == 0x00 || item->data[11] == 0x01))) {
		// Temperature
		float temperature =
				(item->data[10] == 0x0E) ?
						item->data[17] * 0.5F : item->data[13] * 1.0F;
		data += RFN::Utilities::Utilities::StringFormat(
				",\"temperature\": %.2f", temperature);
	}
	data += "}";
	syslog(LOG_NOTICE, "[Network] Publishing Receiver message");
	CloudSendMessage(topic, data);
}

void AWSDataProcessor::SendTagMessage(QUEUE_ITEM *item) {
	uint32_t tag_id;
	std::string str_msg_type = "";
	std::string imei = RFN::Utilities::Utilities::GetIMEI();
	for (uint8_t start_index = 7;
			start_index < item->size && start_index < item->data[4] + 4;
			start_index = start_index + 13) {
		memcpy(&tag_id, &item->data[start_index + 2], 4);
		if (ThingInList(tag_id)) {
			std::string data = "{";
			data +=
					RFN::Utilities::Utilities::StringFormat(
							"\"timestamp\":\"%s\"",
							RFN::Utilities::Utilities::GetRFCTimestamp(
									item->timestamp).c_str());
			uint8_t msg_type = item->data[start_index + 1] & 0x1F;
			data += RFN::Utilities::Utilities::StringFormat(
					",\"msgtype\": %d", msg_type);
			data += RFN::Utilities::Utilities::StringFormat(
					",\"gpio7\": %d",
					(item->data[start_index + 1] & 0x80) >> 7);
			data += RFN::Utilities::Utilities::StringFormat(
					",\"gpio8\": %d",
					(item->data[start_index + 1] & 0x20) >> 5);
			data += RFN::Utilities::Utilities::StringFormat(
					",\"gpio9\": %d",
					(item->data[start_index + 1] & 0x40) >> 6);
			data += RFN::Utilities::Utilities::StringFormat(
					",\"msgcount\": %d",
					item->data[start_index + 6]);

			if (msg_type == 3) {
				// Temperature
				str_msg_type = "Temperature";
				float voltage = (uint8_t) item->data[start_index + 11] / 62.5F;
				float temp = (((uint16_t) item->data[start_index + 10] << 8)
						+ (uint16_t) item->data[start_index + 9]) / 16.0F;
				data += RFN::Utilities::Utilities::StringFormat(
						",\"voltage\": %.2f", voltage);
				data += RFN::Utilities::Utilities::StringFormat(
						",\"temperature\": %.3f", temp);
			} else if (msg_type == 6 || msg_type == 12 || msg_type == 29
					|| msg_type == 1) {
				// Push button GPIO 9 or GPIO7 or GPIO8(Magnetic) or Periodic
				str_msg_type = (msg_type == 1) ? "Periodic" : "Push button";
				float voltage = 0;
				if (msg_type == 6) {
					uint16_t analogA = (((uint16_t) item->data[start_index + 10]
							<< 8) + (uint16_t) item->data[start_index + 9]);
					uint16_t analogB = (((uint16_t) item->data[start_index + 8]
							<< 8) + (uint16_t) item->data[start_index + 7]);
					data += RFN::Utilities::Utilities::StringFormat(
							",\"analog1\": %u", analogA);
					data += RFN::Utilities::Utilities::StringFormat(
							",\"analog2\": %u", analogB);
				}

				if (msg_type == 1) {
					voltage = ((uint8_t) item->data[start_index + 9]) / 62.5F;
				}
				if (voltage >= 1.9F && voltage <= 3.5F) {
					data += RFN::Utilities::Utilities::StringFormat(
							",\"voltage\": %.2f",
							voltage);
				}
			} else if (msg_type == 9) {
				// Movement
				str_msg_type = "Movement";
				int8_t x = (int8_t) item->data[start_index + 9];
				int8_t y = (int8_t) item->data[start_index + 10];
				int8_t z = (int8_t) item->data[start_index + 11];
				data += RFN::Utilities::Utilities::StringFormat(
						",\"x\": %d", x);
				data += RFN::Utilities::Utilities::StringFormat(
						",\"y\": %d", y);
				data += RFN::Utilities::Utilities::StringFormat(
						",: \"z\": %d", z);
			} else if (msg_type == 14 || msg_type == 17) {
				// Analog / Analog counter
				str_msg_type = "Analog";
				float voltage = (int8_t) item->data[start_index + 11] / 62.5F;
				uint16_t analogA =
						(((int16_t) item->data[start_index + 10] << 8)
								+ (int16_t) item->data[start_index + 9]);
				uint16_t analogB = (((int16_t) item->data[start_index + 8] << 8)
						+ (int16_t) item->data[start_index + 7]);
				data += RFN::Utilities::Utilities::StringFormat(
						",\"analog1\": %d", analogA);
				data += RFN::Utilities::Utilities::StringFormat(
						",\"%s\": %d",
						(msg_type == 17) ? "counter" : "analog2", analogB);
				if (voltage >= 1.9F && voltage <= 3.5F) {
					data += RFN::Utilities::Utilities::StringFormat(
							",\"voltage\": %.2f",
							voltage);
				}
			} else if (msg_type == 2) {
				// Counter
				str_msg_type = "Counter";
				float voltage = (int8_t) item->data[start_index + 11] / 62.5F;
				uint32_t counterVal = (((uint32_t) item->data[start_index + 10]
						<< 24) + ((uint32_t) item->data[start_index + 9] << 16)
						+ ((uint32_t) item->data[start_index + 8] << 8)
						+ (uint32_t) item->data[start_index + 7]);

				data += RFN::Utilities::Utilities::StringFormat(
						",\"counter\": %lu", counterVal);
				if (voltage >= 1.9F && voltage <= 3.5F) {
					data += RFN::Utilities::Utilities::StringFormat(
							",\"voltage\": %.2f",
							voltage);
				}
			}

			data += RFN::Utilities::Utilities::StringFormat(
					",\"imei\": %s",
					imei);
			data += RFN::Utilities::Utilities::StringFormat(
					",\"rssi\": %.1f",
					(item->data[start_index + 12] / 2.0f) - 120);
			data += "}";
			syslog(LOG_NOTICE, "[Network] Publishing Tag %u %smessage", tag_id,
					(str_msg_type.empty()) ? "" : (str_msg_type + " ").c_str());
			std::string topic = RFN::Utilities::Utilities::StringFormat(
							"$aws/things/%s/tags/%u/data", imei.c_str(), tag_id);
			CloudSendMessage(topic, data);
		} else {
			syslog(LOG_NOTICE,
					"[Network] Tag %u message received. Tag is not in allowed list, discarding message",
					tag_id);
		}
	}
}

void AWSDataProcessor::SendGPSMessage(QUEUE_ITEM *item) {
	std::string imei = RFN::Utilities::Utilities::GetIMEI();
	std::string topic = RFN::Utilities::Utilities::StringFormat(
			"$aws/things/%s/data", imei.c_str());
	float val;
	if (item == NULL || item->size < 16)
		return;
	std::string data = "{";
	data += RFN::Utilities::Utilities::StringFormat("\"timestamp\": \"%s\",",
			RFN::Utilities::Utilities::GetRFCTimestamp(item->timestamp).c_str());
	data += RFN::Utilities::Utilities::StringFormat("\"rssi\": %d",
			-GetSignalStrength());
	memcpy(&val, item->data + 3, 4);
	data += RFN::Utilities::Utilities::StringFormat(",\"latitude\": %f",
			val);
	memcpy(&val, item->data + 7, 4);
	data += RFN::Utilities::Utilities::StringFormat(",\"longitude\": %f",
			val);
	memcpy(&val, item->data + 11, 4);
	data += RFN::Utilities::Utilities::StringFormat(",\"altitude\": %f",
			val);
	data += RFN::Utilities::Utilities::StringFormat(",\"hdop\": %d",
			item->data[15]);
	data += "}";
	syslog(LOG_NOTICE, "[Network] Publishing GPS message");
	CloudSendMessage(topic, data);
}

} /* namespace RFN */
