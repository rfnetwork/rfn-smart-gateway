/*
 * Copyright (c) 2020, RF Networks Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code are not permitted.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of RF Networks Ltd. nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_UTILITIES_H_
#define SRC_UTILITIES_H_

#include <map>
#include <algorithm>
#include <vector>
#include <cstring>
#include <ctime>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <stdint.h>
#include <uci.h>
#include <math.h>
#include <boost/crc.hpp>
#include <boost/integer.hpp>
#include <boost/filesystem.hpp>
#include <sys/utsname.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/sysinfo.h>
#include <netinet/in.h>
#include <net/if.h>
#include <fcntl.h>
#include <gpiod.h>
extern "C" {
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <i2c/smbus.h>
}
#include <gps.h>
#include "Defines.h"

namespace RFN {

namespace Utilities {

class Utilities {
public:
	Utilities();
	virtual ~Utilities();

	static uint8_t GetDebugLevel() {
		char pathIP[] = "rfn.debug.level";
		char result[2];
		uint8_t size = GetUCIProperty(pathIP, result);
		return (size > 0) ? atoi(result) : APP_LOG_LEVEL;
	}

	static WorkingMode GetMode() {
		char pathMode[] = "rfn.server.mode";
		char result[5];
		uint8_t size = GetUCIProperty(pathMode, result);
		uint8_t value = (size > 0) ? atoi(result) : MODE_UNKNOWN;
		return (value < MODE_MAX) ? (WorkingMode) value : MODE_UNKNOWN;
	}

	static bool SetMode(WorkingMode mode) {
		return SetUCIProperty("rfn", "server", "mode",
				std::to_string((uint8_t) mode));
	}

	static std::string GetTelitToken() {
		char pathToken[] = "rfn.server.token";
		char result[20];
		uint8_t size = GetUCIProperty(pathToken, result);
		return (size > 0) ? std::string(result) : std::string("");
	}

	static bool SetTelitToken(std::string token) {
		return SetUCIProperty("rfn", "server", "token", token);
	}

	static std::string GetTelitCloudURL() {
		char pathURL[] = "rfn.server.cloudurl";
		char result[50];
		uint8_t size = GetUCIProperty(pathURL, result);
		return (size > 0) ? std::string(result) : std::string("");
	}

	static bool SetTelitCloudURL(std::string url) {
		return SetUCIProperty("rfn", "server", "cloudurl", url);
	}

	static std::string GetTelitAppID() {
		char pathAppID[] = "rfn.server.appid";
		char result[50];
		uint8_t size = GetUCIProperty(pathAppID, result);
		return (size > 0) ? std::string(result) : std::string("");
	}

	static bool SetTelitAppID(std::string appid) {
		return SetUCIProperty("rfn", "server", "appid", appid);
	}

	static std::string GetAWSCloudURL() {
		char pathURL[] = "rfn.server.awscloudurl";
		char result[50];
		uint8_t size = GetUCIProperty(pathURL, result);
		return (size > 0) ? std::string(result) : std::string("");
	}

	static bool SetAWSCloudURL(std::string url) {
		return SetUCIProperty("rfn", "server", "awscloudurl", url);
	}

	static std::string GetIMEI() {
		char pathIMEI[] = "rfn.server.imei";
		char result[16];
		uint8_t size = GetUCIProperty(pathIMEI, result);
		return (size > 0) ? std::string(result) : std::string("");
	}

	static bool SetIMEI(std::string imei) {
		return SetUCIProperty("rfn", "server", "imei", imei);
	}

	static bool NoModemDefined() {
//		char pathNoModem[] = "rfn.server.nomodem";
//		char result[5];
//		uint8_t size = GetUCIProperty(pathNoModem, result);
//		uint8_t value = (size > 0) ? atoi(result) : 0;
//		return value;

		return !Contains3GSetting() && !Contains4GSetting();
	}

	static long long GetMacAddress() {
		long long mac = 0;
		int fd;
		struct ifreq s;
		int i;

		fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
		strcpy(s.ifr_name, "wlan0"); // Get mac of the wifi module
		if (ioctl(fd, SIOCGIFHWADDR, &s) == 0) {
			for (i = 0; i < 6; i++) {
				mac += (((long long) (s.ifr_addr.sa_data[i] & 0xFF)) << (8 * (5 - i)));
			}
		}
		return mac;
	}

	static std::string GetSMSPass() {
		char pathSMSPass[] = "rfn.server.smspass";
		char result[16];
		uint8_t size = GetUCIProperty(pathSMSPass, result);
		return (size > 0) ? std::string(result) : std::string("");
	}

	static bool SetSMSPass(std::string pass) {
		return SetUCIProperty("rfn", "server", "smspass", pass);
	}

	static std::string GetServerIP() {
		char pathServerIP[] = "rfn.server.serverIP";
		char result[16];
		uint8_t size = GetUCIProperty(pathServerIP, result);
		return (size > 0) ? std::string(result) : std::string("");
	}

	static bool SetServerIP(std::string ip) {
		return SetUCIProperty("rfn", "server", "serverIP", ip);
	}

	static uint16_t GetServerPort() {
		char pathServerPort[] = "rfn.server.serverPort";
		char result[5];
		uint8_t size = GetUCIProperty(pathServerPort, result);
		return (size > 0) ? (uint16_t) atoi(result) : 0;
	}

	static bool SetServerPort(uint16_t port) {
		return SetUCIProperty("rfn", "server", "serverPort",
				std::to_string(port));
	}

	static uint8_t GetAccelerometerSensitivity() {
		char pathAccSensitivity[] = "rfn.server.accsens";
		char result[5];
		uint8_t size = GetUCIProperty(pathAccSensitivity, result);
		return (size > 0) ? (uint8_t) atoi(result) : 0xFF;
	}

	static bool SetAccelerometerSensitivity(uint8_t sensitivity) {
		return SetUCIProperty("rfn", "server", "accsens",
			std::to_string(sensitivity));
	}

	static int GetGPSPeriod() {
		char pathGPSPeriod[] = "rfn.server.gpsperiod";
		char result[10];
		uint8_t size = GetUCIProperty(pathGPSPeriod, result);
		return (size > 0) ? atoi(result) : -1;
	}

	static bool SetGPSPeriod(uint32_t period) {
		return SetUCIProperty("rfn", "server", "gpsperiod",
			std::to_string(period));
	}

	static int GetGPSMovementThreshold() {
		char pathGPSMovementThreshold[] = "rfn.server.gpsthreshold";
		char result[10];
		uint8_t size = GetUCIProperty(pathGPSMovementThreshold, result);
		return (size > 0) ? atoi(result) : -1;
	}

	static bool SetGPSMovementThreshold(uint32_t threshold) {
		return SetUCIProperty("rfn", "server", "gpsthreshold",
			std::to_string(threshold));
	}

	static bool Contains3GSetting() {
		char result[2] = { 0 };
		char path3G[] = "network.3G.proto";
		uint8_t size = RFN::Utilities::Utilities::GetUCIProperty(path3G,
				result);
		return size > 0;
	}

	static bool Contains4GSetting() {
		char result[2] = { 0 };
		char path3G[] = "network.4G.proto";
		uint8_t size = RFN::Utilities::Utilities::GetUCIProperty(path3G,
				result);
		return size > 0;
	}

	static std::string GetAPN() {
		char result[50];
		if (Contains3GSetting()) {
			char pathAPN[] = "network.3G.apn";
			return (GetUCIProperty(pathAPN, result) > 0)? result : "";
		} else if (Contains4GSetting()) {
			char pathAPN[] = "network.4G.apn";
			return (GetUCIProperty(pathAPN, result) > 0)? result : "";
		}
		return "";
	}

	static std::string GetCellularUsername() {
		char result[50];
		if (Contains3GSetting()) {
			char pathUsername[] = "network.3G.username";
			return (GetUCIProperty(pathUsername, result) > 0)? result : "";
		} else if (Contains4GSetting()) {
			char pathUsername[] = "network.4G.username";
			return (GetUCIProperty(pathUsername, result) > 0)? result : "";
		}
		return "";
	}

	static std::string GetCellularPassword() {
		char result[50];
		if (Contains3GSetting()) {
			char pathPassword[] = "network.3G.password";
			return (GetUCIProperty(pathPassword, result) > 0)? result : "";
		} else if (Contains4GSetting()) {
			char pathPassword[] = "network.4G.password";
			return (GetUCIProperty(pathPassword, result) > 0)? result : "";
		}
		return "";
	}

	static bool Set3GSettings(std::string apn, std::string user,
			std::string pass) {
		return SetUCIProperty("network", "3G", "apn", apn)
				&& SetUCIProperty("network", "3G", "username", user)
				&& SetUCIProperty("network", "3G", "password", pass);
	}

	static bool Set4GSettings(std::string apn, std::string user,
			std::string pass) {
		return false;
	}

	static uint8_t GetMeshChannel() {
		char pathMeshChannel[] = "rfn.server.channel";
		char result[5];
		uint8_t size = GetUCIProperty(pathMeshChannel, result);
		return (size > 0) ? (uint8_t) atoi(result) : 0xFF;
	}

	static bool SetMeshChannel(uint8_t channel) {
		if (channel > 7)
			return false;
		return SetUCIProperty("rfn", "server", "channel",
			std::to_string(channel));
	}

	static uint8_t GetMeshAdvChannel() {
		char pathMeshAdvChannel[] = "rfn.server.advChannel";
		char result[5];
		uint8_t size = GetUCIProperty(pathMeshAdvChannel, result);
		return (size > 0) ? (uint8_t) atoi(result) : 0xFF;
	}

	static bool SetMeshAdvChannel(uint8_t advchannel) {
		if (advchannel > 7)
			return false;
		return SetUCIProperty("rfn", "server", "advChannel",
			std::to_string(advchannel));
	}

	static uint8_t GetMeshBaseTime() {
		char pathMeshBaseTime[] = "rfn.server.basetime";
		char result[5];
		uint8_t size = GetUCIProperty(pathMeshBaseTime, result);
		return (size > 0) ? (uint8_t) atoi(result) : 0xFF;
	}

	static bool SetMeshBaseTime(uint8_t basetime) {
		return SetUCIProperty("rfn", "server", "basetime",
			std::to_string(basetime));
	}

	static uint8_t GetMeshFramesInCycle() {
		char pathMeshFramesInCycle[] = "rfn.server.framesInCycle";
		char result[5];
		uint8_t size = GetUCIProperty(pathMeshFramesInCycle, result);
		return (size > 0) ? (uint8_t) atoi(result) : 0xFF;
	}

	static bool SetMeshFramesInCycle(uint8_t frames) {
		if (frames > 3)
			return false;
		return SetUCIProperty("rfn", "server", "framesInCycle",
			std::to_string(frames));
	}

	static uint8_t GetMeshNetworkID() {
		char pathMeshNetworkID[] = "rfn.server.networkID";
		char result[5];
		uint8_t size = GetUCIProperty(pathMeshNetworkID, result);
		return (size > 0) ? (uint8_t) atoi(result) : 0xFF;
	}

	static bool SetMeshNetworkID(uint8_t networkID) {
		return SetUCIProperty("rfn", "server", "networkID",
			std::to_string(networkID));
	}

	static uint16_t GetMeshSyncWord() {
		char pathMeshSyncWord[] = "rfn.server.syncWord";
		char result[5];
		uint8_t size = GetUCIProperty(pathMeshSyncWord, result);
		return (size > 0) ? (uint16_t) atoi(result) : 0;
	}

	static bool SetMeshSyncWord(uint16_t syncword) {
		return SetUCIProperty("rfn", "server", "syncWord",
			std::to_string(syncword));
	}

	static int GetRebootAfter() {
		char pathRebootAfter[] = "rfn.server.rebootAfter";
		char result[5];
		uint8_t size = GetUCIProperty(pathRebootAfter, result);
		return (size > 0) ? atoi(result) : -1;
	}

	static bool SetRebootAfter(int reboot_after) {
		return SetUCIProperty("rfn", "server", "rebootAfter",
				std::to_string(reboot_after));
	}

	static int GetSocketIdleInterval() {
		char pathSocketIdleInterval[] = "rfn.server.socketIdleInterval";
		char result[5];
		uint8_t size = GetUCIProperty(pathSocketIdleInterval, result);
		return (size > 0) ? atoi(result) : -1;
	}

	static bool SetSocketIdleInterval(int interval) {
		return SetUCIProperty("rfn", "server", "socketIdleInterval",
				std::to_string(interval));
	}

	static void CheckConfigFile() {
		if (!boost::filesystem::exists("/etc/config/rfn")) {
			RFN::Utilities::Utilities::exec(
					"echo \"config server 'server'\" >> /etc/config/rfn");
			RFN::Utilities::Utilities::exec(
					"echo \"config server 'debug'\" >> /etc/config/rfn");
		}
	}

	static uint8_t GetUCIProperty(char *path, char *res) {
		struct uci_ptr ptr;
		struct uci_context *c = uci_alloc_context();

		if (!c)
			return 0;

		if ((uci_lookup_ptr(c, &ptr, path, true) != UCI_OK)
				|| (ptr.o == NULL || ptr.o->v.string == NULL)) {
			uci_free_context(c);
			return 0;
		}

		if (ptr.flags & uci_ptr::UCI_LOOKUP_COMPLETE)
			strcpy(res, ptr.o->v.string);

		uci_free_context(c);

		return strlen(ptr.o->v.string);
	}

	static bool SetUCIProperty(std::string package, std::string section,
			std::string option, std::string value) {
		struct uci_ptr ptr;
		struct uci_context *c = uci_alloc_context();

		if (!c)
			return false;

		std::string path = package;
		if (!section.empty()) {
			path += "." + section;
			if (!option.empty()) {
				path += "." + option;
			}
		}

		int res = uci_lookup_ptr(c, &ptr, (char*) path.c_str(), true);
		if (!value.empty() && ((res != UCI_OK) || (ptr.o == NULL || ptr.o->v.string == NULL))) {
			ptr.package = package.c_str();
			if (!section.empty()) {
				ptr.section = section.c_str();
				if (!option.empty()) {
					ptr.option = option.c_str();
				}
			}
			ptr.value = value.c_str();
			if ((uci_set(c, &ptr) != UCI_OK) || (uci_save(c, ptr.p) != UCI_OK)
					|| (uci_commit(c, &ptr.p, true) != UCI_OK)) {
				uci_free_context(c);
				return false;
			}
			uci_free_context(c);
			return true;
		}
		if (ptr.flags & uci_ptr::UCI_LOOKUP_COMPLETE) {
			ptr.value = value.c_str();
			if (value.empty()) {
				if ((uci_delete(c, &ptr) != UCI_OK)	|| (uci_save(c, ptr.p) != UCI_OK)
						|| (uci_commit(c, &ptr.p, false) != UCI_OK)) {
					uci_free_context(c);
					return false;
				}
			} else {
				if ((uci_set(c, &ptr) != UCI_OK) || (uci_save(c, ptr.p) != UCI_OK)
						|| (uci_commit(c, &ptr.p, true) != UCI_OK)) {
					uci_free_context(c);
					return false;
				}
			}
		}
		uci_free_context(c);
		return true;
	}

	static inline void RemoveSub(std::string &sInput, const std::string &sub) {
		std::string::size_type foundpos = sInput.find(sub);
		if (foundpos != std::string::npos)
			sInput.erase(sInput.begin() + foundpos,
					sInput.begin() + foundpos + sub.length());
	}

	template<typename ... Args>
	static std::string StringFormat(const std::string &format, Args ... args) {
		size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'
		if (size <= 0) {
			throw std::runtime_error("Error during formatting.");
		}
		std::unique_ptr<char[]> buf(new char[size]);
		snprintf(buf.get(), size, format.c_str(), args ...);
		return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
	}

	static bool replace(std::string& str, const std::string& from, const std::string& to) {
	    size_t start_pos = str.find(from);
	    if(start_pos == std::string::npos)
	        return false;
	    str.replace(start_pos, from.length(), to);
	    return true;
	}

	static bool StringIsNumber(const std::string &s) {
		return !s.empty()
				&& std::find_if(s.begin(), s.end(), [](unsigned char c) {
					return !std::isdigit(c);
				}) == s.end();
	}

	static long GetUpTimeSeconds() {
		std::chrono::milliseconds uptime(0u);
		struct timespec ts;
		if (clock_gettime(CLOCK_BOOTTIME, &ts) == 0) {
			uptime = std::chrono::milliseconds(
					static_cast<unsigned long long>(ts.tv_sec) * 1000ULL
							+ static_cast<unsigned long long>(ts.tv_nsec)
									/ 1000000ULL);
		}
		return std::chrono::duration_cast<std::chrono::seconds>(uptime).count();
	}

	static time_t GetUTCTimestamp() {
		time_t now;
		time(&now);
		return now;
	}

	static std::tm* GetUTCTime() {
		time_t now = GetUTCTimestamp();
		return gmtime(&now);
	}

	static bool SetUTCTime(time_t timestamp) {
		return !stime(&timestamp);
	}

	static std::string TimestampUTCToString(long t_value) {
		char buf[20];
		time_t t = (time_t) t_value;
		std::tm *_tm = std::gmtime(&t);
		std::strftime(buf, 20, "%d/%m/%Y %X", _tm);
		return std::string(buf);
	}

	static std::string GetRFCTimestamp() {
		char buf[21];
		std::tm* _tm =  GetUTCTime();
		std::strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%SZ", _tm);
		return std::string(buf);
	}

	static std::string GetRFCTimestamp(long t_value) {
		char buf[21];
		time_t t = (time_t) t_value;
		std::tm* _tm =  std::gmtime(&t);
		std::strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%SZ", _tm);
		return std::string(buf);
	}

	struct exec_result {
		std::string result;
		int result_code;
	};

	static exec_result exec(std::string cmd) {
		char buffer[128];
		exec_result res;
		res.result = "";
		res.result_code = 0;
		syslog(LOG_INFO, "Executing: %s", cmd.c_str());
		FILE *pipe = popen(cmd.c_str(), "r");
		if (!pipe)
			throw std::runtime_error("popen() failed!");
		try {
			while (fgets(buffer, sizeof buffer, pipe) != NULL) {
				res.result += buffer;
			}
		} catch (...) {
			throw;
		}
		res.result_code = pclose(pipe);
		return res;
	}

	static uint8_t* BitStuffingDecode(uint8_t *msg, size_t size, uint8_t *res,
			size_t *new_size) {
		size_t p = 0;
		*new_size = p;
		if (msg == NULL || res == NULL || size < 1)
			return NULL;
		for (size_t i = 0; i < size; i++) {
			if ((msg[i] != 0xAB) && (msg[i] != 0xCD)) {
				if (msg[i] == 0xEF) {
					if ((i + 1) < size)
						res[p++] = (msg[++i] == 0xEF) ? 0xEF : msg[i] - 1;
				} else {
					res[p++] = msg[i];
				}
			} else if (msg[i] == 0xCD) {
				break;
			}
		}
		*new_size = p;
		return res;
	}

	static uint8_t* BitStuffingEncode(uint8_t *msg, size_t size, uint8_t *res,
			size_t *new_size) {
		size_t p = 0;
		*new_size = p;
		if (msg == NULL || res == NULL || size < 1)
			return NULL;
		res[p++] = 0xAB;
		for (size_t i = 0; i < size; i++) {
			if (msg[i] == 0xAB || msg[i] == 0xCD || msg[i] == 0xEF) {
				res[p++] = 0xEF;
				res[p++] = (msg[i] == 0xEF) ? 0xEF : (msg[i] + 1);
			} else {
				res[p++] = msg[i];
			}
		}
		res[p++] = 0xCD;
		*new_size = p;
		return res;
	}

	static boost::uint16_t CalculateCRC16Xmodem(unsigned char *data,
			size_t size) {
		boost::crc_optimal<16, 0x1021, 0, 0, false, false> result;
		result.process_bytes(data, size);
		return result.checksum();
	}

	static uint8_t CalculateChecksum(uint8_t *data, size_t size) {
		uint8_t cs = 0;
		for (size_t i = 0; i < size; i++)
			cs += data[i];
		return cs;
	}

	static std::string HexArrayToString(uint8_t *msg, size_t size) {
		std::stringstream ret("");
		for (size_t i = 0; i < size; i++)
			ret << std::setfill('0') << std::setw(2) << std::hex
					<< std::uppercase << (int) msg[i];
		ret << std::dec;
		return ret.str();
	}

	static unsigned long GetAvailableMemory() {
		struct sysinfo info;
		if (sysinfo(&info) < 0)
			return 0;

		return info.freeram;
	}

	static std::string WorkingModeToString(WorkingMode mode) {
		switch (mode) {
		case MODE_STANDARD:
			return "Standard";
		case MODE_TRANSPARENT:
			return "Transparent";
		case MODE_BRIDGE:
			return "Bridge";
		case MODE_TELIT_CLOUD:
			return "DeviceWise";
		case MODE_AWS_IOT:
			return "AWS IoT Core";
		default:
			return "Unknown";
		}
	}

	static void SetLedStatus(LedStatus status) {
		std::string stat = "phy0tpt";
		if (status == LED_STATUS_OFF) {
			stat = "none";
		} else if (status == LED_STATUS_ON) {
			stat = "default-on";
		}
		exec(
				StringFormat(
						"echo \"%s\" > /sys/class/leds/linkit-smart-7688\\:orange\\:wifi/trigger",
						stat.c_str()));
	}

	static void CloseGPIOs() {
		if (chip)
			gpiod_chip_close(chip);
	}

	static struct gpiod_line* GetGPIO(int gpio) {
		if (GetGPIOChip()) {
			return gpiod_chip_get_line(chip, gpio);
		}
		return 0;
	}

	static int SetGPIO(struct gpiod_line *line, int value) {
		if (line)
			return gpiod_line_set_value(line, value);
		return -1;
	}

	static void SetGPIOOutput(struct gpiod_line *line, int default_value = 0) {
		if (line)
			gpiod_line_request_output(line, "Consumer", default_value);
	}

	static void SetGPIOInput(struct gpiod_line *line) {
		if (line)
			gpiod_line_request_input(line, "Consumer");
	}

	static int ReadGPIO(struct gpiod_line *line) {
		if (line)
			return gpiod_line_get_value(line);
		return -1;
	}

	static int I2CSetWorkingDeviceAddress(int address) {
		int handler = GetI2CHandler();
		if (handler < 0)
			return handler;
		if (ioctl(handler, I2C_SLAVE, address) < 0) {
			close(handler);
			handler = -1;
			return handler;
		}
		return 0;
	}

	static int I2CWriteRegister(uint8_t reg, uint8_t value) {
		int handler = GetI2CHandler();
		return (handler < 0)? -1 : i2c_smbus_write_byte_data(handler, reg, value);
	}

	static int I2CReadRegister(uint8_t reg) {
		int handler = GetI2CHandler();
		return (handler < 0)? -1 : i2c_smbus_read_byte_data(handler, reg);
	}

	static void I2CClose() {
		if (i2c_handler > -1) {
			close(i2c_handler);
			i2c_handler = -1;
		}
	}

	static bool InitializeAccelerometer(uint8_t sensitivity) {
		int data;
		if (I2CSetWorkingDeviceAddress(ACCELEROMETER_I2C_ADDRESS) < 0)
			return false;

		// Validate accelerometer WHO I AM
		data = I2CReadRegister(ACCELEROMETER_WHO_I_AM);
		if (data < 0 || data != ACCELEROMETER_WHO_I_AM_VALUE) {
			return false;
		}

		// Put 0x2A register in standby mode before set all other registers
		if (I2CWriteRegister(0x2A, 0x00) < 0) {
			return false;
		}

		// Set the interrupts to be active high
		if (I2CWriteRegister(0x2C, 0x02) < 0) {
			return false;
		}

		// Enable X ,Y and Z Axes and enable the latch: Register 0x1D Configuration Register
		if (I2CWriteRegister(0x1D, 0x1E) < 0) {
			return false;
		}

		// Set the Threshold: Register 0x1F
		if (I2CWriteRegister(0x1F, sensitivity) < 0) {
			return false;
		}

		// Set the Debounce Counter in ms: Register 0x20
		if (I2CWriteRegister(0x20, 0x05) < 0) {
			return false;
		}

		// Enable Transient Detection Interrupt in 0x2D register
		if (I2CWriteRegister(0x2D, 0x20) < 0) {
			return false;
		}

		// Route the Transient Interrupt to INT 1
		if (I2CWriteRegister(0x2E, 0x20) < 0) {
			return false;
		}

		// 8g, measurment mode
		if (I2CWriteRegister(0x0E, 0x02) < 0) {
			return false;
		}

		// 100Hz and activate the ACC
		if (I2CWriteRegister(0x2A, 0x19) < 0) {
			return false;
		}

		// Set the Threshold: Register 0x1F
		if (I2CWriteRegister(0x1F, 0x01) < 0) {
			return false;
		}

		data = I2CReadRegister(0x0C);
		if (data == 0x20) {
			data = I2CReadRegister(0x1E);
		}

		return true;
	}

	static void ResetAccelerometerInterrupt() {
		int data;
		if (I2CSetWorkingDeviceAddress(ACCELEROMETER_I2C_ADDRESS) < 0)
			return;
		data = I2CReadRegister(0x0C);
		if (data == 0x20) {
			data = I2CReadRegister(0x1E);
		}
	}

	static int GetGPS(struct gps_data_t *gps_data) {
		int res;
		res = gps_open(GPSD_SHARED_MEMORY, NULL, gps_data);
		if (res != -1) {
			res = gps_read(gps_data);
			gps_close(gps_data);
		}
		return res;
	}

private:
	static struct gpiod_chip *chip;
	static int i2c_handler;

	static struct gpiod_chip* GetGPIOChip() {
		if (!chip) {
			chip = gpiod_chip_open("/dev/gpiochip0");
		}
		return chip;
	}

	static int GetI2CHandler() {
		if (i2c_handler < 0) {
			i2c_handler = open("/dev/i2c-0", O_RDWR);
		}
		return i2c_handler;
	}
};

namespace config {
extern std::map<std::string, std::string> mapArgs;
extern std::map<std::string, std::vector<std::string> > mapMultiArgs;
void OptionParser(int argc, const char *const argv[]);
int GetArg(const std::string &strArg, int nDefault);
std::string GetArg(const std::string &strArg, const std::string &strDefault);
const char* GetCharArg(const std::string &strArg, const std::string &nDefault);
} /* namespace config */

} // Namespace Utilities

} // Namespace RFN

#endif /* SRC_UTILITIES_H_ */
